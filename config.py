import os
from flask import url_for

# You need to replace the next values with the appropriate values for your configuration

basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_DATABASE_URI = "postgresql://postgres:monolito@192.168.0.87/db_api_conselho"
ARQUIVO_ROTAS_ABERTAS = "rotas_abertas.txt"

UPLOAD_FOLDER = '/opt/ARQUIVOS_FINANCEIRO'
ALLOWED_EXTENSIONS = {'csv'}

"""
SMTP CONFIG
"""
SMTP_HOST = "email-smtp.us-east-1.amazonaws.com"
SMTP_PORT = 587
SMTP_USER = "AKIAIIZJVK5RAIIHJ7HQ"
SMTP_PASS = "AryzSu3UZa2ho9/d/8nAeGF++eBFjJ8BDSFIQtxfoYzf"
SMTP_TTLS = True

"""
Rotas para API cobranca
"""
API_COBRANCA_CHAVE_DE_ACESSO = "8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5"
API_COBRANCA_ROTA_PAGAMENTO_EXECUTAR = "/rota/pagamento/executar"
API_COBRANCA_ROTA_VENDEDOR_BUSCAR_LANCAMENTOS_FUTUROS_FINACEIRA = "/rota/vendedor/buscar/lancamentos/futuros/financeira"
API_COBRANCA_ROTA_PAGAMENTO_EXECUTAR = "/rota/pagamento/executar"
SEGURANCA_CHAVE_DE_ACESSO = "8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5"

HOST_POPPAY = "54.233.239.191"
PORTA_POPPAY = "3000"

"""
Front-end config
"""
SMS_ATIVADO = False

API_CONSELHO_URL_PRINCIPAL = "http://127.0.0.1:5000"
API_CONSELHO_ROTA_AUTENTICAR = "/rota/autenticacao/autenticar"
API_CONSELHO_ROTA_LOGOUT = "/rota/autenticacao/deslogar"
API_CONSELHO_ROTA_ADMINISTRACAO_LISTA_ULTIMAS_TRANSACOES = "/rota/administracao/listar/transacoes"
API_CONSELHO_ROTA_ADMINISTRACAO_LISTAR_ASSOCIADOS = "/rota/administracao/listar/associados"
API_CONSELHO_ROTA_ADMINISTRACAO_RECUPERAR_DADOS_ASSOCIADO = "/rota/administracao/recuperar/dados/associado"
API_CONSELHO_ROTA_ADMINISTRACAO_EDITAR_DADOS_ASSOCIADO = "/rota/administracao/editar/associado"
API_CONSELHO_ROTA_ADMINISTRACAO_LISTA_PAGAMENTOS = "/rota/administracao/listar/pagamentos"
API_CONSELHO_ROTA_ADMINISTRACAO_DEVEDORES = "/rota/administracao/devedores"

"""
 Rotas Api Conselho Cliente
"""
API_CONSELHO_ROTA_ASSOCIADOS_LISTAR_PENDENCIAS = "/rota/associado/listar/pendencias"
API_CONSELHO_ROTA_ASSOCIADOS_LISTAR_PAGAMENTOS = "/rota/associado/listar/pagamentos"
API_CONSELHO_ROTA_ASSOCIADOS_RECUPERAR_DADOS = "/rota/associado/dados"
API_CONSELHO_ROTA_ASSOCIADOS_DETALHES_COBRANCA = "/rota/associado/detalhes/cobranca"


