from faker import Faker
from faker.providers import python
ultimas_transacoes = []

fake = Faker(['pt_BR'])
fake.add_provider(python)

for _ in range(10):
    tmp_data = {
        'id': fake.pyint(min_value=1, max_value=500, step=2),
        'nome': fake.name(),
        'inscricao': fake.pyint(min_value=11111, max_value=99999, step=2),
        'valor': fake.pyint(min_value=1000, max_value=9999, step=2),
        'forma': fake.pyint(min_value=1, max_value=2, step=1),
        'ano': fake.pyint(min_value=10, max_value=20, step=2)
    }
    ultimas_transacoes.append(tmp_data)

print(ultimas_transacoes)