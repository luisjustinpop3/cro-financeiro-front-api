from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor


class OperacoesAutenticacao:
    global SEGURANCA_PERMITIR_ACESSO

    def __init__(self):
        self.SEGURANCA_PERMITIR_ACESSO = False

    """
    Verifica a sessao ID gerada pela autenticacao
    """

    def autenticacao_verificar_chave(self, objeto_requisicao):
        permissao = False
        cb = ConectarBanco()
        db_cursor = cb.db_conexao.cursor()
        db_cursor.execute(
            "select * from tb_sessao where str_sessao_token = %s ",
            [
                objeto_requisicao['SESSAO_TOKEN']
            ]
        )
        rows = db_cursor.fetchall()
        if (len(rows) > 0):
            permissao = True

        db_cursor.close()
        cb.fecha_conexao()
        return permissao



