# LIBRARYS
import json, secrets
import os

from flask import Flask, request, Request, render_template, flash, url_for, make_response, session
from flask_middleware import Middleware
from flask_cors import CORS
# MIDDLEWHARE
from flask_restful import abort
from werkzeug.utils import secure_filename, redirect

from api_externa import GerarRespostaPadrao
from decorators.AutenticadoDecorator import esta_autenticado_api
from middlewares.OperacoesAutenticacao import OperacoesAutenticacao
from resources.rotas_autenticacao.RotasAutenticacao import rotas_autenticacao
from resources.rotas_associados.RotasAssociado import rotas_associado
from resources.rotas_webhook.RotasWebhook import rotas_webhooks
from resources.rotas_administracao.RotasAministracao import rotas_administracao
from modulos.modulo_administracao.AdministracaoOperacoes import AdministracaoOperacoes
from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor
from threading import Thread

global JSON_DATA  # Json para receber de toda requisicao e verificar primeiramente se é autenticação (autenticar)
global AUTENTICAR # Define se é uma rota padrao ou é autenticar, que deve ser pública
global SEGURANCA_PERMITIR_ACESSO # informa se o acesso e permitido ou nao validado pelo modulo de seguranca
global SEGURANCA_CHAVE_DE_ACESSO # chave definida temporariamente pelo modo de segurança para proxima requisicao

JSON_DATA = {}
AUTENTICAR = None
SEGURANCA_PERMITIR_ACESSO = False
SEGURANCA_CHAVE_DE_ACESSO = None


UPLOAD_FOLDER = '/opt/ARQUIVOS_FINANCEIRO'
ALLOWED_EXTENSIONS = {'csv'}
RETORNO = None



def create_app(filename):

    app = Flask(__name__)
    app.config.from_object('config')
    CORS(app)
    app.secret_key = secrets.token_urlsafe(16)
    middleware = Middleware(app)
    app.register_blueprint(rotas_autenticacao)
    app.register_blueprint(rotas_associado)
    app.register_blueprint(rotas_webhooks)
    app.register_blueprint(rotas_administracao)
    from web_routes import registerRoutes
    registerRoutes(app)
    app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

    """
    Rota padrão 
    """
    @app.route('/', methods = ['GET'])
    def post():
        return redirect('admin')

    def allowed_file(filename):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

    @app.route('/rota/admininstracao/subir/arquivo/com/todos/usuarios', methods=['GET', 'POST'])
    @esta_autenticado_api
    def upload_file():
        if request.method == 'POST':
            # verifica se a requisicao tem o arquivo
            if 'file' not in request.files:
                flash('No file part')
                RETORNO = "NAO_FOI_SELECIONADO_O_ARQUIVO"
            else:
                file = request.files['file']
            # se o usuario nao escolheu o arquivo
            if file.filename == '':
                RETORNO = "NAO_EXISTE_ARQUIVO_SELECIONADO"
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], "lista_usuarios.csv"))
                RETORNO = "ARQUIVO_PROCESSADO"
                order_data = json.loads(request.form.get("col_to_csv"))
                delimiter = request.form.get("delimiter")

                cb = ConectarBanco()
                res_data = []
                token = session.get('token_acesso')
                cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
                cursor.execute("SELECT id FROM tb_usuario WHERE id = (SELECT tb_sessao.tb_usuario_id FROM tb_sessao WHERE tb_sessao.str_sessao_token = %s)", [
                    token
                ])
                data = cursor.fetchone()

                token = data["id"]
                administracao_operacoes = AdministracaoOperacoes(order_data, delimiter, token)
                administracao_operacoes.start()


        return make_response(GerarRespostaPadrao.criar_resposta(
            False,
            "ACAO_SUBIR_ARQUIVO_CSV_COM_LISTA_DE_TODOS_USUARIOS",
            "CSV_PROCESSADO"
        ))

    @app.route('/status', methods=["GET"], defaults={ "manutencao": False })
    @app.route('/status/<string:manutencao>', methods=["GET"])
    def notification(manutencao = False):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        res_data = {
            "manutencao": False
        }

        if manutencao:
            cursor.execute("SELECT * FROM tb_notificacoes WHERE type='IMPORT_STARTED' AND status=0 AND token='0'")
            cursor.fetchone()

            if cursor.rowcount > 0:
                res_data = {
                    "manutencao": True
                }

            cursor.execute("SELECT * FROM tb_notificacoes WHERE type='MAINTENCE' AND status=0 AND token='0'")
            cursor.fetchone()

            if cursor.rowcount > 0:
                res_data = {
                    "manutencao": True
                }

        else:
            token = session.get('token_acesso')
            cursor.execute(
                "SELECT id FROM tb_usuario WHERE id = (SELECT tb_sessao.tb_usuario_id FROM tb_sessao WHERE tb_sessao.str_sessao_token = %s)",
                [
                    token
                ])
            uid = cursor.fetchone()["id"]
            cursor.execute("SELECT * FROM tb_notificacoes WHERE token = '%s' AND status=0", [
                uid
            ])
            data = cursor.fetchall()
            num_rows = cursor.rowcount
            # Notificacao especifica
            if num_rows > 0:
                res_data = data
                for d in data:
                    cursor.execute("UPDATE tb_notificacoes SET status=1 WHERE id=%s", [
                        d["id"]
                    ])
                    cb.db_conexao.commit()

        cursor.close()
        cb.fecha_conexao()

        return make_response(json.dumps(res_data))




    return app

if __name__ == "__main__":
    app = create_app('config')
    app.run(host='0.0.0.0', debug=True, port=5000)
