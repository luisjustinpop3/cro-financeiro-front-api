*****************************************************************************************
EXEMPLO TRIGER PARA DELETAR SEMPRE QUE INSERIR ALGO USADO PARA SESSAO
*****************************************************************************************

CREATE FUNCTION function_deleta_a_sessao() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM tb_sessao WHERE timestamp < NOW() - INTERVAL '2 days';
  RETURN NULL;
END;
$$;

CREATE TRIGGER trigger_deleta_a_sessao
    AFTER INSERT ON tb_sessao
    EXECUTE PROCEDURE function_deleta_a_sessao();


*****************************************************************************************

http://f43d69a0.ngrok.io/rota/webhook/zoop/atualiza/estatus/do/pagamento



*****************************************************************************************
JSON DE RETORNO DE NOSSA API

{
	"operacao": "ACAO_TESTE",
	"retorno": {
		"bol_cobranca_regras_de_divisao_dividir": false,
		"dat_cobranca_data_de_vencimento": "2019-10-16 00:00:00",
		"dou_cobranca_regras_de_divisao_em_porcentagem": 0.0,
		"int_cobranca_numero_de_parcelas": 1,
		"lon_cobranca_regras_de_divisao_em_valor": 0,
		"lon_cobranca_regras_de_divisao_taxa_de_processamento": 0,
		"lon_cobranca_valor_total": 1000,
		"str_chave_de_acesso": "8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5",
		"str_cobranca_moeda": "BRL",
		"str_cobranca_tipo": "card",
		"tb_situacao_da_cobranca_str_situacao_da_cobranca": "COBRANCA_ENVIADA_AGUARDANDO",
		"txt_cobranca_capturar": "true",
		"txt_cobranca_descricao": "Pagamento 2019",
		"txt_cobranca_hash_identificar_cliente": "hash que identifica o pagante cliente da loja",
		"txt_cobranca_hash_identificar_cobranca": "hash_da_cobranca",
		"txt_cobranca_identificacao_de_referencia": "8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]hash_da_cobranca[#]pagado90",
		"txt_cobranca_identificacao_do_vendedor": "8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5",
		"txt_cobranca_regras_de_divisao_codigo_extra": "0",
		"txt_cobranca_regras_de_divisao_recebedor": "false",
		"txt_cobranca_str_token_do_cartao_utilizado_para_pagar": "hash_do_cliente_gerado_identifica_o_dono_do_cartao21c36ff63663eb998d840f7268bc20b66904be8a1b912a3054545d328601576f7048414717119833543236040985797198353",
		"txt_hash_unico_gerado": "pagado90"
	}
}



****************************************************************************************


VERSAO FINAL QUE DE SER ENVIADA PARA API 1 (GATEWAY) QUE VAI EXECUTAR
{
	"ACAO": "ACAO_ASSOCIADO_PAGAR",
"STR_SESSAO_TOKEN":"0b23c2402c7b4482c65d187fc90fa2803435fbbd674205ef8229434395a03278",
       "SEGURANCA_CHAVE_DE_ACESSO":"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5",
"txt_payment_type":"credit",
	"dat_cobranca_data_de_vencimento": "2019-10-16",
	"dat_cobranca_data_retorno_financeira": "2019-10-16",
	"lon_cobranca_valor_total": "1000",
	"str_cobranca_moeda": "BRL",
	"txt_cobranca_descricao": "Pagamento 2019",
	"txt_cobranca_capturar": "true",
	"txt_cobranca_identificacao_de_referencia": "8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]aa1[#]pagado63",
	"int_cobranca_numero_de_parcelas": "1",
	"bol_cobranca_regras_de_divisao_dividir":"false",
	"txt_cobranca_regras_de_divisao_recebedor": "false",
	"txt_cobranca_regras_de_divisao_codigo_extra": "0",
	"lon_cobranca_regras_de_divisao_taxa_de_processamento": "0",
	"dou_cobranca_regras_de_divisao_em_porcentagem": "0.0",
	"lon_cobranca_regras_de_divisao_em_valor": "0",
	"str_cobranca_tipo": "card",
	"txt_cobranca_hash_identificar_cobranca": "hash identifica a propria cobranca aqui",
	"txt_cobranca_str_token_do_cartao_utilizado_para_pagar": "hash_do_cliente_gerado_identifica_o_dono_do_cartao21c36ff63663eb998d840f7268bc20b66904be8a1b912a3054545d328601576f7048414717119833543236040985797198353",
	"txt_cobranca_hash_identificar_cliente": "hash que identifica o pagante cliente da loja",
"str_cobranca_cartao_bandeira":"VISA",
"str_cobranca_cartao_ano":"2020",
"str_cobranca_cartao_mes":"03",
"str_cobranca_cartao_numero":"4539003370725497",
"str_cobranca_cartao_codigo_de_seguranca":"123",
"str_cobranca_cartao_titular":"Rafael",
"tb_situacao_da_cobranca_str_situacao_da_cobranca":"COBRANCA_ENVIADA_AGUARDANDO",
"txt_usage":"single_use",
"txt_mode":"interest_free"
}

VERSSAO SIMPLIFICADA QUE O O SISTEMA API (DO CONSELHO) DEVE RECEBER, ALGUNS CAMPOS SAO TERADOS NO PROPRIO BACK END ANTES DE SER ENVIADO PARA A API DO GATEWAY

{
	"ACAO": "ACAO_ASSOCIADO_PAGAR",
    "STR_SESSAO_TOKEN":"0b23c2402c7b4482c65d187fc90fa2803435fbbd674205ef8229434395a03278",
    "SEGURANCA_CHAVE_DE_ACESSO":"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5",
    "txt_payment_type":"credit",
	"dat_cobranca_data_de_vencimento": "2019-10-16",
	"dat_cobranca_data_retorno_financeira": "",
	"lon_cobranca_valor_total": "1000",
	"str_cobranca_moeda": "BRL",
	"txt_cobranca_descricao": "2019/Unica",
	"txt_cobranca_capturar": "true",
	"txt_cobranca_identificacao_de_referencia": "8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]hash identifica a propria cobranca aqui[#]pagado63",
	"int_cobranca_numero_de_parcelas": "1",
	"bol_cobranca_regras_de_divisao_dividir":"false",
	"txt_cobranca_regras_de_divisao_recebedor": "false",
	"txt_cobranca_regras_de_divisao_codigo_extra": "0",
	"lon_cobranca_regras_de_divisao_taxa_de_processamento": "0",
	"dou_cobranca_regras_de_divisao_em_porcentagem": "0.0",
	"lon_cobranca_regras_de_divisao_em_valor": "0",
	"str_cobranca_tipo": "card",
	"txt_cobranca_hash_identificar_cobranca": "hash identifica a propria cobranca aqui",
	"txt_cobranca_str_token_do_cartao_utilizado_para_pagar": "",
	"txt_cobranca_hash_identificar_cliente": "hash que identifica o pagante cliente da loja",
"str_cobranca_cartao_bandeira":"VISA",
"str_cobranca_cartao_ano":"2020",
"str_cobranca_cartao_mes":"03",
"str_cobranca_cartao_numero":"4539003370725497",
"str_cobranca_cartao_codigo_de_seguranca":"123",
"str_cobranca_cartao_titular":"Rafael",
"tb_situacao_da_cobranca_str_situacao_da_cobranca":"COBRANCA_ENVIADA_AGUARDANDO",
"txt_usage":"single_use",
"txt_mode":"interest_free"
}


******************************************************************************************

JSON DE EXECUTAR O PAGAMENTO

  """
            {
	"ACAO": "ACAO_PAGAMENTO_EXECUTAR",
       "SEGURANCA_CHAVE_DE_ACESSO":"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5",
"txt_payment_type":"credit",
	"dat_cobranca_data_de_vencimento": "2019-10-16",
	"dat_cobranca_data_retorno_financeira": "2019-10-16",
	"lon_cobranca_valor_total": "1000",
	"str_cobranca_moeda": "BRL",
	"txt_cobranca_descricao": "Pagamento 2019",
	"txt_cobranca_capturar": "true",
	"txt_cobranca_identificacao_de_referencia": "8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]aa1[#]pagado63",
	"int_cobranca_numero_de_parcelas": "1",
	"bol_cobranca_regras_de_divisao_dividir":"false",
	"txt_cobranca_regras_de_divisao_recebedor": "false",
	"txt_cobranca_regras_de_divisao_codigo_extra": "0",
	"lon_cobranca_regras_de_divisao_taxa_de_processamento": "0",
	"dou_cobranca_regras_de_divisao_em_porcentagem": "0.0",
	"lon_cobranca_regras_de_divisao_em_valor": "0",
	"str_cobranca_tipo": "card",
	"txt_cobranca_hash_identificar_cobranca": "hash identifica a propria cobranca aqui",
	"txt_cobranca_str_token_do_cartao_utilizado_para_pagar": "hash_do_cliente_gerado_identifica_o_dono_do_cartao21c36ff63663eb998d840f7268bc20b66904be8a1b912a3054545d328601576f7048414717119833543236040985797198353",
	"txt_cobranca_hash_identificar_cliente": "hash que identifica o pagante cliente da loja",
"str_cobranca_cartao_bandeira":"VISA",
"str_cobranca_cartao_ano":"2020",
"str_cobranca_cartao_mes":"03",
"str_cobranca_cartao_numero":"4539003370725497",
"str_cobranca_cartao_codigo_de_seguranca":"123",
"str_cobranca_cartao_titular":"Rafael",
"tb_situacao_da_cobranca_str_situacao_da_cobranca":"COBRANCA_ENVIADA_AGUARDANDO",
"txt_usage":"single_use",
"txt_mode":"interest_free"
}

