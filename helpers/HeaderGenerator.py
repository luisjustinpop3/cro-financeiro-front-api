import json

def generateHeader(token):
    sess = {
        'SESSAO_TOKEN': token
    }
    headers = {
        'SESSAO_TOKEN': json.dumps(sess)
    }

    return headers