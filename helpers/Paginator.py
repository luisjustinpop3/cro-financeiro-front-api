import math
from flask import request

class Paginator:

    def __init__(self, page, total, url, per_page = 10, search_param="search"):
        self.page = page;
        self.total = total
        self.url = url
        self.per_page = per_page
        self.search_param = search_param;

    def paginate(self):
        html = ""
        search = ""

        if request.args.get('search') != None:
            search = "&"+ str(self.search_param) +"=" + str(request.args.get('search'))

        if( math.ceil( self.total / self.per_page ) > 0 ):
            html += '<ul class="pagination">'

            if ( self.page > 1 ):
                html += '''<li class="page-item">
                                <a class="page-link" href="{}" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                           </li>'''.format(self.url + "?page=" + str(self.page - 1))

            if( self.page > 3 ):
                html += '<li class="page-item"><a class="page-link" href="{}">{}</a></li>'.format(self.url + "?page=" + str("1") + str(search), str("1"))
                html += '<li class="page-item"><span class="page-link">...</span></li>'

            if( (self.page - 2) > 0 ):
                html += '<li class="page-item"><a class="page-link" href="{}">{}</a></li>'.format(self.url + "?page=" + str(self.page - 2) + str(search), str(self.page - 2))

            if( (self.page - 1) > 0 ):
                html += '<li class="page-item"><a class="page-link" href="{}">{}</a></li>'.format(self.url + "?page=" + str(self.page - 1) + str(search), str(self.page - 1))

            #Atual
            html += '<li class="page-item"><a class="page-link" href="{}">{}</a></li>'.format(self.url + "?page=" + str(self.page) + str(search), str(self.page))

            if( (self.page + 1) < math.ceil(self.total / self.per_page)+1 ):
                html += '<li class="page-item"><a class="page-link" href="{}">{}</a></li>'.format(self.url + "?page=" + str(self.page + 1) + str(search), str(self.page + 1))

            if( (self.page + 2) < math.ceil(self.total / self.per_page)+1 ):
                html += '<li class="page-item"><a class="page-link" href="{}">{}</a></li>'.format(self.url + "?page=" + str(self.page + 2) + str(search), str(self.page + 2))

            if( self.page < math.ceil(self.total / self.per_page)-2 ):
                html += '<li class="page-item"><span class="page-link">...</span></li>'
                html += '<li class="page-item"><a class="page-link" href="{}">{}</a></li>'.format(self.url + "?page=" + str(math.ceil(self.total / self.per_page)) + str(search), str(math.ceil(self.total / self.per_page)))

            if( self.page < math.ceil(self.total / self.per_page)):
                html += '''<li class="page-item">
                                                <a class="page-link" href="{}" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>'''.format(self.url + "?page=" + str(self.page + 1))

            html += '</ul>'

        return html