from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor
import datetime

class NotificationSystem:
    def __init__(self):
        self._cb = ConectarBanco()
        self._cursor = self._cb.db_conexao.cursor(cursor_factory=RealDictCursor)

    def insertForUser(self, uid, message, type="NOTIFICATION"):
        sql = "INSERT INTO tb_notifications (type, message, uid, status, created_at) VALUES (%s, %s, %s, 0, NOW())"
        self._cursor.execute(sql, [
            type,
            message,
            uid
        ])

    def insertForUserList(self, uids, message, type="NOTIFICATION"):
        sql = "INSERT INTO tb_notifications (type, message, uid, status, created_at) VALUES (%s, %s, %s, 0, NOW())"
        for uid in uids:
            self._cursor.execute(sql, [
                type,
                message,
                uid
            ])
            self._cb.db_conexao.commit()

    def get(self, uid, status = 0, type = None):
        sql = "SELECT * FROM tb_notifications WHERE uid=%s AND status=%s AND (%s is null OR type = %s)"
        self._cursor.execute(sql, [
            uid,
            status,
            type,
            type
        ])
        result = self._cursor.fetchall()
        print(result)