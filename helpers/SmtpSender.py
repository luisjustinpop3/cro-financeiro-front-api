import pathlib
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class SmtpSender:

    def __init__(self, host, port, login, password, ttls=False):
        self.__host = host
        self.__port = port
        self.__login = login
        self.__password = password
        self.__ttls = ttls
        self.__connection = self.__make_connection()

        self.__use_html_template = False
        self.__parsed_html_template = ""

        self.__use_plain_template = False
        self.__parsed_plain_template = ""

    def __make_connection(self):
        conn = smtplib.SMTP(host=self.__host, port=self.__port)
        if self.__ttls:
            conn.starttls()

        conn.login(self.__login, self.__password)
        return conn

    def __read_template(self, template_location, kwargs):
        with open(template_location) as template_file:
            data = template_file.read()
            for arg in kwargs.items():
                data = data.replace("{" + str(arg[0]) + "}", arg[1])

        return data

    def read_html_template(self, template_location, **kwargss):
        self.__use_html_template = True
        template_location = str(pathlib.Path().absolute()) + template_location
        self.__parsed_html_template = self.__read_template(template_location, kwargss)

    def read_txt_template(self, template_location, **kwargss):
        self.__use_plain_template = True
        template_location = str(pathlib.Path().absolute()) + template_location
        self.__parsed_plain_template = self.__read_template(template_location, kwargss)

    def send_email(self, email_from, to, title, message_html="", message_text=""):
        msg = MIMEMultipart()
        msg["From"] = email_from
        msg["To"] = to
        msg["Subject"] = title

        if self.__use_html_template:
            msg.attach(MIMEText(self.__parsed_html_template, 'html'))
        else:
            if len(message_html) > 0:
                msg.attach(MIMEText(message_html, 'html'))

        if self.__use_plain_template:
            msg.attach(MIMEText(self.__parsed_plain_template, 'plain'))
        else:
            if len(message_text) > 0:
                msg.attach(MIMEText(message_html, 'plain'))

        return self.__connection.sendmail(email_from, to, msg.as_string())

    def close_connection(self):
        return self.__connection.close()
