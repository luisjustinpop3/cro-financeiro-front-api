from helpers.SmsSender.SmsSenderGenerics import SmsSenderGenerics
from urllib import parse
from uuid import uuid1
import requests

class FacilitaMovelGateway(SmsSenderGenerics):

    def __init__(self, auth_parameters):
        self._url = "http://www.facilitamovel.com.br/api/simpleSend.ft?user={user}&password={password}&destinatario={destination}&externalkey={ident_key}123&msg={msg}"
        self._username = auth_parameters["username"]
        self._password = auth_parameters["password"]
        self._message = ""
        self._error_status = {
            1: "Login Inválido",
            2: "Usuário sem Créditos",
            3: "Campo Mensagem e Campo destinatario possuem posicoes diferentes",
            4: "Campo Mensagem Invalida",
            6: "Mensagem enviada"
        }

    def compose(self, msg):
        if len(msg) > 0:
            if len(msg) <= 190:
                self._message = parse.quote(msg)
                return True
            else:
                raise Exception("Message is greather than 190 characters!")
        else:
            raise Exception("Message need greather than 1 character.")

    def send(self, destination):
        if len(self._message) > 0:
            unique_id = parse.quote(str(uuid1()))
            prepared_url = self._url.format(user=self._username, password=self._password, destination=destination, ident_key=unique_id, msg=self._message)
            response = str(requests.get(prepared_url).content)
            status = int(response.split(";")[0].replace("b'", ""))

            if status != 6:
                raise Exception("ERRO STATUS => " + str(self._error_status.get(status)))
            else:
                return True

        else:
            raise Exception("You need compose the message!")