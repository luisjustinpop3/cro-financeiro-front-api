class SmsSender:

    def __init__(self, gateway, auth_parameters):
        self.__gateway = gateway(auth_parameters)
        self.__use_msg_template = False
        self.__parsed_msg_template = ""

    def read_txt_template(self, template_location, **kwargs):
        self.__use_msg_template = True
        self.__parsed_msg_template = self.__read_template(template_location, kwargs)
        self.__gateway.compose(self.__parsed_msg_template)

    @staticmethod
    def __read_template(template_location, kwargs):
        with open(template_location) as template_file:
            data = template_file.read()
            for arg in kwargs.items():
                data = data.replace("{" + str(arg[0]) + "}", arg[1])

        return data

    def compose(self, msg):
        if self.__use_msg_template:
            raise Exception("You cannot use compose if use a txt template.")
        else:
            self.__gateway.compose(msg)

    def send(self, destination):
        return self.__gateway.send(destination)