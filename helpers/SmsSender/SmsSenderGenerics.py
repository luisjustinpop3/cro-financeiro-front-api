from abc import ABC, abstractmethod

class SmsSenderGenerics(ABC):
    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def compose(self):
        pass

    @abstractmethod
    def send(self):
        pass