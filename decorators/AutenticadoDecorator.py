from flask import session, redirect, abort, url_for, request, make_response
from functools import wraps

from middlewares.OperacoesAutenticacao import OperacoesAutenticacao
from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor
import json

#levels
# 1 - cliente
# 2 - admin

def esta_autenticado_cliente(func):
    level = "PADRAO"
    @wraps(func)
    def wrapper(*args, **kwargs):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        cursor.execute("SELECT * FROM tb_notificacoes WHERE type='IMPORT_STARTED' AND status=0 AND token='0'")
        cursor.fetchone()

        if cursor.rowcount > 0:
            session.clear()
            return redirect(url_for('cliente_login_route.manutencao'))

        if ( session.get('token_acesso') != None ):
            if (len(session.get("token_acesso")) > 0):
                if (session.get("nivel_acesso") == level):
                    return func(*args, **kwargs)
                else:
                    session.clear()
                    return redirect(url_for('cliente_login_route.login'))
            else:
                session.clear()
                return redirect(url_for('cliente_login_route.login'))

        else:
            session.clear()
            return redirect(url_for('cliente_login_route.login'))

    return wrapper

def esta_autenticado_admin(func):
    level = "SISTEMA"
    @wraps(func)
    def wrapper(*args, **kwargs):
        if ( session.get('token_acesso') != None ):
            if (len(session.get("token_acesso")) > 0):
                if (session.get("nivel_acesso") == level):
                    return func(*args, **kwargs)
                else:
                    session.clear()
                    return redirect(url_for('login_route.login'))
            else:
                session.clear()
                return redirect(url_for('login_route.login'))
        else:
            session.clear()
            return redirect(url_for('login_route.login'))

    return wrapper

def esta_autenticado_api(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        operacoes_autenticacao = OperacoesAutenticacao()
        response_data = {"menssagem": "Você não está autorizado a acessar está rota."}
        if request.headers.get('SESSAO_TOKEN') != None:
            SESSAO_TOKEN = json.loads(request.headers['SESSAO_TOKEN'])
            if (operacoes_autenticacao.autenticacao_verificar_chave(SESSAO_TOKEN) == True):
                return func(*args, **kwargs)
            else:
                return make_response(response_data, 401)
        else:
            return make_response({"messagem": "Necessário o envio da autenticação!"}, 401)

    return wrapper