# LIBRARYS
import json
from flask import Blueprint
from flask import request
from flask import make_response
from modulos.modulo_logar.Logar import Logar

# MODULOS
from api_externa import GerarRespostaPadrao
from modulos.modulo_webhhook.WebHook import WebHook

rotas_webhooks = Blueprint('rotas_webhook', __name__, None)

webhook = WebHook()


""" Rota para atualizar o estatus do pagamento da SelfPay através do webhook recebe da API POPPAY """
""" Definida no arquivo rotas_abertas.txt """
@rotas_webhooks.route("/rota/webhook/atualiza/estatus/do/pagamento", methods=['POST'])
def rota_webhook_selfpay_atualiza_estatus_do_pagamento():
    if request.form:
        JSON_DATA = json.loads(request.form['data'])
    else:
        JSON_DATA = json.loads(request.data)
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_WEBHOOK_RECEBENDO_ATUALIZACAO_DE_PAGAMENTO",
        webhook.recebe_retorno_atualiza_estatus_do_pagamento_selfpay(JSON_DATA, request)
    ))