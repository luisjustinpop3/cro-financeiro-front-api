# LIBRARYS
import json
from flask import Blueprint
from flask import request
from flask import make_response

# MODULOS
from api_externa import GerarRespostaPadrao
from decorators.AutenticadoDecorator import esta_autenticado_api
from modulos.modulo_associados.AssociadoOperacoes import AssociadoOperacoes


rotas_associado = Blueprint('rotas_associado', __name__, None)

associado_operacoes = AssociadoOperacoes()

global retorno_mensagem
global retorno_resposta

"""
Rota Associados pagar
"""

@rotas_associado.route("/rota/associado/pagar", methods=['POST'])
@esta_autenticado_api
def rota_associado_pagar():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ASSOCIADO_PAGAR",
        associado_operacoes.associado_executa_pagamento(JSON_DATA)
    ))


"""
Rota Associados listar pendencias
"""

@rotas_associado.route("/rota/associado/listar/pendencias", methods=['POST'])
@esta_autenticado_api
def rota_associado_listar_pendencias():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ASSOCIADO_LISTAR_PENDENCIAS",
        associado_operacoes.associado_listar_pendencias(JSON_DATA)
    ))

"""
Rota Associados listar pagamentos
"""

@rotas_associado.route("/rota/associado/listar/pagamentos", methods=['POST'])
@esta_autenticado_api
def rota_associado_listar_pagamentos():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ASSOCIADO_LISTAR_PAGAMENTOS",
        associado_operacoes.associado_listar_pagamentos(JSON_DATA)
    ))

"""
Rota Associados ajuda
"""

@rotas_associado.route("/rota/associado/ajuda", methods=['POST'])
@esta_autenticado_api
def rota_associado_ajuda():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ASSOCIADO_AJUDA",
        None
    ))

"""
Rota Associados solicitar cancelamento
"""

@rotas_associado.route("/rota/associado/solicitar/cancelamento", methods=['POST'])
@esta_autenticado_api
def rota_associado_solicitar_cancelamento():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ASSOCIADO_SOLICITAR_CANCELAMENTO",
        None
    ))


"""
Rota Associados buscar recibos
"""

@rotas_associado.route("/rota/associado/buscar/recibos", methods=['POST'])
@esta_autenticado_api
def rota_associado_buscar_recibos():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ASSOCIADO_BUSCAR_RECIBOS",
        None
    ))

"""
Rota edição de associado
"""
@rotas_associado.route("/rota/associado/dados", methods=['GET', 'POST'])
@esta_autenticado_api
def rota_associado_recuperar_dados():
    header = json.loads(request.headers["SESSAO_TOKEN"])

    if request.method == "POST":
        email = request.form.get("email")
        telefone = request.form.get("telefone")

        return make_response(GerarRespostaPadrao.criar_resposta(
            False,
            "EDITAR_DADOS",
            associado_operacoes.associado_editar_dados(header["SESSAO_TOKEN"], email, telefone)
        ))

    else:
        return make_response(GerarRespostaPadrao.criar_resposta(
            False,
            "RECUPERAR_DADOS",
            associado_operacoes.associado_recuperar_dados(header["SESSAO_TOKEN"])
        ))

@rotas_associado.route("/rota/associado/detalhes/cobranca/<int:id>", methods=["GET"])
@esta_autenticado_api
def rota_administracao_detalhes_cobranca(id):
    header = json.loads(request.headers["SESSAO_TOKEN"])
    response = associado_operacoes.recuperar_dados_cobranca(id)

    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "DETALHES_COBRANCA",
        response
    ))

@rotas_associado.route("/rota/associado/recuperar/senha/email", methods=['POST'])
@esta_autenticado_api
def rota_associado_recuperar_senha_email():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ASSOCIADO_RECUPERAR_SENHA_EMAIL",
        associado_operacoes.associado_recuperar_senha_email(JSON_DATA)
    ))

@rotas_associado.route("/rota/associado/atualizar/senha", methods=['POST'])
def rota_associado_atualizar_senha():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ASSOCIADO_ATUALIZAR_SENHA",
        associado_operacoes.associado_atualizar_senha(JSON_DATA)
    ))
