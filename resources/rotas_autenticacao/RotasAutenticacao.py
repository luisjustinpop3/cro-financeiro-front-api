# LIBRARYS
import json
from flask import Blueprint
from flask import request
from flask import make_response

# MODULOS
from api_externa import GerarRespostaPadrao
from decorators.AutenticadoDecorator import esta_autenticado_api
from modulos.modulo_autenticacao.FabricaAutenticador import FabricaAutenticador

from modulos.modulo_seguranca.Seguranca import Seguranca

rotas_autenticacao = Blueprint('rotas_autenticacao', __name__, None)

fabrica_autenticador = FabricaAutenticador()

seguranca = Seguranca()

global retorno_mensagem
global retorno_resposta



"""
Rota autenticao autenticar 
"""

@rotas_autenticacao.route("/rota/autenticacao/autenticar", methods=['POST'])
def rota_autenticacao_autenticar():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_AUTENTICACAO_AUTENTICAR",
        fabrica_autenticador.seleciona_tipo_autenticador(JSON_DATA)
    ))

"""         
Rota autenticao deslogar
"""

@rotas_autenticacao.route("/rota/autenticacao/deslogar", methods=['POST'])
@esta_autenticado_api
def rota_autenticacao_deslogar():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_AUTENTICACAO_DESLOGAR",
        fabrica_autenticador.seleciona_tipo_autenticador(JSON_DATA)
    ))

"""         
Rota autenticao verificar se usuario existe
"""

@rotas_autenticacao.route("/rota/autenticacao/verificar/usuario", methods=['POST'])
@esta_autenticado_api
def rota_autenticacao_verificar_usuario():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_AUTENTICACAO_VERIFICAR_USUARIO",
        fabrica_autenticador.seleciona_tipo_autenticador(JSON_DATA)
    ))

"""         
Rota autenticao verificar se senha existe
"""

@rotas_autenticacao.route("/rota/autenticacao/verificar/senha", methods=['POST'])
@esta_autenticado_api
def rota_autenticacao_verificar_senha():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_AUTENTICACAO_VERIFICAR_SENHA",
        fabrica_autenticador.seleciona_tipo_autenticador(JSON_DATA)
    ))


"""
Rota autenticao recuperar senha
"""

@rotas_autenticacao.route("/rota/autenticacao/recuperar/senha", methods=['POST'])
@esta_autenticado_api
def rota_autenticacao_recuperar_senha():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_AUTENTICACAO_RECUPERAR_SENHA",
        None
    ))

"""
Rota autenticao solicitar alterar senha
"""

@rotas_autenticacao.route("/rota/autenticacao/solicitar/alterar/senha", methods=['POST'])
@esta_autenticado_api
def rota_autenticacao_solicitar_alterar_senha():

    JSON_DATA = json.loads(request.form['data'])
    retorno_mensagem = "ACAO_AUTENTICACAO_ALTERAR_SENHA"

    if (seguranca.verificar_permissao(JSON_DATA) == True):
        retorno_resposta = fabrica_autenticador.seleciona_tipo_autenticador(JSON_DATA)
    else:
        retorno_resposta = "SEGURANCA_USUARIO_NAO_POSSUI_ESTA_PERMISSAO"

    return make_response(GerarRespostaPadrao.criar_resposta(False, retorno_mensagem, retorno_resposta))


"""
Rota autenticao alterar senha
"""

@rotas_autenticacao.route("/rota/autenticacao/alterar/senha", methods=['POST'])
@esta_autenticado_api
def rota_autenticacao_alterar_senha():

    JSON_DATA = json.loads(request.form['data'])
    retorno_mensagem = "ACAO_AUTENTICACAO_ALTERAR_SENHA"

    if(seguranca.verificar_permissao(JSON_DATA) == True):
        retorno_resposta = fabrica_autenticador.seleciona_tipo_autenticador(JSON_DATA)
    else:
        retorno_resposta = "SEGURANCA_USUARIO_NAO_POSSUI_ESTA_PERMISSAO"

    return make_response(GerarRespostaPadrao.criar_resposta(False, retorno_mensagem, retorno_resposta))
