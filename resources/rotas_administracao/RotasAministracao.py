# LIBRARYS
import json
import os
from _curses import flash

from flask import Blueprint
from flask import request
from flask import make_response

# MODULOS
from werkzeug.utils import redirect, secure_filename

from decorators.AutenticadoDecorator import esta_autenticado_api
from modulos.modulo_administracao.AdministracaoOperacoesModule import AdministracaoOperacoesModule

from api_externa import GerarRespostaPadrao
from middlewares.OperacoesAutenticacao import OperacoesAutenticacao

from modulos.modulo_seguranca.Seguranca import Seguranca

rotas_administracao = Blueprint('rotas_administracao', __name__, None)

administracao_operacoes_module = AdministracaoOperacoesModule()
operacoes_autenticacao = OperacoesAutenticacao()
seguranca = Seguranca()

"""
Rota Administracao pode gerar cobrancas (extras)
"""


@rotas_administracao.route("/rota/admininstracao/gerar/cobranca", methods=['POST'])
@esta_autenticado_api
def rota_administracao_gerar_cobranca():
    JSON_DATA = json.loads(request.form['data'])
    seguranca.verificar_permissao(JSON_DATA)
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ADMINISTRACAO_GERAR_COBRANCA",
        None
    ))


"""
Rota Administracao pode cancelar uma pagamento
"""


@rotas_administracao.route("/rota/admininstracao/cancela/pagamento", methods=['POST'])
@esta_autenticado_api
def rota_administracao_cancela_pagamento():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ADMINISTRACAO_CANCELA_PAGAMENTO",
        None
    ))


"""
Rota Administracao pode cancelar uma cobranca
"""


@rotas_administracao.route("/rota/admininstracao/cancela/cobranca", methods=['POST'])
@esta_autenticado_api
def rota_administracao_cancela_cobranca():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ADMINISTRACAO_CANCELA_COBRANCA",
        None
    ))


"""
Rota Administracao extornar pagamento
"""


@rotas_administracao.route("/rota/admininstracao/extornar/pagamento", methods=['POST'])
@esta_autenticado_api
def rota_administracao_extornar_pagamento():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ADMINISTRACAO_EXTORNAR_PAGAMENTO",
        None
    ))


"""
Rota Administracao busca lista de todos pagamentos pendentes em um periodo de tempo
"""


@rotas_administracao.route("/rota/admininstracao/busca/todos/pagamentos/pendentes/em/periodo/de/tempo",
                           methods=['POST'])
@esta_autenticado_api
def rota_administracao_busca_todos_pagamentos_pendentes_em_um_periodo_de_tempo():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ADMINISTRACAO_BUSCA_TODOS_PAGAMENTOS_PENDENTES_EM_UM_PERIODO_DE_TEMPO",
        None
    ))


"""
Rota Administracao busca todos pagamentos liberados em periodo de tempo
"""


@rotas_administracao.route("/rota/admininstracao/busca/todos/pagamentos/liberados/em/periodo/de/tempo",
                           methods=['POST'])
@esta_autenticado_api
def rota_administracao_busca_todos_pagamentos_liberados_em_periodo_de_tempo():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ADMINISTRACAO_BUSCA_TODOS_PAGAMENTOS_LIBERADOS_EM_PERIODO_DE_TEMPO",
        None
    ))


"""
Rota Administracao lista totais de pagamentos em periodo de tempo
"""


@rotas_administracao.route("/rota/admininstracao/lista/totais/de/pagamentos/em/periodo/de/tempo", methods=['POST'])
@esta_autenticado_api
def rota_administracao_lista_totais_de_pagamentos_em_periodo_de_tempo():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ADMINISTRACAO_LISTA_TOTAIS_DE_PAGAMENTOS_EM_PERIODO_DE_TEMPO",
        None
    ))


"""
Rota Administracao busca lista de pagamentos com vencimento proximo
"""


@rotas_administracao.route("/rota/admininstracao/busca/lista/pagamentos/com/vencimentos/proximos", methods=['POST'])
@esta_autenticado_api
def rota_administracao_busca_lista_pagamentos_com_vencimentos_proximos():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ADMINISTRACAO_BUSCA_LISTA_PAGAMENTOS_COM_VENCIMENTOS_PROXIMOS",
        None
    ))


"""
Rota Administracao listar lancamentos futuros
"""


@rotas_administracao.route("/rota/admininstracao/listar/lancamentos/futuros", methods=['POST'])
@esta_autenticado_api
def rota_administracao_listar_lancamentos_futuros():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ADMINISTRACAO_LISTAR_LANCAMENTOS_FUTUROS",
        None
    ))


"""
Rota Administracao enviar nova senha para usuario (com dificuldade de acesso)
"""


@rotas_administracao.route("/rota/admininstracao/enviar/nova/senha", methods=['POST'])
@esta_autenticado_api
def rota_administracao_enviar_nova_senha():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ADMINISTRACAO_ENVIAR_NOVA_SENHA",
        None
    ))


"""
Rota Listar todos associados com paginacao e busca
"""


@rotas_administracao.route("/rota/administracao/listar/associados", methods=["GET"], defaults={"pg": 0, "search": None})
@rotas_administracao.route("/rota/administracao/listar/associados/<int:pg>", methods=["GET"], defaults={"search": None})
@rotas_administracao.route("/rota/administracao/listar/associados/<int:pg>/buscar/<string:search>", methods=["GET"])
@rotas_administracao.route("/rota/administracao/listar/associados/buscar/<string:search>", methods=["GET"],
                           defaults={"pg": 0})
@esta_autenticado_api
def rota_administracao_listar_associados(pg, search):
    print(pg)
    header = json.loads(request.headers["SESSAO_TOKEN"])
    response = administracao_operacoes_module.listar_associados(pg, search)

    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "AÇÂO_ADMINISTRACAO_LISTAR_ASSOCIADOS",
        response
    ))


@rotas_administracao.route("/rota/administracao/recuperar/dados/admin", methods=["GET"])
@esta_autenticado_api
def rota_administracao_recuperar_dados_admin():
    header = json.loads(request.headers["SESSAO_TOKEN"])

    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "RECUPERAR_EMAIL_ADMIN",
        administracao_operacoes_module.recuperar_dados_admin(header["SESSAO_TOKEN"])
    ))


@rotas_administracao.route("/rota/administracao/editar/dados/admin", methods=["POST"])
@esta_autenticado_api
def rota_administracao_editar_dados_admin():
    header = json.loads(request.headers["SESSAO_TOKEN"])

    if len(request.form.get("senha")) > 0:
        senha = request.form.get("senha")
    else:
        senha = ""

    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "EDITAR_DADOS_ADMIN",
        administracao_operacoes_module.editar_dados_admin(request.form.get('email'), header["SESSAO_TOKEN"], senha)
    ))


@rotas_administracao.route("/rota/administracao/listar/pagamentos", methods=["GET"], defaults={"pg": 0, "search": None})
@rotas_administracao.route("/rota/administracao/listar/pagamentos/<int:pg>", methods=["GET"], defaults={"search": None})
@rotas_administracao.route("/rota/administracao/listar/pagamentos/<int:pg>/buscar/<string:search>", methods=["GET"])
@rotas_administracao.route("/rota/administracao/listar/pagamentos/buscar/<string:search>", methods=["GET"],
                           defaults={"pg": 0})
@esta_autenticado_api
def rota_administracao_listar_pagamentos(pg, search):
    header = json.loads(request.headers["SESSAO_TOKEN"])
    response = administracao_operacoes_module.listar_pagamentos(pg, search)

    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "LISTAR_PAGAMENTOS",
        response
    ))


@rotas_administracao.route("/rota/administracao/recuperar/dados/associado/<int:id>", methods=["GET"])
@esta_autenticado_api
def rota_administracao_editar_dados_associado(id):
    header = json.loads(request.headers["SESSAO_TOKEN"])
    response = administracao_operacoes_module.recuperar_dados_associado(id)

    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "RECUPERAR_DADOS_DO_ASSOCIADO",
        response
    ))


@rotas_administracao.route("/rota/administracao/editar/associado/<int:id>", methods=["POST"])
@esta_autenticado_api
def rota_administracao_editar_associado(id):
    header = json.loads(request.headers["SESSAO_TOKEN"])
    print(request.values)
    response = administracao_operacoes_module.editar_dados_associados(id, request.form.get('telefone'),
                                                                      request.form.get('email'))

    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "RECUPERAR_DADOS_DO_ASSOCIADO",
        response
    ))


@rotas_administracao.route("/rota/administracao/listar/transacoes", methods=["GET"],
                           defaults={"pg": 0, "search": None, "limit": 10})
@rotas_administracao.route("/rota/administracao/listar/transacoes/limit/<int:limit>", methods=["GET"],
                           defaults={"pg": 0, "search": None})
@rotas_administracao.route("/rota/administracao/listar/transacoes/<int:pg>", methods=["GET"],
                           defaults={"search": None, "limit": 10})
@rotas_administracao.route("/rota/administracao/listar/transacoes/<int:pg>/limit/<int:limit>", methods=["GET"],
                           defaults={"search": None})
@rotas_administracao.route("/rota/administracao/listar/transacoes/<int:pg>/buscar/<string:search>", methods=["GET"],
                           defaults={"limit": 10})
@rotas_administracao.route("/rota/administracao/listar/transacoes/<int:pg>/buscar/<string:search>/limit/<int:limit>",
                           methods=["GET"])
@rotas_administracao.route("/rota/administracao/listar/transacoes/buscar/<string:search>", methods=["GET"],
                           defaults={"pg": 0, "limit": 10})
@rotas_administracao.route("/rota/administracao/listar/transacoes/buscar/<string:search>/limit/<int:limit>",
                           methods=["GET"], defaults={"pg": 0})
@esta_autenticado_api
def rota_administracao_listar_ultimas_transacoes(pg, search, limit=10):
    header = json.loads(request.headers["SESSAO_TOKEN"])
    response = administracao_operacoes_module.listar_pagamentos(pg, search, limit)

    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "LISTAR_ULTIMAS_TRANSACOES",
        response
    ))


@rotas_administracao.route("/rota/administrador/detalhes/transacao/<int:id>", methods=["GET"])
@esta_autenticado_api
def rota_administracao_detalhes_transacao(id):
    header = json.loads(request.headers["SESSAO_TOKEN"])
    response = administracao_operacoes_module.recuperar_detalhes_transacao(id)

    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "DETALHES TRANSACAO",
        response
    ))

"""
    Valor pendencias & Total de Pagamentos
"""
@rotas_administracao.route("/rota/administracao/valores", methods=['POST'])
@esta_autenticado_api
def rota_administracao_valores():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ADMINISTRACAO_VALORES",
        administracao_operacoes_module.administracao_valores(JSON_DATA)
    ))

@rotas_administracao.route("/rota/administracao/valor/liberado/saque", methods=['POST'])
@esta_autenticado_api
def rota_administracao_valor_liberado_saque():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
           False,
           "ACAO_ADMINISTRACAO_VALOR_LIBERADO_SAQUE",
           administracao_operacoes_module.administracao_valor_liberado_saque(JSON_DATA)
    ))

"""
    Valor ha liberar
"""
@rotas_administracao.route("/rota/administracao/valor/ha/liberar", methods=['POST'])
@esta_autenticado_api
def rota_administracao_valor_ha_liberar():
    # JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_ADMINISTRACAO_VALOR_HA_LIBERAR",
        administracao_operacoes_module.administracao_valor_ha_liberar()
    ))

@rotas_administracao.route("/rota/administracao/devedores", methods=["GET"],
                           defaults={"pg": 0, "search": None, "limit": 10})
@rotas_administracao.route("/rota/administracao/devedores/limit/<int:limit>", methods=["GET"],
                           defaults={"pg": 0, "search": None})
@rotas_administracao.route("/rota/administracao/devedores/<int:pg>", methods=["GET"],
                           defaults={"search": None, "limit": 10})
@rotas_administracao.route("/rota/administracao/devedores/<int:pg>/limit/<int:limit>", methods=["GET"],
                           defaults={"search": None})
@rotas_administracao.route("/rota/administracao/devedores/<int:pg>/buscar/<string:search>", methods=["GET"],
                           defaults={"limit": 10})
@rotas_administracao.route("/rota/administracao/devedores/<int:pg>/buscar/<string:search>/limit/<int:limit>",
                           methods=["GET"])
@rotas_administracao.route("/rota/administracao/devedores/buscar/<string:search>", methods=["GET"],
                           defaults={"pg": 0, "limit": 10})
@rotas_administracao.route("/rota/administracao/devedores/buscar/<string:search>/limit/<int:limit>",
                           methods=["GET"], defaults={"pg": 0})
@esta_autenticado_api
def rota_administracao_devedores(pg, search, limit=10):
    header = json.loads(request.headers["SESSAO_TOKEN"])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "DEVEDORES",
        administracao_operacoes_module.administracao_devedores(pg, search, limit)
    ))

@rotas_administracao.route("/rota/administracao/detalhes_transacao/<int:id>", methods=["GET"])
@esta_autenticado_api
def rota_administracao_detalhes_transacao_nova(id):
    header = json.loads(request.headers["SESSAO_TOKEN"])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "DETALHES_TRANSACAO",
        administracao_operacoes_module.administracao_detalhes_transacao(id)
    ))

@rotas_administracao.route("/rota/enviar_dados_de_login/<int:uid>", methods=["GET"], defaults={"send_method": "email"})
@rotas_administracao.route("/rota/enviar_dados_de_login/<int:uid>/<string:send_method>", methods=["GET"])
@esta_autenticado_api
def enviar_dados_de_login(uid, send_method):
    r = administracao_operacoes_module.enviar_login_senha(uid, send_method)
    print(r)
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ENVIAR_DADOS_DE_LOGIN",
        {
            'status': r
        }
    ))