# LIBRARYS
import json
from flask import Blueprint
from flask import request
from flask import make_response

# MODULOS
from api_externa import GerarRespostaPadrao
from middlewares.OperacoesAutenticacao import OperacoesAutenticacao

rotas_operacoes = Blueprint('rotas_operacoes', __name__, None)

operacoes_autenticacao = OperacoesAutenticacao()

global retorno_mensagem
global retorno_resposta

"""
Rota operacoes tokenizar cartao
"""

@rotas_operacoes.route("/rota/operacoes/tokenizar/cartao", methods=['POST'])
def rota_operacoes_tokenizar_cartao():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_OPERACOES_TOKENIZAR_CARTAO",
        None
    ))

"""
Rota operacoes mudar token do cartao
"""

@rotas_operacoes.route("/rota/operacoes/mudar/token/do/cartao", methods=['POST'])
def rota_operacoes_tokenizar_cartao():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_OPERACOES_MUDAR_TOKEN_DO_CARTAO",
        None
    ))

"""
Rota operacoes bloquear token de cartao
"""

@rotas_operacoes.route("/rota/operacoes/bloquear/token/de/cartao", methods=['POST'])
def rota_operacoes_bloquear_token_de_cartao():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_OPERACOES_BLOQUEAR_TOKEN_DE_CARTAO",
        None
    ))


"""
Rota operacoes desbloquear token de cartao
"""

@rotas_operacoes.route("/rota/operacoes/desbloquear/token/de/cartao", methods=['POST'])
def rota_operacoes_desbloquear_token_de_cartao():
    JSON_DATA = json.loads(request.form['data'])
    return make_response(GerarRespostaPadrao.criar_resposta(
        False,
        "ACAO_OPERACOES_DESBLOQUEAR_TOKEN_DE_CARTAO",
        None
    ))


