# CRO Gerenciador Financeiro
Desenvolvido por: POP3 Planning
Equipe: Luis Justin.

## Como declarar rota como fechada
Para declarar um rota como fechada ou aberta o que difere elas é apeans a adição de um decorator, segue o exemplo abaixo:
```python
from flask import app, make_response
from decorators.AutenticadoDecorator import esta_autenticado_api

@app.route("/rota_teste_fechada", methods=["GET"])
@esta_autenticado_api
def rota_teste_fechada():
    return make_response("Rota fechada que necessita autenticação")
```

## Como efetuar autenticação na API
Para efetuar a autenticação na API é necessário que seja enviado via **HEADER** a chave de autenticação, segue abaixo código de exemplo:

```python
import requests
from helpers.HeaderGenerator import generateHeader

response = requests.post("URL DA API", headers=generateHeader("TOKEN_DE_AUTENTICAO"), data="DADOS A SEREM ENVIADOS")

print(response.content)
```
Nota-se que foi utilizado uma função chamama ``` generateHeader() ``` que foi criada para facilitar o processo de geração de header para autenticação na API, você tambem pode gerar manualmente o código de header seguindo o seguinte código abaixo:

```python
session_data = {  
    'SESSAO_TOKEN': token  
}  
headers = {  
    'SESSAO_TOKEN': json.dumps(session_data)  
}
```
## Como utilizar paginação
Para páginação foi criada um classe dedicada chamda Paginator, está classe você precisa passar alguns parametros quando iniciar ela, que serão listado aqui abaixo:
```python
paginator = Paginator(actual_page, total_of_records, actual_url, records_per_page=10, search_param="search")
```
agora listando um a um dos argumentos:
``` actual_page ``` => Página atual geralmente é zero quando entra-se na página que ira ter paginação.
```total_of_records ``` => O numero total de registros no banco de dados, isso pode ser alterado quando é feito uma pesquisa que dai é o numero total de registros que aquela pesquisa retornou.
``` actual_url ``` => Esse argumento é a URL da página atual para ele poder montar os links da  seguinte forma pagina_atual?page=1 esse numero 1 muda conforme a pagina por padrão a pagina 1 não vem o parametro GET ?page.
``` records_per_page ``` => Esse agumento é a paginação em si a quantidade de itens a serem exibidas por padrão ela sempre é 10 mas pode ser alterada a vontade.
``` search_param ``` => Esse argumento é o parametro que será passado por GET que representa a busca por exemplo: ``` pagina_atual?page=2&search=fulano ``` caso queira mudar o parametro search para outro basta definir no search_param.

agora para gerar a paginação é necessário que seja executado o metodo paginate, código de exemplo logo abaixo:
```python
import requests
from helpers.Paginator import Paginator
from helpers.HeaderGenerator import generateHeader
import json

response = requests.post("URL", headers=generateHeader("TOKEN_DO_USUARIO"), data="DADOS_PASSADOS")

data = json.loads(data)
total_records = data["result"]["total_of_records"]

paginator = Paginator(int(actual_page), total_records, "URL_PAGINA_ATUAL", int(limit), "search")

print(paginator.paginate())

RESULTADO DO PRINT :::::::::::::::::::::::::::::::::::::::::::
Código HTML do Bootstrap para paginação
```
## Como utilizar SmtpSender para enviar E-Mails por SMTP
O SmtpSender foi um classe criada para facilitar o processo de envio de menssagens de email por SMTP, onde a pessoa especifica os dados do SMTP, pode carregar um texto para envio ou carregar um template em HTML ou TXT, inclui junto uma template engine própria criada para facilitar a substituição de campos dinamicos no HTML e no TXT logo abaixo irei mostrar exemplos de uso desta Classe:
```python
from helpers.SmtpSender import SmtpSender

# Host do SMTP, Porta do SMTP, Login do SMTP, Senha do SMTP, Usa TTLS ?
smtp_sender = SmtpSender("SMTP_HOST", 25, "SMTP_LOGIN", "SMTP_PASSWORD", False)

#Usado para emails em HTML
smtp_sender.read_html_template("TEMPLATE_LOCATION", name="Fulano", nova_senha="123")
#Usado para emails em PLAIN TEXT
smtp_sender.read_txt_template("TEMPLATE_LOCATION", name="Fulano", nova_senha="123")

# Caso queira enviar um email que esteja em uma variavel pode-se
# usar direto a função que ira efetuar o envio do email, mas somente
# se não foi usado as funções de template, caso contrario a função apenas enviara o email
# HTML MESSAGE e TEXT MESSAGE usar somente se não foi carregado template
email_sended = smtp_sender.send_email("EMAIL_FROM", "EMAIL_TO", "EMAIL TITLE", "HTML MESSAGE", "TEXT MESSAGE")

```
## Como utilizar o SmsSender para envio de SMS
Primeiramente iremos discutir o que é o SmsSender e como funciona. O SmsSender é uma classe criada para envio de SMS em diversos Gateways diferentes ou seja você pode ter 3 empresas de fornecimento de serviço SMS e pode escolher qual delas quer usar em cada momento, primeiro ire explicar como criar o Gateway, após isto irei explicar como utilizar o gateway no envio, por padrão o sistema vem com a classe de envio da Facilita Movel para envios de SMS.
### Como criar um gateway de envio de SMS
Para efetuar a criação de um gateway é necessario que você saiba como seu fornecedor de SMS envia os mesmos. Irei exemplificar a criação mas não a implementação do código de envio de SMS.

Primeiramente para criar o gateway no SmsSender temos uma classe abastrata e genérica que você sempre deve seguir ela para criação é preciso que ela seja extendida no seu gateway, ela possu 3 metodos dentro:

 - init(auth_paramets)
 - compose(msg)
 - send(to)

Agora explicando todos os parametros:
``` __init__(auth_paramets) ``` => metodo padrão do python em todas classes, onde você ira definir variaveis que precisam ser passadas por regra geral foi determinado que só será passado uma variavel chamada ``` auth_params ``` ela é uma lista onde você deve passar tudo que seu gateway ira receber, ou seja você vai passar usuário, keys de envio e etc...

``` compose(msg) ```  => metodo que é para compor a messagem geralmente esse metodo guarda o parametro msg dentro de uma variavel mas isso vai do que você precisa fazer conforme cada fornecedor de SMS, alguns precisa converter para URL ENCODED dai dentro desse metodo você aproveita recebe, converte e guarda em uma variavel de sua classe para mais tarde ser usado.

``` send(to) ``` => metodo que ira ter toda lógica de envio e vai efetuar o envio.

Agora irei mostrar um exemplo que foi criado para ser utilizado com a Facilita Movel SMS:
```python
from helpers.SmsSender.SmsSenderGenerics import SmsSenderGenerics  
from urllib import parse  
from uuid import uuid1  
import requests  
  
class FacilitaMovelGateway(SmsSenderGenerics):  

    def __init__(self, auth_parameters):  
        self._url = "http://www.facilitamovel.com.br/api/simpleSend.ft?user={user}&password={password}&destinatario={destination}&externalkey={ident_key}123&msg={msg}"  
        self._username = auth_parameters["username"]  
        self._password = auth_parameters["password"]  
        self._message = ""  
        self._error_status = {  
              1: "Login Inválido",  
              2: "Usuário sem Créditos",  
              3: "Campo Mensagem e Campo destinatario possuem posicoes diferentes",  
              4: "Campo Mensagem Invalida",  
              6: "Mensagem enviada"  
        }

    def compose(self, msg):  
        if len(msg) > 0:  
            if len(msg) <= 190:  
                self._message = parse.quote(msg)  
                return True  
             else:  
                raise Exception("Message is greather than 190 characters!")  
        else:  
            raise Exception("Message need greather than 1 character.")

    def send(self, destination):  
        if len(self._message) > 0:  
            unique_id = parse.quote(str(uuid1()))  
            prepared_url = self._url.format(user=self._username, password=self._password, destination=destination, ident_key=unique_id, msg=self._message)  
            response = str(requests.get(prepared_url).content)  
            status = int(response.split(";")[0].replace("b'", ""))  
  
            if status != 6:  
                raise Exception("ERRO STATUS => " + str(self._error_status.get(status)))  
            else:  
                return True  
        else:  
            raise Exception("You need compose the message!")
```

### Como adicionar um gateway ao sistema após criar ele
Isto é uma tarefa simples basta abrir o arquivo SmsGateways.py e adicionar no arquivo uma importa igual está:
```python
from helpers.SmsSender.Gateways.FacilitaMovelGateway import FacilitaMovelGateway
```

###  Como enviar utilizando um gateway já criado
Agora iremos enviar um SMS utilizando um gateway já criado, segue o código abaixo:

```python
from helpers.SmsSender.SmsSender import SmsSender  
from helpers.SmsSender import SmsGateways

auth_params = {}  
auth_params["username"] = "fmaciel"  
auth_params["password"] = "asdfasdfa"  
sms_sender = SmsSender(SmsGateways.FacilitaMovelGateway, auth_params)  
sms_sender.read_txt_template('/opt/test.html', nome="Luis", senha="Cabra")  
sms_sender.send('51980533300')
```
