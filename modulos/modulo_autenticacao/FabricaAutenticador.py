from modulos.modulo_autenticacao.AutenticadorSimples import AutenticadorSimples

class FabricaAutenticador:


    global SEGURANCA_PERMITIR_ACESSO

    def __init__(self):
        self.tmp = None


    def seleciona_tipo_autenticador(self, objeto_autenticacao):
        self.retorno = None
        self.autenticador = None

        if(objeto_autenticacao['STR_TIPO'] == 'AUTENTICACAO_SIMPLES'):
            self.autenticador = AutenticadorSimples()
        else:
            self.autenticador = None

        if(objeto_autenticacao["ACAO"] == "ACAO_AUTENTICAR"):
            self.retorno = self.autenticador.logar_usuario(objeto_autenticacao)

        elif(objeto_autenticacao['ACAO']== "ACAO_DESLOGAR"):
            self.retorno = self.autenticador.deslogar_usuario(objeto_autenticacao)

        elif (objeto_autenticacao['ACAO'] == "ACAO_VERIFICAR_USUARIO"):
            self.retorno = self.autenticador.verificar_usuario(objeto_autenticacao)

        elif (objeto_autenticacao['ACAO'] == "ACAO_VERIFICAR_SENHA"):
            self.retorno = self.autenticador.verificar_senha(objeto_autenticacao)

        elif (objeto_autenticacao['ACAO'] == "ACAO_RECUPERAR_SENHA"):
            self.retorno = self.autenticador.operacoes_autenticacao.recuperar_senha(objeto_autenticacao)

        elif (objeto_autenticacao['ACAO'] == "ACAO_AUTENTICACAO_SOLICITAR_ALTERAR_SENHA"):
            self.retorno = self.autenticador.operacoes_autenticacao.solicitar_alteracao_de_senha(objeto_autenticacao)

        elif (objeto_autenticacao['ACAO'] == "ACAO_AUTENTICACAO_ALTERAR_SENHA"):
            self.retorno = self.autenticador.operacoes_autenticacao.alterar_senha(objeto_autenticacao)


        return self.retorno