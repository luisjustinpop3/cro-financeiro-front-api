import hashlib
import random

from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor
from modulos.modulo_autenticacao.OperacoesAutenticacao import OperacoesAutenticacao
from psycopg2.extensions import AsIs

class AutenticadorSimples:

    def __init__(self):
        self.tmp = None
        self.operacoes_autenticacao = OperacoesAutenticacao()

    """
    Autenticar
    """
    def logar_usuario(self, objeto_autenticacao):
        self.permitir = False
        self.sessao_token = None
        self.atualizar = False
        self.chave_gerada_256 = None
        self.tipo_usuario = None
        self.usuario_nome = None

        cb = ConectarBanco()
        db_cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        db_cursor.execute(
            "select * from tb_usuario where str_usuario = %s and str_senha = %s and bol_ativo = 't'",
            [
                objeto_autenticacao['STR_USUARIO'],
                objeto_autenticacao['STR_SENHA']
            ]
        )

        self.rows_usuario = db_cursor.fetchall()
        print(self.rows_usuario)

        if (len(self.rows_usuario) > 0):
            self.permitir = True
            # gera token para sessao
            self.chave_gerada_256 = hashlib.sha256(str(random.getrandbits(256)).encode('utf-8'))
        else:
            self.permitir = False

        db_cursor.close()
        cb.fecha_conexao()

        # salva na sessao
        self.db = ConectarBanco()
        self.db_cursor = self.db.db_conexao.cursor(cursor_factory=RealDictCursor)

        try:

            self.chave_gerada_256 = self.chave_gerada_256.hexdigest()

            self.db_cursor.execute(
                "insert into tb_sessao(str_sessao_token, tb_usuario_id) values(%s, %s); ",
                [
                    self.chave_gerada_256,
                    self.rows_usuario[0]["id"]
                ]
            )

            self.db.db_conexao.commit()

            if (self.db_cursor.rowcount > 0):
                self.permitir = True
            else:
                self.permitir = False

        except Exception as e:
            if(str(e).find("duplicate key value violates constraint")):
                self.atualizar = True
            else:
                self.atualizar = False

            self.permitir = False

        db_cursor.close()
        cb.fecha_conexao()

        if(self.atualizar == True):
            self.db = ConectarBanco()
            self.db_cursor = self.db.db_conexao.cursor(cursor_factory=RealDictCursor)

            try:
                self.db_cursor.execute(
                    "update tb_sessao set str_sessao_token = %s where tb_usuario_id = %s; ",
                    [
                        self.chave_gerada_256,
                        self.rows_usuario[0]["id"]
                    ]
                )

                self.db.db_conexao.commit()

                if (self.db_cursor.rowcount > 0):
                    self.permitir = True
                    self.tipo_usuario = self.rows_usuario[0]["str_tipo"]
                    self.usuario_nome = self.rows_usuario[0]["str_nome"]
                else:
                    self.permitir = False
                    self.tipo_usuario = False

            except Exception as e:
                print("ERRO-SQL :", str(e))
                self.permitir = False

            db_cursor.close()
            cb.fecha_conexao()


        self.tmp_objeto_retorno = {
            "PERMISSAO": self.permitir,
            "SESSAO_TOKEN": self.chave_gerada_256,
            "TIPO USUARIO": self.tipo_usuario,
            "USUARIO_NOME": self.usuario_nome,
        }

        return self.tmp_objeto_retorno

    """
    Deslogar
    """
    def deslogar_usuario(self, objeto_autenticacao):
        self.retorno = None
        self.db = ConectarBanco()
        self.db_cursor = self.db.db_conexao.cursor(cursor_factory=RealDictCursor)
        try:
            self.db_cursor.execute(
                "delete from tb_sessao where str_sessao_token = %s ",
                [
                    objeto_autenticacao['STR_SESSAO_TOKEN']
                ]
            )
            self.db.db_conexao.commit()
            if (self.db_cursor.rowcount > 0):
                self.retorno = True
            else:
                self.retorno = False

        except Exception as e:
            self.retorno = False

        self.db_cursor.close()
        self.db.fecha_conexao()
        return self.retorno

    """
    Verifica se usuario existe
    """
    def verificar_usuario(self, objeto_autenticacao):
        self.retorno = False
        cb = ConectarBanco()
        db_cursor = cb.db_conexao.cursor()
        db_cursor.execute(
            "select * from tb_usuario where str_usuario = %s",
            [
                objeto_autenticacao['STR_USUARIO']
            ]
        )
        rows = db_cursor.fetchall()
        if (len(rows) > 0):
            self.retorno = True
        else:
            self.retorno = False
        db_cursor.close()
        cb.fecha_conexao()
        return self.retorno

    """
    Verifica se a senha existe
    """
    def verificar_senha(self, objeto_autenticacao):
        self.retorno = False
        cb = ConectarBanco()
        db_cursor = cb.db_conexao.cursor()
        db_cursor.execute(
            "select * from tb_usuario where str_senha = %s",
            [
                objeto_autenticacao['STR_SENHA']
            ]
        )
        rows = db_cursor.fetchall()
        if (len(rows) > 0):
            self.retorno = True
        else:
            self.retorno = False
        db_cursor.close()
        cb.fecha_conexao()
        return self.retorno

