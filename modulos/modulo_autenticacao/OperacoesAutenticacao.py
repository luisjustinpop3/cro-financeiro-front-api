import hashlib
import random

from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor

class OperacoesAutenticacao:

    def __init__(self):
        self.tmp = None

    """
    Solicita token-link para alterar senha
    obs: Deve ser por link no e-mail para que apenas o usuario possa solicitar esta alteracao para ele mesmo
    """
    def solicitar_alteracao_de_senha(self, objeto_autenticacao):
        self.chave_gerada_256 = None
        self.aprovado = False
        self.retorno = None
        self.tmp_objeto_retorno = None

        cb = ConectarBanco()

        db_cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        db_cursor.execute(
            "select * from tb_sessao inner join tb_usuario on tb_sessao.tb_usuario_id = tb_usuario.id and tb_sessao.str_sessao_token = %s",
            [
                objeto_autenticacao['STR_SESSAO_TOKEN']
            ]
        )

        self.rows = db_cursor.fetchall()

        if (len(self.rows) > 0):
            self.aprovado = True
        else:
            self.aprovado = False

        db_cursor.close()
        cb.fecha_conexao()


        if(self.aprovado == True):

            self.db = ConectarBanco()
            self.db_cursor = self.db.db_conexao.cursor(cursor_factory=RealDictCursor)

            self.chave_gerada_256 = hashlib.sha256(str(random.getrandbits(256)).encode('utf-8'))

            try:
                self.db_cursor.execute(
                    "insert into tb_alterar_senha(tb_usuario_id, str_token_link) values(%s, %s)",
                    [
                        self.rows[0]['tb_usuario_id'],
                        self.chave_gerada_256.hexdigest()
                    ]
                )

                self.db.db_conexao.commit()

                if (self.db_cursor.rowcount > 0):
                    self.retorno = self.chave_gerada_256.hexdigest()
                else:
                    self.retorno = None

            except Exception as e:
                print("ERRO-SQL :", str(e))
                self.retorno = None

            db_cursor.close()
            cb.fecha_conexao()

        self.tmp_objeto_retorno = {
            "TOKEN_LINK": self.retorno
        }

        return self.tmp_objeto_retorno

    """
    Recuperar a senha para um e-mail por e-mail ou sms
    deve ser verificado um perido de tempo de recuperacao para evitar multiplas requisicoes em curto tempo
    obs: Nao e necessario estar logado porem, a senha so deve ser alterada mediante token-link no e-mail    
    """
    def recuperar_senha(self, objeto_autenticacao):
        return None

    """
    Alteraa senha do usuario para uma nova e necessario passar a velha senha e a nova
    validacoes de repetir a senha sao incrementadas no FRONT-END
    obs: Para alterar a senha e necessario estar logado
    """
    def alterar_senha(self, objeto_autenticacao):
        print("@@@@@@@@@@@@@@---ALTERANDO-A-SENHA", objeto_autenticacao)
        """
        0 e necessario ter um token-link de acesso
        1 pega o id do usuario pela sesao token
        2 verifica se a senha atual esta correta
        3 muda a senha para a nova utilizando update
        4 depois de alterado com sucesso no final deste processo deleta tb_alterar_senha
        """

        

        return "alterando a senha..."
