import json
import requests as requests
from psycopg2.extras import RealDictCursor
from modulos.modulo_logar.Logar import Logar
from modulos.conexao_pgsql.ConectarBanco import ConectarBanco


class WebHook:

    def _init_(self):
        # INICIA OBJETO DE LOGS
        # self.LOGAR = Logar() (precisa o arquivo na pasta a tabela e importar ele antes de usar)
        self.retorno = None
        self.headers = None
        self.executar = False
        self.retorno_status = None
        self.request = None
        self.tmp_json_cobranca = None
        self.rows_webhook_cobranca = None

    """
    Recebe retorno do estatus do pagamento da API SELFPAY que recebe atraves da API POPPAY
    """

    def recebe_retorno_atualiza_estatus_do_pagamento_selfpay(self, tmp_json_retorno_financeira, request):

        json_completo = json.loads(tmp_json_retorno_financeira)
        json_hash_identificacao = json_completo['txt_cobranca_identificacao_de_referencia']

        self.retorno = {}
        self.headers = None
        self.request = None
        self.executar = False
        self.retorno_status = None
        self.tmp_json_cobranca = None
        self.rows_webhook_cobranca = None

        tmp_referencia = json_hash_identificacao.split("[#]")
        str_chave_de_acesso = tmp_referencia[0]
        txt_cobranca_hash_identificar_cobranca = tmp_referencia[1]
        txt_hash_unico_gerado = tmp_referencia[2]

        self.SINAL_RETORNADO_DA_SITUACAO_DO_PAGAMENTO = None

        if (json_completo['tb_situacao_da_cobranca_str_situacao_da_cobranca'] == 'COBRANCA_APROVADA_AGUARDANDO_RETORNO'):
            self.SINAL_RETORNADO_DA_SITUACAO_DO_PAGAMENTO = 'COBRANCA_APROVADA_AGUARDANDO_RETORNO'

        if (json_completo['tb_situacao_da_cobranca_str_situacao_da_cobranca'] == 'COBRANCA_EXTORNADA'):
            self.SINAL_RETORNADO_DA_SITUACAO_DO_PAGAMENTO = 'COBRANCA_EXTORNADA'

        cb = ConectarBanco()
        db_cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

        try:

            db_cursor.execute(
                "update tb_cobranca set "
                "tb_situacao_da_cobranca_str_situacao_da_cobranca = %s, "
                "txt_retorno_financeira = %s "
                "where str_chave_de_acesso = %s "
                #"and txt_cobranca_hash_identificar_cobranca = %s "
                "and txt_hash_unico_gerado = %s ",
                [
                    self.SINAL_RETORNADO_DA_SITUACAO_DO_PAGAMENTO,
                    json.dumps(tmp_json_retorno_financeira),
                    str_chave_de_acesso,
                    #txt_cobranca_hash_identificar_cobranca,
                    txt_hash_unico_gerado
                ]
            )

            cb.db_conexao.commit()

            db_cursor.execute("UPDATE ")

            self.retorno = "{'status':'200'}"

        except Exception as e:
            pass

        db_cursor.close()
        cb.fecha_conexao()

        return self.retorno