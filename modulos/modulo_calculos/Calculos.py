import urllib.request, json
from datetime import date, datetime

class Calculos():
  """
    @param tipo: tipo da cobrança - Anuidade e Multa Eleitoral [ ANU | ME ]
    @param ano: ano de vencimento da cobrança
    @param categoria: categoria do profissional [ CD | TPD | TSB | ABS | APD | EPAO | EPO | LB ]
    @param data_inscricao: data da inscrição do profissional ou clínica
    @param razao: passar None ou valor da razão social se a categoria for EPAO, EPO ou LB
    @return: JSON com o valor final, juros, multa, valor pagamento: JSON com boleto, cartão avista, cartão 2 à 6, cartão 7 à 12
  """
  def __init__(self, tipo, ano, categoria, data_inscricao, razao):
    # VARIÁVEIS DE CONFIGURAÇÃO DE TAXAS
    self.TAXAS = {'boleto': 0.02208, 'credito_avista': 0.0259, 'credito_26_parc': 0.0366, 'credito_712_parc': 0.0398}
    self.PERCENTUAIS = {'juros': 0.01, 'multa_max': 0.2, 'multa_dia': 0.0033}
    self.DESCONTOS = {'juros_avista': 0.5, 'multa_avista': 0.0, 'juros_26_parc': 0.6, 'multa_26_parc': 0.2, 'juros_712_parc': 0.7, 'multa_712_perc': 0.4}
    self.VENC_ME = json.loads('{"2000": "10/01/2001", "2002": "30/08/2002", "2004": "18/03/2005", "2006": "19/09/2006", "2008": "19/03/2009", "2010": "10/06/2011", "2012": "31/08/2012", "2013": "30/06/2014", "2016": "14/12/2016", "2018": "01/10/2018"}')
    self.CAT_PF = {'CD': 1.0, 'APD': 0.1, 'ASB': 0.1, 'TPD': 0.666666667, 'TSB': 0.2}
    self.CAT_PJ = {'LB': {'A50K': 0.333333333, '50K200K': 0.666666667, '200K500K': 1.0, '500K1M': 1.333333333, '1M2M': 1.666666667, '2M10M': 2.0, '10M': 2.666666667}, 'EPAO': {'A50K': 1.0, '50K200K': 2.0, '200K500K': 3.0, '500K1M': 4.0, '1M2M': 5.0, '2M10M': 6.0, '10M': 8.0}, 'EPO': {'A50K': 1.0, '50K200K': 2.0, '200K500K': 3.0, '500K1M': 4.0, '1M2M': 5.0, '2M10M': 6.0, '10M': 8.0}}
    self.ANUIDADE = {'1984': 13.75, '1985': 14.44, '1986': 18.05, '1987': 8.94, '1988': 23.16, '1989': 26.13, '1990': 37.62, '1991': 113.38, '1992': 92.03, '1993': 121.07, '1994': 121.07, '1995': 203.01, '1996': 199.00, '1997': 183.00, '1998': 183.00, '1999': 200.00, '2000': 200.00, '2001': 200.00, '2002': 216.80, '2003': 238.48, '2004': 262.32, '2005': 278.06, '2006': 294.74, '2007': 305.06, '2008': 324.04, '2009': 343.48, '2010': 343.48, '2011': 343.48, '2012': 343.48, '2013': 343.48, '2014': 377.14, '2015': 401.07, '2016': 459.63, '2017': 503.52, '2018': 503.52}
    self.MULTA_ELEITORAL = {'1994': 40.36, '1996': 66.33, '1998': 61.00, '2000': 66.67, '2002': 72.27, '2004': 87.44, '2006': 98.25, '2008': 108.01, '2010': 114.49, '2012': 114.49, '2013': 114.49, '2016': 153.21, '2018': 167.84}

    if tipo == 'ANU':
      valor = self.categoria(ano, categoria, data_inscricao, razao)
    elif tipo == 'ME':
      for index in self.MULTA_ELEITORAL:
        if index == ano:
          valor = self.MULTA_ELEITORAL[index]
    print(valor)
    self.valor_corrigido = self.selic(tipo, valor, ano)
    print(self.valor_corrigido)
    self.valor_total = self.desconto(self.valor_corrigido, self.juros(self.valor_corrigido), self.multa(tipo, self.valor_corrigido, ano))
    print(self.valor_total)
    #return self.valor_total



  """
    Acessa a API do BC e trás os valores diários da Selic, soma os valores para geral o valor mensal e aplica sobre o valor bruto
    @param tipo: tipo da cobrança - Anuidade e Multa Eleitoral [ ANU | ME ]
    @param valor: valor bruto (sem aplicação de Selic)
    @param ano: ano de vencimento
    @return: valor corrigido pela Selic
  """
  def selic(self, tipo, valor, ano) -> float:
    if tipo == 'ANU':
      vencimento = '31/03/' + ano
    elif tipo == 'ME':
      for index in self.VENC_ME:
        if index == ano:
          vencimento = self.VENC_ME[index]
    final = '31/'+ str(int(date.today().strftime("%m"), base=10)) +'/'+ str(date.today().strftime("%Y"))
    with urllib.request.urlopen("https://api.bcb.gov.br/dados/serie/bcdata.sgs.4390/dados?formato=json&dataInicial={vencimento}&dataFinal={final}".format(vencimento=vencimento, final=final)) as url:
      data = json.loads(url.read().decode())
      selic = 0
      for item in data:
        selic = selic + float(item['valor'])
    valor_corrigido = valor + (valor * (selic / 100))
    return valor_corrigido

  """
    Porcentagem de C{PERC_JUROS} sobre o valor corrigido pela Selic
    @param valor: valor já corrigido pela Selic
    @return: valor do juros
  """
  def juros(self, valor) -> float:
    self.juros = valor * self.PERCENTUAIS['juros']
    return self.juros

  """
    Multa: se dias corridos contados de 01/04 do ano da anuidade vencida até o dia de hoje:
    Maior que 60 dias: multa de [PERC_MULTA_MAX]
    Menor ou igual 60 dias: multa de [PERC_MULTA_DIA] ao dia
    @param tipo: tipo da cobrança - Anuidade e Multa Eleitoral [ ANU | ME ]
    @param valor: valor já corrigido pela Selic
    @param ano: ano de vencimento
    @return: valor da multa
  """
  def multa(self, tipo, valor, ano) -> float:
    if tipo == 'ANU':
      data_multa = '01/04/' + ano
    elif tipo == 'ME':
      for index in self.VENC_ME:
        if index == ano:
          data_multa = self.VENC_ME[index]
    dias_corridos = abs((datetime.strptime(date.today().strftime('%d/%m/%Y'), '%d/%m/%Y') - datetime.strptime(data_multa, '%d/%m/%Y')).days)
    if dias_corridos > 60:
      self.multa = (valor + self.juros) * self.PERCENTUAIS['multa_max']
    elif dias_corridos <= 60:
      self.multa = ((valor + self.juros) * self.PERCENTUAIS['multa_dia']) * dias_corridos
    return self.multa

  """
    À vista: desconto de [DESC_MULTA_AVISTA] na multa e [DESC_JUROS_AVISTA] no juros e taxa de pagamento de [TAXAS[boleto e credito_avista]]
    2 a 6 parcelas: desconto de [DESC_MULTA_26_PARCELA] na multa e [DESC_JUROS_26_PARCELA] no juros e taxa de pagamento de [TAXAS[credito_26_parc]]
    7 a 12 parcelas: desconto de [DESC_MULTA_712_PARCELA] na multa e [DESC_JUROS_712_PARCELA] nos juros e taxa de pagamento de [TAXAS[credito_712_parc]]
    @param valor: valor já corrigido pela selic
    @param juros: valor já calculado do juros
    @param multa: valor já calculado da multa
    @return: json com o valor já aplicado o desconto e já calculado para cada tipo de pagamento
  """
  def desconto(self, valor, juros, multa) -> json:
    final = {}
    for i, index in enumerate(self.TAXAS, start=0):
      if i == 0 or i == 1:
        valor_final = valor + (juros * self.DESCONTOS['juros_avista']) + (multa * self.DESCONTOS['multa_avista'])
        final[index] = round(valor_final + (valor_final * self.TAXAS[index]), ndigits=2)
      elif i == 2:
        valor_final = valor + (juros * self.DESCONTOS['juros_26_parc']) + (multa * self.DESCONTOS['multa_26_parc'])
        final[index] = round(valor_final + (valor_final * self.TAXAS[index]), ndigits=2)
      elif i == 3:
        valor_final = valor + (juros * self.DESCONTOS['juros_712_parc']) + (multa * self.DESCONTOS['multa_712_perc'])
        final[index] = round(valor_final + (valor_final * self.TAXAS[index]), ndigits=2)
    return final

  """
    Pega o valor da anuidade conforme categoria do profissional ou clínica e se é valor um proporcional aplica o desconto relativo
    @param ano: ano de vencimento da cobrança
    @param categoria: categoria do profissional [ CD | TPD | TSB | ABS | APD | EPAO | EPO | LB ]
    @param data_inscricao: data da inscrição do profissional ou clínica
    @param razao: passar None ou valor da razão social se a categoria for EPAO, EPO ou LB
    @return: valor da anuidade conforme as especificações
  """
  def categoria(self, ano, categoria, data_inscricao, razao) -> float:
    valor_cat = 0
    valor_anu = 0
    for index in self.ANUIDADE:
      if index == ano:
        valor_anu = self.ANUIDADE[index]
    if razao == None:
      for index in self.CAT_PF:
        if index == categoria:
          valor_cat = valor_anu * self.CAT_PF[index]
    elif razao != None:
      for index in self.CAT_PJ:
        if index == categoria:
          for index_x in self.CAT_PJ[index]:
            if index_x == razao:
              valor_cat = valor_anu * self.CAT_PJ[index][index_x]
    if ano == str(datetime.strptime(data_inscricao, '%d/%m/%Y').year):
      valor_cat = self.proporcional(valor_cat, ano, data_inscricao)
    return valor_cat

  """
    Calcula o valor retroativo caso o ano de vencimento se o mesmo que o de inscrição
    @param valor: valor bruto da anuidade (sem aplicação de Selic)
    @param ano: ano de vencimento da cobrança
    @param data_inscricao: data da inscrição do profissional ou clínica
    return: valor da anuidade proporcional aos meses de inscrição do ano da cobrança
  """
  def proporcional(self, valor, ano, data_inscricao) -> float:
    prop = valor * ((12 - datetime.strptime(data_inscricao, '%d/%m/%Y').month) / 12)
    return prop

# x = Calculos('ANU', '2018', 'CD', '13/11/2015', None) # <-- ANUIDADE PESSOA FISICA
# x = Calculos('ANU', '2017', 'EPAO', '13/11/2011', '200K500K') # <-- ANUIDADE PESSOA JURIDICA
#x = Calculos('ME', '2018', 'CD', '13/11/2013', None) # <-- MULTA ELEITORAL