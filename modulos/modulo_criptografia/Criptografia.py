import base64

import Crypto
from Crypto.Cipher import PKCS1_OAEP, PKCS1_v1_5
from Crypto.PublicKey import RSA
import psycopg2
from psycopg2.extras import RealDictCursor
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding


class Criptografia:

    def __init__(self):
        self.private_key = None
        self.public_key = None

        random = Crypto.Random.new().read
        self._private_key = RSA.generate(1024, random)
        self._public_key = self._private_key.publickey()
        self._signer = PKCS1_v1_5.new(self._private_key)



    """
    Encripta
    """

    def encripta(self, message):
        encrypted = self.public_key.encrypt(
            message,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA512()),
                algorithm=hashes.SHA512(),
                label=None
            )
        )
        return encrypted

    """
    Decripta
    """

    def decripta(self, encrypted):
        original_message = self.private_key.decrypt(
            encrypted,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA512()),
                algorithm=hashes.SHA512(),
                label=None
            )
        )
        return original_message


    """
    Gerar chaves RSA (certificados)
    """
    def gera_chaves_privada_e_publica(self):

        self.gerada_private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )

        self.gerada_public_key = self.gerada_private_key.public_key()

        self.pem_private_key = self.gerada_private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        )

        self.pem_public_key = self.gerada_public_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )

        rsa_para_de_chaves = {
            "chave_privada": self.pem_private_key,
            "chave_publica": self.pem_public_key
        }

        return rsa_para_de_chaves