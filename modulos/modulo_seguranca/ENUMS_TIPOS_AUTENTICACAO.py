import enum

class ENUNS_TIPOS_AUTENTICACAO(enum.Enum):

    TIPO_API = "TIPO_API"
    TIPO_APLICATIVO = "TIPO_APLICATIVO"
    TIPO_SITE = "TIPO_SITE"

    def ENUN_TIPO_API():
        return ENUNS_TIPOS_AUTENTICACAO.TIPO_API

    def ENUN_TIPO_APLICATIVO():
        return ENUNS_TIPOS_AUTENTICACAO.TIPO_APLICATIVO

    def ENUN_TIPO_SITE():
        return ENUNS_TIPOS_AUTENTICACAO.TIPO_SITE
