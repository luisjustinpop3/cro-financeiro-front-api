from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor

class Seguranca:

    def __init__(self):
        self.retorno = None


    def verificar_permissao(self, objeto_requisicao):

        self.rows_tb_sessao = None
        self.permitido = False

        cb = ConectarBanco()

        # busca id do usuario logado pela sessao do usuario
        db_cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        db_cursor.execute(
            "select * from tb_sessao where str_sessao_token = %s",
            [
                objeto_requisicao['STR_SESSAO_TOKEN'],
            ]
        )

        self.rows_tb_sessao = db_cursor.fetchall()

        # verifica se o id do usuario possui a permissao especifica em ACAO no json rescebido

        db_cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        db_cursor.execute(
            "select	tb_permissao.str_permissao_alias from tb_permissao "
            "inner join tb_conecta_permissao_ao_perfil "
            "on tb_conecta_permissao_ao_perfil.tb_permissao_id = tb_permissao.id "
            "inner join tb_perfil "
            "on tb_perfil.id = tb_conecta_permissao_ao_perfil.tb_perfil_id "
            "inner join tb_conecta_perfil_ao_grupo "
            "on tb_conecta_perfil_ao_grupo.id = tb_perfil.id "
            "inner join tb_conecta_usuario_ao_grupo "
            "on tb_conecta_usuario_ao_grupo.tb_grupo_id = tb_conecta_perfil_ao_grupo.tb_grupo_id "
            "where tb_conecta_usuario_ao_grupo.tb_usuario_id = %s "
            "and tb_permissao.str_permissao_alias = %s ",
            [
                self.rows_tb_sessao[0]['tb_usuario_id'],
                objeto_requisicao['ACAO']
            ]
        )

        self.rows_permissoes = db_cursor.fetchall()

        if(len(self.rows_permissoes) > 0 and self.rows_permissoes[0]['str_permissao_alias'] == objeto_requisicao['ACAO']):
            self.permitido = True
        else:
            self.permitido = False

        db_cursor.close()
        cb.fecha_conexao()

        print("@@@@@@@@@@@@@@@@2-seguranca->:", self.permitido)

        return self.permitido

    def criar_sessao(self, objeto_autenticacao):
        return None

    def destruir_sessao(self, objeto_autenticacao):
        return None

    def gerar_certificados_paraSessao(self, objeto_autenticacao):
        return None

    def verificar_antropia(self, objeto_autenticacao):
        return None

    def bloqueia_ip(self, objeto_autenticacao):
        return None

    def adicionar_antropiaBloco(self, objeto_autenticacao):
        return None

    def assinatura_certificado_do_sistema(self, str_tex_texto):
        return None

    def assinatura_certificado_da_sessao(self, objeto_autenticacao, str_tex_texto):
        return None

    def verificaAssinaturaCertificadoDoSistema(texTexto):
        return None

    def verificaAssinaturaCertificadoDaSessao(textTexto):
        return None
