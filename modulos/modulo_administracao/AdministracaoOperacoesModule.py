from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor
from flask import current_app
import requests, json
import hashlib
import json
import math

import random
import requests
import config
from flask import app

from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor
from flask_restful import abort
import datetime
from datetime import date, datetime
from currencies import Currency
from collections import OrderedDict
from collections import Counter
from datetime import date, datetime
import pathlib

from helpers.SmtpSender import SmtpSender


class AdministracaoOperacoesModule:

    def __init__(self):
        self.retorno = None

    def listar_associados(self, page, search=""):

        cb = ConectarBanco()

        cursor_1 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

        total = 0

        response = []

        if search != None:
            # Buscando algo
            search = "%" + str(search) + "%"
            sql = "SELECT * FROM tb_usuario WHERE str_tipo != 'SISTEMA' AND (str_inscricao LIKE %s OR str_nome LIKE %s OR str_email_1 LIKE %s) ORDER BY id DESC LIMIT 10 OFFSET %s"
            sql2 = "SELECT * FROM tb_usuario WHERE str_tipo != 'SISTEMA' AND (str_inscricao LIKE %s OR str_nome LIKE %s OR str_email_1 LIKE %s) "
            cursor_1.execute(sql, [
                search,
                search,
                search,
                page
            ])

            cur_fetch = cursor_1.fetchall()

            for d in cur_fetch:
                data = {
                    "ID": d["id"],
                    "Nome": d["str_nome"],
                    "Inscricao": d["str_inscricao"],
                    "Email": d["str_email_1"],
                    "Telefone": d["str_telefone_1"]
                }

                response.append(data)

            cursor_1.execute(sql2, [
                search,
                search,
                search
            ])

            total = cursor_1.rowcount

        else:
            # Sem buscar nada
            sql = "SELECT * FROM tb_usuario WHERE str_tipo != 'SISTEMA' ORDER BY id DESC LIMIT 10 OFFSET %s"
            sql2 = "SELECT * FROM tb_usuario WHERE str_tipo != 'SISTEMA' "
            cursor_1.execute(sql, [
                page
            ])

            cur_fetch = cursor_1.fetchall()

            for d in cur_fetch:
                data = {
                    "ID": d["id"],
                    "Nome": d["str_nome"],
                    "Inscricao": d["str_inscricao"],
                    "Email": d["str_email_1"],
                    "Telefone": d["str_telefone_1"]
                }

                response.append(data)

            cursor_1.execute(sql2)
            total = cursor_1.rowcount

            cursor_1.close()
            cb.fecha_conexao()

        return {
            "resultados": response,
            "total": total
        }

    def recuperar_dados_admin(self, hash):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        sql = "SELECT u.str_email_1 FROM tb_usuario as u INNER JOIN tb_sessao ts on u.id = ts.tb_usuario_id WHERE ts.str_sessao_token = %s LIMIT 1"

        cursor.execute(sql, [hash])

        email = cursor.fetchone()["str_email_1"]

        cursor.close()
        cb.fecha_conexao()

        return {
            "email": email
        }

    def editar_dados_admin(self, email, hash, senha=None):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        sql = ""

        if len(senha) > 0:
            sql = "UPDATE tb_usuario SET str_email_1 = %s, str_senha = %s WHERE id = (SELECT tb_usuario_id FROM tb_sessao WHERE str_sessao_token = %s)"
            cursor.execute(sql, [
                email,
                senha,
                hash
            ])
        else:
            sql = "UPDATE tb_usuario SET str_email_1 = %s WHERE id = (SELECT tb_usuario_id FROM tb_sessao WHERE str_sessao_token = %s)"
            cursor.execute(sql, [
                email,
                hash
            ])

        cb.db_conexao.commit()
        cursor.close()
        cb.fecha_conexao()

        return {
            "updated": True
        }

    def listar_pagamentos(self, page, search="", limit=10):
        cb = ConectarBanco()

        cursor_1 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

        total = 0

        response = []

        if search != None:
            # Buscando algo
            search = "%" + str(search) + "%"

            sql = "SELECT u.str_nome, c.str_inscricao, p.dou_valor, p.str_ano, c.id  FROM tb_cobranca as c INNER JOIN tb_pendencia p ON p.id = c.tb_pendencia_id INNER JOIN tb_usuario u on u.str_inscricao = c.str_inscricao  WHERE c.bol_concluida = true AND (u.str_inscricao LIKE %s OR u.str_nome LIKE %s) ORDER BY c.dat_data_registro DESC LIMIT %s OFFSET %s"
            sql2 = "SELECT u.str_nome, c.str_inscricao, p.dou_valor, p.str_ano, c.id  FROM tb_cobranca as c INNER JOIN tb_pendencia p ON p.id = c.tb_pendencia_id INNER JOIN tb_usuario u on u.str_inscricao = c.str_inscricao  WHERE c.bol_concluida = true AND (u.str_inscricao LIKE %s OR u.str_nome LIKE %s) ORDER BY c.dat_data_registro DESC"
            cursor_1.execute(sql, [
                search,
                search,
                limit,
                page
            ])

            cur_fetch = cursor_1.fetchall()

            for d in cur_fetch:
                data = {
                    "id": d["id"],
                    "str_nome": d["str_nome"],
                    "str_inscricao": d["str_inscricao"],
                    "dou_valor": d["dou_valor"],
                    "str_ano": d["str_ano"]
                }

                response.append(data)

            cursor_1.execute(sql2, [
                search,
                search
            ])

            total = cursor_1.rowcount

        else:
            # Sem buscar nada
            sql = "SELECT u.str_nome, c.str_inscricao, p.dou_valor, p.str_ano, c.id  FROM tb_cobranca as c INNER JOIN tb_pendencia p ON p.id = c.tb_pendencia_id INNER JOIN tb_usuario u on u.str_inscricao = c.str_inscricao  WHERE c.bol_concluida = true ORDER BY c.dat_data_registro DESC LIMIT %s OFFSET %s"
            sql2 = "SELECT u.str_nome, c.str_inscricao, p.dou_valor, p.str_ano, c.id  FROM tb_cobranca as c INNER JOIN tb_pendencia p ON p.id = c.tb_pendencia_id INNER JOIN tb_usuario u on u.str_inscricao = c.str_inscricao  WHERE c.bol_concluida = true ORDER BY c.dat_data_registro DESC "
            cursor_1.execute(sql, [
                limit,
                page
            ])

            cur_fetch = cursor_1.fetchall()

            for d in cur_fetch:
                data = {
                    "id": d["id"],
                    "str_nome": d["str_nome"],
                    "str_inscricao": d["str_inscricao"],
                    "dou_valor": d["dou_valor"],
                    "str_ano": d["str_ano"]
                }

                response.append(data)

            cursor_1.execute(sql2)
            total = cursor_1.rowcount

            cursor_1.close()
            cb.fecha_conexao()

        return {
            "resultados": response,
            "total": total
        }

    def recuperar_dados_associado(self, id):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

        # Nome, inscrição, Email, Telefone
        sql = "SELECT u.id, u.str_nome, u.str_inscricao, u.str_email_1, u.str_telefone_1 FROM tb_usuario as u WHERE id = %s LIMIT 1"
        cursor.execute(sql, [
            id
        ])

        data = cursor.fetchone()

        cursor.close()
        cb.fecha_conexao()

        return {
            "Id": data["id"],
            "Nome": data["str_nome"],
            "Inscricao": data["str_inscricao"],
            "Email": data["str_email_1"],
            "Telefone": data["str_telefone_1"]
        }

    def editar_dados_associados(self, id, tel, email):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

        if len(tel) > 0 and len(email) > 0:
            # ambos
            sql = "UPDATE tb_usuario SET str_telefone_1 = %s, str_email_1 = %s WHERE id = %s"
            cursor.execute(sql, [
                tel,
                email,
                id
            ])
        elif len(tel) > 0:
            sql = "UPDATE tb_usuario SET str_telefone_1 = %s WHERE id = %s"
            cursor.execute(sql, [
                tel,
                id
            ])
        else:
            sql = "UPDATE tb_usuario SET str_email_1 = %s WHERE id = %s"
            cursor.execute(sql, [
                email,
                id
            ])

        cb.db_conexao.commit()
        cursor.close()
        cb.fecha_conexao()

        return {
            "updated": True
        }

    def recuperar_detalhes_transacao(self, id_transacao):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

        sql = "SELECT c.id, u.str_nome, u.str_cep, u.str_endereco, p.str_ano, c.lon_cobranca_valor_total, c.int_cobranca_numero_de_parcelas, c.dat_data_registro FROM tb_cobranca as c INNER JOIN tb_usuario u ON u.str_inscricao = c.str_inscricao INNER JOIN tb_pendencia p ON p.id = c.tb_pendencia_id WHERE c.id = %s"
        cursor.execute(sql, [
            id_transacao
        ])

        d = cursor.fetchone()

        return {
            'id': d["id"],
            'nome': d["str_nome"],
            'cep': d["str_cep"],
            'endereco': d["str_endereco"],
            'ano': d["str_ano"],
            'valor': d["lon_cobranca_valor_total"],
            'parcelas': d["int_cobranca_numero_de_parcelas"],
            'data_pagamento': d["dat_data_registro"]
        }

    def administracao_devedores(self, page=0, search="", limit=10):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        sql_contagem_padrao = "SELECT * FROM tb_pendencia p WHERE p.bol_paga = false"
        sql_contagem_busca = "SELECT u.str_inscricao as inscricao, u.str_nome as nome, p.bol_paga as paga, p.dou_valor as valor, p.str_ano as ano, u.str_telefone_1 as telefone FROM tb_pendencia as p INNER JOIN tb_usuario u ON u.str_inscricao = p.str_inscricao WHERE p.bol_paga = false AND (u.str_nome LIKE %s OR u.str_inscricao LIKE %s OR u.str_email_1 LIKE %s AND u.str_telefone_1 LIKE %s)"
        sql_padrao = "SELECT u.str_inscricao as inscricao, u.str_nome as nome, p.bol_paga as paga, p.dou_valor as valor, p.str_ano as ano, u.str_telefone_1 as telefone FROM tb_pendencia as p INNER JOIN tb_usuario u ON u.str_inscricao = p.str_inscricao WHERE p.bol_paga = false LIMIT %s OFFSET %s"
        sql_busca = "SELECT u.str_inscricao as inscricao, u.str_nome as nome, p.bol_paga as paga, p.dou_valor as valor, p.str_ano as ano, u.str_telefone_1 as telefone FROM tb_pendencia as p INNER JOIN tb_usuario u ON u.str_inscricao = p.str_inscricao WHERE p.bol_paga = false AND (u.str_nome LIKE %s OR u.str_inscricao LIKE %s OR u.str_email_1 LIKE %s AND u.str_telefone_1 LIKE %s) LIMIT %s OFFSET %s"
        if search != None:
            # Quando tem busca
            search = "%{}%".format(search)
            cursor.execute(sql_busca, [
                search,
                search,
                search,
                search,
                limit,
                page
            ])
            data = cursor.fetchall()
            cursor.execute(sql_contagem_busca, [
                search,
                search,
                search,
                search
            ])
            count = cursor.rowcount
        else:
            # Quando nao tem busca
            cursor.execute(sql_padrao, [
                limit,
                page
            ])
            data = cursor.fetchall()
            cursor.execute(sql_contagem_padrao)
            count = cursor.rowcount

        cursor.close()
        cb.fecha_conexao()

        return {
            'data': data,
            'total': count
        }

    def administracao_detalhes_transacao(self, id):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        sql = "SELECT c.id as id, u.str_nome as nome, u.str_inscricao as inscricao, c.lon_cobranca_valor_total as valor, c.str_ano as ano FROM tb_cobranca as c INNER JOIN tb_usuario u ON u.str_inscricao = c.str_inscricao WHERE c.id = %s LIMIT 1"
        cursor.execute(sql, [
            id
        ])
        d = cursor.fetchone()
        cursor.close()
        cb.fecha_conexao()
        return d

    def administracao_valores(self, objeto_autenticacao):
        # ROTA ADMINISTRACAO VALORES - http://127.0.0.1:4000/rota/administracao/valores
        # Headers: SESSAO_TOKEN {"SESSAO_TOKEN":"7636f0a38d7bf63d42c943aacefb872cdb4d464074fbfc968b8a67f8befaeb19"}
        # Body: data {"ACAO": "ACAO_ADMINISTRACAO_VALORES","STR_SESSAO_TOKEN":"7636f0a38d7bf63d42c943aacefb872cdb4d464074fbfc968b8a67f8befaeb19","DATA_INICIAL":"16/12/2019","DATA_FINAL":"17/12/2019"}
        self.retorno = None
        self.tmp_objeto_retorno = None
        cb = ConectarBanco()
        db_cursor_1 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        db_cursor_2 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

        #self.data_inicial = datetime.strptime(objeto_autenticacao['DATA_INICIAL'], '%d/%m/%Y').date()
        self.data_inicial = objeto_autenticacao['DATA_INICIAL']

        #self.data_final = datetime.strptime(objeto_autenticacao['DATA_FINAL'], '%d/%m/%Y').date()
        self.data_final = objeto_autenticacao['DATA_FINAL']

        db_cursor_1.execute(
            "select SUM(tb_pendencia.dou_valor) as total_pendencias from tb_pendencia "
            "where tb_pendencia.dat_data_de_registro::date "
            "between %s and %s and tb_pendencia.bol_paga='FALSE'",
            [
                self.data_inicial,
                self.data_final
            ]
        )
        db_cursor_2.execute(
            "select SUM(tb_pendencia.dou_valor) as total_pagamentos from tb_pendencia "
            "where tb_pendencia.dat_data_de_registro::date "
            "between %s and %s and tb_pendencia.bol_paga='TRUE'",
            [
                self.data_inicial,
                self.data_final
            ]
        )

        self.rows_pendencia = db_cursor_1.fetchall()

        self.pendencias = []

        for row_pendencia in self.rows_pendencia:
            temporario_pendencias = {
                "VALOR_EM_PENDENCIAS": "%.2f" % (row_pendencia['total_pendencias']),

            }
            self.pendencias.append(temporario_pendencias)

        self.rows_pagamento = db_cursor_2.fetchall()

        self.pagamentos = []

        for row_pagamento in self.rows_pagamento:
            temporario_pagamentos = {
                "TOTAL_DE_PAGAMENTOS": "%.2f" % (row_pagamento['total_pagamentos']),
            }
            self.pagamentos.append(temporario_pagamentos)

        self.tmp_objeto_retorno = {
            "VALORES ": [
                self.pendencias,
                self.pagamentos,
            ]
        }

        return self.tmp_objeto_retorno

    def administracao_valor_liberado_saque(self, objeto_autenticacao):

        print(objeto_autenticacao)

        #self.data_inicial = datetime.strptime(objeto_autenticacao['DATA_INICIAL'], "%d/%m/%Y").strftime("%Y-%m-%d")
        self.data_inicial = objeto_autenticacao['DATA_INICIAL']
        #self.data_final = datetime.strptime(objeto_autenticacao['DATA_FINAL'], "%d/%m/%Y").strftime("%Y-%m-%d")
        self.data_final = objeto_autenticacao['DATA_FINAL']
        self.headers = {
            'Content-Type': 'application/json',
            'Accept': '/',
            'Host': 'localhost',
            'Accept-Encoding': 'gzip, deflate',
            'Connection': 'keep-alive',
            'cache-control': 'no-cache',
            'OBJETO_SEGURANCA': '{"SEGURANCA_CHAVE_DE_ACESSO":"'+config.SEGURANCA_CHAVE_DE_ACESSO+'"}'
        }
        self.params = {"OBJETO_SEGURANCA": {
            "SEGURANCA_CHAVE_DE_ACESSO": config.SEGURANCA_CHAVE_DE_ACESSO}}

        try:
            self.data = {
                "ACAO": "ACAO_VENDEDOR_BUSCAR_LANCAMENTOS_FUTUROS",
                "SEGURANCA_CHAVE_DE_ACESSO": config.SEGURANCA_CHAVE_DE_ACESSO,
                "PAGINA": "0",
                "CODIGO": "13",
                "DATA_INICIAL": self.data_inicial,
                "DATA_FINAL": self.data_final
            }
            self.retorno = json.loads(requests.post("http://"+config.HOST_POPPAY+":"+config.PORTA_POPPAY+"/rota/vendedor/buscar/lancamentos/futuros/financeira", params=self.params, headers=self.headers, data=json.dumps(self.data)).text)
            self.total_de_paginas = self.retorno["retorno"][0]["TRANS_PAGINAS"]["totalPages"]
            self.total = 0
            self.contador = 0
            self.codigo = 13

            for x in range(self.total_de_paginas):
                self.data = {
                    "ACAO": "ACAO_VENDEDOR_BUSCAR_LANCAMENTOS_FUTUROS",
                    "SEGURANCA_CHAVE_DE_ACESSO": config.SEGURANCA_CHAVE_DE_ACESSO,
                    "PAGINA": self.contador,
                    "CODIGO": self.codigo,
                    "DATA_INICIAL": self.data_inicial,
                    "DATA_FINAL": self.data_final
                }
                self.retorno = json.loads(requests.post("http://"+config.HOST_POPPAY+":"+config.PORTA_POPPAY+"/rota/vendedor/buscar/lancamentos/futuros/financeira", params=self.params,headers=self.headers, data=json.dumps(self.data)).text)

                # PORCENTAGEM PARA CRO - 67%
                # PORCENTAGEM PARA CFO - 33%

            for self.pagamento in self.retorno["retorno"]:
                self.total += float(self.pagamento["TRANS_VALOR"])
                self.total_pagamentos_para_cro = (self.total * 67) / 100
                self.total_pagamentos_para_cfo = (self.total * 33) / 100

                self.tmp_objeto_retorno_1 = "%.2f" % (self.total)
                self.tmp_objeto_retorno_2 = "%.2f" % (self.total_pagamentos_para_cro)
                self.tmp_objeto_retorno_3 = "%.2f" % (self.total_pagamentos_para_cfo)
                self.tmp_objeto_retorno_4 = "%.2f" % (self.total_pagamentos_para_cro + self.total_pagamentos_para_cfo)

                self.contador += 1
                return {
                    "VALOR_LIBERADO_PARA_SAQUE": self.tmp_objeto_retorno_1,
                    "TOTAL_PAGAMENTOS_PARA_CRO": self.tmp_objeto_retorno_2,
                    "TOTAL_PAGAMENTOS_PARA_CFO": self.tmp_objeto_retorno_3,
                    "TOTAL_PAGAMENTOS_CRO+CFO": self.tmp_objeto_retorno_4
                }

        except Exception as e:
            self.tmp_objeto_retorno_1 = 0.00
            self.tmp_objeto_retorno_2 = 0.00
            self.tmp_objeto_retorno_3 = 0.00
            self.tmp_objeto_retorno_4 = 0.00
            return {
                    "VALOR_LIBERADO_PARA_SAQUE": self.tmp_objeto_retorno_1,
                    "TOTAL_PAGAMENTOS_PARA_CRO": self.tmp_objeto_retorno_2,
                    "TOTAL_PAGAMENTOS_PARA_CFO": self.tmp_objeto_retorno_3,
                    "TOTAL_PAGAMENTOS_CRO+CFO": self.tmp_objeto_retorno_4
            }

    def administracao_valor_ha_liberar(self, objeto_autenticacao):
        # ROTA ADMINISTRACAO VALOR HÁ LIBERAR
        # http://127.0.0.1:4000/rota/administracao/valor/ha/liberar
        # Body: data {"DATA_INICIAL":"01/01/2019","DATA_FINAL":"30/01/2020"}

        #self.data_inicial = datetime.strptime(objeto_autenticacao['DATA_INICIAL'], "%d/%m/%Y").strftime("%Y-%m-%d")
        self.data_inicial = objeto_autenticacao['DATA_INICIAL']
        #self.data_final = datetime.strptime(objeto_autenticacao['DATA_FINAL'], "%d/%m/%Y").strftime("%Y-%m-%d")
        self.data_final = objeto_autenticacao['DATA_FINAL']
        self.headers = {
            'Content-Type': 'application/json',
            'Accept': '/',
            'Host': 'localhost',
            'Accept-Encoding': 'gzip, deflate',
            'Connection': 'keep-alive',
            'cache-control': 'no-cache',
            'OBJETO_SEGURANCA': '{"SEGURANCA_CHAVE_DE_ACESSO":"' + config.SEGURANCA_CHAVE_DE_ACESSO + '"}'
        }
        self.params = {"OBJETO_SEGURANCA": {
            "SEGURANCA_CHAVE_DE_ACESSO": config.SEGURANCA_CHAVE_DE_ACESSO}}

        try:
            self.data = {
                "ACAO": "ACAO_VENDEDOR_BUSCAR_LANCAMENTOS_FUTUROS",
                "SEGURANCA_CHAVE_DE_ACESSO": config.SEGURANCA_CHAVE_DE_ACESSO,
                "PAGINA": "0",
                "CODIGO": "14",
                "DATA_INICIAL": self.data_inicial,
                "DATA_FINAL": self.data_final
            }
            self.retorno = json.loads(requests.post(
                "http://" + config.HOST_POPPAY + ":" + config.PORTA_POPPAY + "/rota/vendedor/buscar/lancamentos/futuros/financeira",
                params=self.params, headers=self.headers, data=json.dumps(self.data)).text)
            self.total_de_paginas = self.retorno["retorno"][0]["TRANS_PAGINAS"]["totalPages"]
            self.total = 0
            self.contador = 0
            self.codigo = 14
            for x in range(self.total_de_paginas):
                self.data = {
                    "ACAO": "ACAO_VENDEDOR_BUSCAR_LANCAMENTOS_FUTUROS",
                    "SEGURANCA_CHAVE_DE_ACESSO": config.SEGURANCA_CHAVE_DE_ACESSO,
                    "PAGINA": self.contador,
                    "CODIGO": self.codigo,
                    "DATA_INICIAL": self.data_inicial,
                    "DATA_FINAL": self.data_final

                }
                self.retorno = json.loads(requests.post(
                    "http://" + config.HOST_POPPAY + ":" + config.PORTA_POPPAY + "/rota/vendedor/buscar/lancamentos/futuros/financeira",
                    params=self.params, headers=self.headers, data=json.dumps(self.data)).text)

            for self.pagamento in self.retorno["retorno"]:
                self.total += float(self.pagamento["TRANS_VALOR"])
                self.tmp_objeto_retorno = "%.2f" % (self.total)
                self.contador += 1

            return {
                "VALOR_A_LIBERAR": self.tmp_objeto_retorno
            }
        except Exception as e:
            print("ERROR : ", str(e))
            return {
                "VALOR_A_LIBERAR": 0.00
            }

    """
    Enviar Login e senha por email ou sms
    """

    def enviar_login_senha(self, uid, send_method="email"):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        sql = "SELECT str_nome, str_email_1, str_inscricao, str_senha FROM tb_usuario WHERE id = %s"
        cursor.execute(sql, [
            uid
        ])
        dados = cursor.fetchone()
        cursor.close()
        cb.fecha_conexao()
        if send_method == "email":
            if len(dados["str_email_1"]) > 0:
                smtp_sender = SmtpSender(current_app.config["SMTP_HOST"], current_app.config["SMTP_PORT"],
                                         current_app.config["SMTP_USER"], current_app.config["SMTP_PASS"],
                                         current_app.config["SMTP_TTLS"])

                smtp_sender.read_html_template('/templates/cliente/dados_de_login.html', nome=dados["str_nome"],
                                               inscricao=dados["str_inscricao"], senha=dados["str_senha"])
                smtp_sender.read_txt_template('/templates/cliente/dados_de_login.html', nome=dados["str_nome"],
                                              inscricao=dados["str_inscricao"], senha=dados["str_senha"])

                smtp_sender.send_email("nao-responder@poppay.co", dados["str_email_1"], "Seus dados de login")
                return "EMAIL_SENDED"
            else:
                return "EMAIL_EMPTY"
        else:
            print("SMS não implementado")