import hashlib
import random
from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor
from datetime import date, datetime
import datetime
import csv
from threading import Thread

class AdministracaoOperacoes(Thread):

    def __init__(self, order_data, delimiter, token):
        Thread.__init__(self)
        self.order_data = order_data
        self.delimiter = delimiter
        self.token = token


    def run(self):
        print("Emitindo notificação!")
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        cursor.execute(
            "INSERT INTO tb_notificacoes (type, message, status, token) VALUES ('IMPORT_STARTED', 'Importação Iniciada', 0, 0)")
        cb.db_conexao.commit()
        cursor.close()
        cb.fecha_conexao()
        print("Iniciando Processamento")
        self.processar()
        print("Processamento Finalizado")

    def processar(self):
        cb = ConectarBanco()

        with open('/opt/ARQUIVOS_FINANCEIRO/lista_usuarios.csv') as csvfile:
            readCSV = csv.reader(csvfile, delimiter=self.delimiter)
            next(readCSV, None)

            for row in readCSV:

                if len(row) > 0:
                    #print(row)

                    chave_gerada_256 = hashlib.sha256(str(random.getrandbits(256)).encode('utf-8') + datetime.datetime.now().strftime("%Y-%m-%d %H:%M").encode('utf-8'))
                    #print("HASH DE IDENTIFICACAO DE USUARIO : ", chave_gerada_256.hexdigest())

                    str_inscricao = ""
                    str_nome = ""
                    str_endereco = ""
                    str_bairro = ""
                    str_uf = ""
                    str_municipio = ""
                    str_cep = ""
                    str_telefone1 = ""
                    str_telefone2 = ""
                    str_cpf_cnpj = ""
                    str_debito = ""
                    str_insc_resp_tec = ""
                    str_nome_resp_tec = ""
                    str_ident = ""
                    str_emissor = ""
                    str_uf_resp_tec = ""
                    str_estado_civil = ""
                    str_sexo = ""
                    str_nacionalidade = ""
                    str_cancelado = ""
                    str_email1 = ""
                    str_email2 = ""
                    str_data_cro = ""
                    str_capital_social = ""
                    str_data_requerimento = ""

                    for order in self.order_data:

                        order["index"] = int(order["index"])
                        order["selected"] = int(order["selected"])
                        #print(order)

                        if order["index"] == 0:
                            str_inscricao = row[order["selected"]]
                        elif order["index"] == 1:
                            str_nome = row[order["selected"]]
                        elif order["index"] == 2:
                            str_endereco = row[order["selected"]]
                        elif order["index"] == 3:
                            str_bairro = row[order["selected"]]
                        elif order["index"] == 4:
                            str_uf = row[order["selected"]]
                        elif order["index"] == 5:
                            str_municipio = row[order["selected"]]
                        elif order["index"] == 6:
                            str_cep = row[order["selected"]]
                        elif order["index"] == 7:
                            str_telefone1 = row[order["selected"]]
                        elif order["index"] == 8:
                            str_telefone2 = row[order["selected"]]
                        elif order["index"] == 9:
                            str_cpf_cnpj = row[order["selected"]]
                        elif order["index"] == 10:
                            str_debito = row[order["selected"]]
                        elif order["index"] == 11:
                            str_insc_resp_tec = row[order["selected"]]
                        elif order["index"] == 12:
                            str_nome_resp_tec = row[order["selected"]]
                        elif order["index"] == 13:
                            str_ident = row[order["selected"]]
                        elif order["index"] == 14:
                            str_emissor = row[order["selected"]]
                        elif order["index"] == 15:
                            str_uf_resp_tec = row[order["selected"]]
                        elif order["index"] == 16:
                            str_estado_civil = row[order["selected"]]
                        elif order["index"] == 17:
                            str_sexo = row[order["selected"]]
                        elif order["index"] == 18:
                            str_nacionalidade = row[order["selected"]]
                        elif order["index"] == 19:
                            str_cancelado = row[order["selected"]]
                        elif order["index"] == 20:
                            str_email1 = row[order["selected"]]
                        elif order["index"] == 21:
                            str_email2 = row[order["selected"]]
                        elif order["index"] == 22:
                            str_data_cro = row[order["selected"]]
                        elif order["index"] == 23:
                            str_capital_social = row[order["selected"]]
                        elif order["index"] == 24:
                            str_data_requerimento = row[order["selected"]]
                        else:
                            raise Exception("Option not exist!")

                    # str_inscricao = row[0]
                    # str_nome = row[1]
                    # str_endereco = row[2]
                    # str_bairro = row[3]
                    # str_uf = row[4]
                    # str_municipio = row[5]
                    # str_cep = row[6]
                    # str_telefone1 = row[7]
                    # str_telefone2 = row[8]
                    # str_cpf_cnpj = row[9]
                    # str_debito = row[10]
                    # str_insc_resp_tec = row[11]
                    # str_nome_resp_tec = row[12]
                    # str_ident = row[13]
                    # str_emissor = row[14]
                    # str_uf_resp_tec = row[15]
                    # str_estado_civil = row[16]
                    # str_sexo = row[17]
                    # str_nacionalidade = row[18]
                    # str_cancelado = row[19]
                    # str_email1 = row[20]
                    # str_email2 = row[21]
                    # str_data_cro = row[22]
                    # str_capital_social = row[23]
                    # str_data_requerimento = row[24]
                    str_tipo = ""

                    if len(str_cpf_cnpj) > 0:
                        tmp_doc = str_cpf_cnpj.replace('.', '')
                        tmp_doc = tmp_doc.replace('-', '')
                        tmp_doc = tmp_doc.replace('/', '')
                        if len(tmp_doc) == 11:
                            str_tipo = "PF"
                        elif len(tmp_doc) == 14:
                            str_tipo = "PJ"
                        else:
                            str_tipo = ""


                    try:

                        db_cursor_1 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
                        db_cursor_1.execute("SELECT * FROM tb_usuario WHERE str_inscricao = %s", [
                            str_inscricao
                        ])

                        usuario_row = db_cursor_1.fetchone()

                    except Exception as e:
                        print("ERRO SELECT 1: " + str(e))

                    if db_cursor_1.rowcount > 0:
                        #Usuário existe no banco de dados

                        print("Atualizando usuário: " + str(str_inscricao))
                        try:
                            db_cursor_2 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
                            db_cursor_2.execute("update tb_usuario set "
                                                "str_email_1 = %s, "
                                                "str_tipo = %s, "
                                                "str_inscricao = %s,"
                                                "str_email_2 = %s, "
                                                "str_nome = %s, "
                                                "str_endereco = %s, "
                                                "str_bairro = %s, "
                                                "str_uf = %s, "
                                                "str_municipio = %s, "
                                                "str_cep = %s, "
                                                "str_telefone_1 = %s, "
                                                "str_telefone_2 = %s, "
                                                "str_cpf_cnpj = %s, "
                                                "str_debito = %s, "
                                                "str_inscrespttec = %s, "
                                                "str_nomeresptec = %s, "
                                                "str_ident = %s, "
                                                "str_emissor = %s, "
                                                "str_estcivil = %s, "
                                                "str_sexo = %s, "
                                                "str_nacionalidade = %s, "
                                                "str_cancelado = %s, "
                                                "str_dat_cro = %s, "
                                                "str_capital_social = %s, "
                                                "str_data_requerimento = %s, "
                                                "str_tipo_pf_ou_pj = %s "
                                                "where str_inscricao = %s ",
                                                [
                                                    str_email1,
                                                    "PADRAO",
                                                    str_inscricao,
                                                    str_email2,
                                                    str_nome,
                                                    str_endereco,
                                                    str_bairro,
                                                    str_uf,
                                                    str_municipio,
                                                    str_cep,
                                                    str_telefone1,
                                                    str_telefone2,
                                                    str_cpf_cnpj,
                                                    str_debito,
                                                    str_insc_resp_tec,
                                                    str_nome_resp_tec,
                                                    str_ident,
                                                    str_emissor,
                                                    str_estado_civil,
                                                    str_sexo,
                                                    str_nacionalidade,
                                                    str_cancelado,
                                                    str_data_cro,
                                                    str_capital_social,
                                                    str_data_requerimento,
                                                    str_tipo,
                                                    str_inscricao
                                                ])
                        except Exception as e:
                            print("ERRO AO ATUALIZAR NO BANCO: " + str(e))

                        db_cursor_2.close()
                        db_cursor_1.close()

                    else:
                        #Usuário não existe

                        db_cursor_1.close()

                        print("Inserindo usuário: " + str(str_inscricao))

                        try:
                            db_cursor_3 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
                            db_cursor_3.execute(
                                "insert into tb_usuario "
                                "("
                                "str_usuario,"
                                "str_senha,"
                                "str_email_1,"
                                "str_tipo,"
                                "str_inscricao,"
                                "str_email_2,"
                                "str_nome,"
                                "str_endereco,"
                                "str_bairro,"
                                "str_uf,"
                                "str_municipio,"
                                "str_cep,"
                                "str_telefone_1,"
                                "str_telefone_2,"
                                "str_cpf_cnpj,"
                                "str_debito,"
                                "str_inscrespttec,"
                                "str_nomeresptec,"
                                "str_ident,"
                                "str_emissor,"
                                "str_estcivil,"
                                "str_sexo,"
                                "str_nacionalidade,"
                                "str_cancelado,"
                                "str_dat_cro,"
                                "str_capital_social,"
                                "str_data_requerimento,"
                                "str_tipo_pf_ou_pj,"
                                "str_hash_identifica_o_cliente"
                                ") values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                                [
                                    str_inscricao,
                                    "123",
                                    str_email1,
                                    "PADRAO",
                                    str_inscricao,
                                    str_email2,
                                    str_nome,
                                    str_endereco,
                                    str_bairro,
                                    str_uf,
                                    str_municipio,
                                    str_cep,
                                    str_telefone1,
                                    str_telefone2,
                                    str_cpf_cnpj,
                                    str_debito,
                                    str_insc_resp_tec,
                                    str_nome_resp_tec,
                                    str_ident,
                                    str_emissor,
                                    str_estado_civil,
                                    str_sexo,
                                    str_nacionalidade,
                                    str_cancelado,
                                    str_data_cro,
                                    str_capital_social,
                                    str_data_requerimento,
                                    str_tipo,
                                    chave_gerada_256.hexdigest()
                                ]
                            )

                            cb.db_conexao.commit()

                        except Exception as e:
                            print ("ERRO AO INSERIR USUARIO NO DB: " + str(e))

                        db_cursor_3.close()


                    print("Atualizando dados de debito usuário: " + str(str_inscricao))
                    data_splited = str_debito.split("@")
                    for d in data_splited:
                        d_splited = d.split("#")
                        tmp_ano = d_splited[0]
                        tmp_titulo = d_splited[1]
                        tmp_valor = d_splited[2].replace('.', '').replace(',', '.').replace('R$', '')
                        print("-------------------------")
                        print("ANO: " + str(d_splited[0]))
                        print("TITULO: " + str(d_splited[1]))
                        print("VALOR: " + str(d_splited[2]))
                        print("-------------------------")

                        try:
                            db_cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
                            sql = "SELECT id FROM tb_pendencia WHERE str_inscricao = %s AND str_ano = %s AND str_tipo_de_cobranca = %s AND bol_paga = false"
                            db_cursor.execute(sql, [
                                str_inscricao,
                                tmp_ano,
                                tmp_titulo
                            ])
                            tmp_pendencia_id = db_cursor.fetchone()
                            print(tmp_pendencia_id)
                        except Exception as e:
                            print("Exception 357: " + str(e))



                        if db_cursor.rowcount > 0:
                            print("ENTREI")
                            sql = "UPDATE tb_pendencia SET dou_valor = %s WHERE id = %s"
                            db_cursor.execute(sql, [
                                tmp_valor,
                                tmp_pendencia_id["id"]
                            ])
                            cb.db_conexao.commit()
                        else:
                            hash_identifica_a_pendencia = hashlib.sha256(
                                str(random.getrandbits(256)).encode('utf-8') + datetime.datetime.now().strftime(
                                    "%Y-%m-%d %H:%M").encode('utf-8'))

                            try:
                                db_cursor_5 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
                                sql = "INSERT INTO tb_pendencia (str_inscricao, str_ano, str_tipo_de_cobranca, dou_valor, str_hash_identifica_a_pendencia) VALUES (%s, %s, %s, %s, %s)"
                                db_cursor_5.execute(sql, [
                                    str_inscricao,
                                    tmp_ano,
                                    tmp_titulo,
                                    tmp_valor,
                                    hash_identifica_a_pendencia.hexdigest()
                                ])
                                cb.db_conexao.commit()
                                db_cursor_5.close()
                            except Exception as e:
                                print("ERRO AO INSERIR PENDENCIA " + str(e))

        cursor_notification = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        cursor_notification.execute("INSERT INTO tb_notificacoes (type, message, status, token) VALUES ('IMPORT_FINISHED', 'Importação Concluída', 0, %s)", [
            self.token
        ])
        cb.db_conexao.commit()

        cursor_notification.execute("UPDATE tb_notificacoes SET status=1 WHERE type='IMPORT_STARTED' AND status=0")
        cb.db_conexao.commit()

        cursor_notification.close()

        cb.fecha_conexao()