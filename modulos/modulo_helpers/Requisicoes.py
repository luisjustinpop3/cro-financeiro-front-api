import base64, json
import requests as requests
from psycopg2.extras import RealDictCursor
from requests.auth import HTTPBasicAuth

from modulos.conexao_pgsql.ConectarBanco import ConectarBanco

class Requisicoes:

    def __init__(self):
        self.retorno = None

    def requisicao_post_json_api_cobranca_pagamento(self, url, data, chave_da_loja):

        self.headers = {
            'Content-Type': 'application/json',
            'User-Agent': 'PostmanRuntime/7.17.1',
            'OBJETO_SEGURANCA': json.dumps({"SEGURANCA_CHAVE_DE_ACESSO": chave_da_loja}),
            'Accept': '*/*',
            'Accept-Encoding': 'gzip, deflate',
            'Connection': 'keep-alive',
            'cache-control': 'no-cache'
        }

        self.json_data = {
            "data": data
        }

        self.tmp_json = {}

        try:
            self.tmp_json = json.loads(requests.post(url, headers=self.headers, data=json.dumps(self.json_data)).text)
        except Exception as e:
            print(str(e))

        return self.tmp_json
