import json
from datetime import datetime

from currencies import Currency
from flask import url_for

from modulos.modulo_calculos.Calculos import Calculos
import hashlib
import random

from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor
from flask_restful import abort

import datetime
from datetime import date, datetime

from modulos.modulo_helpers.Requisicoes import Requisicoes

import config

import json
import smtplib
import threading
import time
from datetime import datetime
import hashlib
import random
import miniupnpc

from currencies import Currency
from modulos.modulo_calculos.Calculos import Calculos
from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor
from flask_restful import abort
import datetime
from datetime import date, datetime
from modulos.modulo_helpers.Requisicoes import Requisicoes

import config
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from helpers.SmtpSender import SmtpSender

class AssociadoOperacoes:

    def __init__(self):
        self.retorno = None
        self.helpers_requisicoes = Requisicoes()

    """
    Executa o pagamento calculando o valor utilizando o algoritimo Calculos
    """

    def associado_listar_pendencias(self, objeto_autenticacao):
        self.retorno = None
        self.tmp_objeto_retorno = None

        cb = ConectarBanco()

        db_cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

        db_cursor_2 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

        db_cursor.execute(
            "select tb_pendencia.id,tb_pendencia.dat_data_de_registro,tb_pendencia.str_inscricao,tb_pendencia.str_tipo_de_cobranca,tb_pendencia.bol_paga,tb_pendencia.str_ano,tb_usuario.str_nome "
            "from tb_usuario inner join tb_sessao on tb_sessao.tb_usuario_id = tb_usuario.id "
            "inner join tb_pendencia on tb_pendencia.str_inscricao = tb_usuario.str_usuario and tb_sessao.str_sessao_token  =  %s "
            " where tb_pendencia.bol_paga = 'FALSE' order by tb_pendencia.id ",
            [
                objeto_autenticacao['STR_SESSAO_TOKEN']
            ]
        )

        total_pendencias = db_cursor.rowcount

        db_cursor_2.execute(
            "select tb_pendencia.id,tb_pendencia.dat_data_de_registro,tb_pendencia.str_inscricao,tb_pendencia.str_tipo_de_cobranca,tb_pendencia.bol_paga,tb_pendencia.str_ano,tb_usuario.str_nome "
            "from tb_usuario inner join tb_sessao on tb_sessao.tb_usuario_id = tb_usuario.id "
            "inner join tb_pendencia on tb_pendencia.str_inscricao = tb_usuario.str_usuario and tb_sessao.str_sessao_token  =  %s "
            " where tb_pendencia.bol_paga = 'FALSE' order by tb_pendencia.id limit %s offset %s ",
            [
                objeto_autenticacao['STR_SESSAO_TOKEN'],
                objeto_autenticacao['INT_LIMIT'],
                objeto_autenticacao['INT_OFFSET'],
            ]
        )

        self.rows_pendencia = db_cursor_2.fetchall()

        self.pendencias = []

        for x in self.rows_pendencia:

            pendencia_bol_paga = ""

            if (x["bol_paga"] == True):
                pendencia_bol_paga = 'PAGO'
            else:
                pendencia_bol_paga = 'NAO PAGO'

            tmp = {
                "ID PENDENCIA": x["id"],
                "ASSOCIADO": x["str_nome"],
                "INSCRICAO ASSOCIADO": x["str_inscricao"],
                "STATUS PENDENCIA": pendencia_bol_paga,
                "ANO PENDENCIA": x["str_ano"]

            }

            self.pendencias.append(tmp)

        self.tmp_objeto_retorno = {
            "PENDENCIAS": self.pendencias,
            "TOTAL PENDENCIAS": total_pendencias
        }

        return self.tmp_objeto_retorno

    def associado_listar_pagamentos(self, objeto_autenticacao):
        self.retorno = None
        self.tmp_objeto_retorno = None
        cb = ConectarBanco()

        db_cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        db_cursor_2 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

        db_cursor.execute(
            "select tb_pendencia.id,tb_pendencia.dat_data_de_registro,tb_pendencia.str_inscricao,tb_pendencia.str_tipo_de_cobranca,tb_pendencia.bol_paga,tb_pendencia.str_ano,tb_usuario.str_nome "
            "from tb_usuario inner join tb_sessao on tb_sessao.tb_usuario_id = tb_usuario.id "
            "inner join tb_pendencia on tb_pendencia.str_inscricao = tb_usuario.str_usuario and tb_sessao.str_sessao_token  =  %s "
            " where tb_pendencia.bol_paga = 'TRUE' order by tb_pendencia.id ",
            [
                objeto_autenticacao['STR_SESSAO_TOKEN']
            ]
        )

        total_pagamentos = db_cursor.rowcount

        db_cursor_2.execute(
            "select tb_pendencia.dou_valor, tb_pendencia.id,tb_pendencia.dat_data_de_registro,tb_pendencia.str_inscricao,tb_pendencia.str_tipo_de_cobranca,tb_pendencia.bol_paga,tb_pendencia.str_ano,tb_usuario.str_nome "
            "from tb_usuario inner join tb_sessao on tb_sessao.tb_usuario_id = tb_usuario.id "
            "inner join tb_pendencia on tb_pendencia.str_inscricao = tb_usuario.str_usuario and tb_sessao.str_sessao_token  =  %s "
            " where tb_pendencia.bol_paga = 'TRUE' order by tb_pendencia.id limit %s offset %s ",
            [
                objeto_autenticacao['STR_SESSAO_TOKEN'],
                objeto_autenticacao['INT_LIMIT'],
                objeto_autenticacao['INT_OFFSET']
            ]
        )

        self.rows_pagamento = db_cursor_2.fetchall()

        self.pagamentos = []

        for row_pagamento in self.rows_pagamento:

            self.pagamento_tipo = 'CARTAO'

            self.tipo_moeda = Currency('BRL')

            self.pagamento_valor = self.tipo_moeda.get_money_format(row_pagamento["dou_valor"])

            if (self.pagamento_valor == 'R$ 0'):
                self.pagamento_valor = 'R$ 0,0'

            temporario = {
                "ID PAGAMENTO": row_pagamento["id"],
                "ASSOCIADO": row_pagamento["str_nome"],
                "INSCRICAO_ASSOCIADO": row_pagamento["str_inscricao"],
                "VALOR_PAGAMENTO": self.pagamento_valor,
                "FORMA_PAGAMENTO": self.pagamento_tipo,
                "ANO_PAGAMENTO": row_pagamento["str_ano"],

            }

            self.pagamentos.append(temporario)

        self.tmp_objeto_retorno = {
            "PAGAMENTOS": self.pagamentos,
            "TOTAL_PAGAMENTOS": total_pagamentos

        }

        return self.tmp_objeto_retorno

    def associado_listar_detalhes_pagamentos(self, objeto_autenticacao):
        self.retorno = None
        self.tmp_objeto_retorno = None
        cb = ConectarBanco()
        db_cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        db_cursor_2 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        db_cursor.execute(
            "select tb_pendencia.id,tb_pendencia.dat_data_de_registro,tb_pendencia.str_inscricao,tb_pendencia.str_tipo_de_cobranca,tb_pendencia.bol_paga,tb_pendencia.str_ano,tb_usuario.str_nome "
            "from tb_usuario inner join tb_sessao on tb_sessao.tb_usuario_id = tb_usuario.id "
            "inner join tb_pendencia on tb_pendencia.str_inscricao = tb_usuario.str_usuario and tb_sessao.str_sessao_token  =  %s "
            " where tb_pendencia.bol_paga = 'TRUE' and tb_pendencia.id = %s order by tb_pendencia.id ",
            [
                objeto_autenticacao['STR_SESSAO_TOKEN'],
                objeto_autenticacao['INT_ID_PAGAMENTO']
            ]
        )

        self.rows_detalhe_pagamento = db_cursor.fetchall()

        self.detalhes_pagamento = []

        for row_detalhes_pagamento in self.rows_detalhe_pagamento:

            if (row_detalhes_pagamento["bol_concluida"] == False):
                self.status_detalhe_pagamento = 'NAO CONCLUIDO'

            if (row_detalhes_pagamento["bol_concluida"] == True):
                self.status_detalhe_pagamento = 'CONCLUIDO'

            self.tipo_moeda = Currency('BRL')

            self.detalhe_pagamento_valor = self.tipo_moeda.get_money_format(
                row_detalhes_pagamento["lon_cobranca_valor_total"])

            row_detalhes_pagamento_tamanho_cartao = len(row_detalhes_pagamento["str_cobranca_cartao_numero"])

            row_detalhes_pagamento_ultimos_digitos = row_detalhes_pagamento["str_cobranca_cartao_numero"]

            if (self.detalhe_pagamento_valor == 'R$ 0'):
                self.detalhe_pagamento_valor = 'R$ 0,0'

            temporario = {
                "ASSOCIADO": row_detalhes_pagamento["str_nome"],
                "CEP": row_detalhes_pagamento["str_cep"],
                "ENDERECO": row_detalhes_pagamento["str_endereco"],
                "ID PAGAMENTO": row_detalhes_pagamento["id"],
                "ANO PAGAMENTO": row_detalhes_pagamento["str_ano"],
                "VALOR TOTAL": self.detalhe_pagamento_valor,
                "BANDEIRA CARTAO": row_detalhes_pagamento["str_cobranca_cartao_bandeira"],
                "04 ULTIMOS DIGITOS CARTAO": row_detalhes_pagamento_ultimos_digitos[-4:],
                "DATA PAGAMENTO": row_detalhes_pagamento["dat_data_registro"],
                "STATUS PAGAMENTO": self.status_detalhe_pagamento,

            }

            self.detalhes_pagamento.append(temporario)

        self.tmp_objeto_retorno = {
            "DETALHES DO PAGAMENTO": self.detalhes_pagamento

        }
        return self.tmp_objeto_retorno

    def associado_ajuda(self, json):
        return None

    def associado_solicitar_cancelamento(self, json):
        return None

    def asociado_buscar_recibos(self, json):
        return None

    def associado_recuperar_dados(self, hash):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        sql = "SELECT u.str_email_1 as email, u.str_telefone_1 as telefone FROM tb_usuario u INNER JOIN tb_sessao ts on u.id = ts.tb_usuario_id WHERE ts.str_sessao_token = %s"
        cursor.execute(sql, [
            hash
        ])
        d = cursor.fetchone()
        cursor.close()
        cb.fecha_conexao()
        return d

    def associado_editar_dados(self, hash, email, telefone):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        sql = "SELECT u.id FROM tb_usuario u INNER JOIN tb_sessao ts on u.id = ts.tb_usuario_id WHERE ts.str_sessao_token = %s"
        cursor.execute(sql, [
            hash
        ])
        id = cursor.fetchone()["id"]
        sql = "UPDATE tb_usuario SET str_email_1 = %s, str_telefone_1 = %s WHERE id = %s"
        cursor.execute(sql, [
            email,
            telefone,
            id
        ])
        cb.db_conexao.commit()
        return True

    def recuperar_dados_cobranca(self, cobranca_id):
        cb = ConectarBanco()
        cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

        sql = "SELECT p.id, p.str_inscricao as inscricao, p.str_ano as ano, p.dou_valor as valor, u.str_nome as nome FROM tb_pendencia p INNER JOIN tb_usuario u ON u.str_inscricao = p.str_inscricao WHERE p.id=%s"
        cursor.execute(sql, [
            cobranca_id
        ])

        data = cursor.fetchone()

        return data

    def associado_executa_pagamento(self, objeto_pagamento):
        """
        tb_pendencia = str_tip (ANU, ME)
        tb_pendencia = str_ano
        tb_pendencia = str_inscricao (CD , EPAO, TPD, LB, TSB, ASB)
        tb_usuario = str_data_requerimento
        QUANDO FOR PESSOA FISICA CD = None
        QUANDO FOR PESSOA JURIDICA = Capital 200K500K, A50K, 500K1M
        tipo, ano, categoria, data_inscricao, razao
        tmp = Calculos()
        testar RS-CD-1 = c2d078c41a3c42d720bcefdaac00c746dddd856daac891229530c15cb38a49d4
        este hash vem la da lista das pendencias do associado logado
        """

        self.tmp_retorno = {}

        cb = ConectarBanco()

        self.CAPITAL_SOCIAL = None

        """
        if(objeto_pagamento['calculo_pf_ou_cnpj'] == 'PF'):
            self.CAPITAL_SOCIAL = None
        if(objeto_pagamento['calculo_pf_ou_cnpj'] == "PJ"):
            self.CAPITAL_SOCIAL = objeto_pagamento['calculor_capital_social']

        calculo_final = Calculos(
            objeto_pagamento['calculo_tipo'],
            objeto_pagamento['calculo_ano'],
            objeto_pagamento['calculo_categoria'],
            objeto_pagamento['calculo_data_de_inscricao'],
            self.CAPITAL_SOCIAL
        )
        """

        # EXECUTAR OPERACOES DE COBRANCA

        tmp_hash_identifica_a_cobranca = hashlib.sha256(str(random.getrandbits(256)).encode('utf-8'))

        txt_cobranca_identificacao_de_referencia = objeto_pagamento['SEGURANCA_CHAVE_DE_ACESSO'] + "[#]" + \
                                                   objeto_pagamento['txt_cobranca_hash_identificar_cliente'] + "[#]" + tmp_hash_identifica_a_cobranca.hexdigest()

        print(tmp_hash_identifica_a_cobranca.hexdigest())

        if (objeto_pagamento['txt_cobranca_token_do_cartao_utilizado_para_pagar'] == ""):
            txt_cobranca_token_do_cartao_utilizado_para_pagar = "NAO_UTILIZADO_CARTAO_TOKENIZADO"
        else:
            txt_cobranca_token_do_cartao_utilizado_para_pagar = objeto_pagamento[
                'txt_cobranca_str_token_do_cartao_utilizado_para_pagar']

        self.bol_cobranca_foi_gerada_no_banco = False

        try:
            db_cursor_insere_cobranca = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

            db_cursor_insere_cobranca.execute(
                "insert into tb_cobranca "
                "("
                "dat_cobranca_data_de_vencimento,"
                "lon_cobranca_valor_total,"
                "str_cobranca_moeda,"
                "txt_cobranca_descricao,"
                "txt_cobranca_identificacao_do_vendedor,"
                "txt_cobranca_capturar,"
                "txt_cobranca_identificacao_de_referencia,"
                "int_cobranca_numero_de_parcelas,"
                "bol_cobranca_regras_de_divisao_dividir,"
                "txt_cobranca_regras_de_divisao_recebedor,"
                "txt_cobranca_regras_de_divisao_codigo_extra,"
                "lon_cobranca_regras_de_divisao_em_valor,"
                "dou_cobranca_regras_de_divisao_em_porcentagem,"
                "lon_cobranca_regras_de_divisao_taxa_de_processamento,"
                "str_cobranca_tipo,"
                "txt_cobranca_hash_identificar_cobranca,"
                "txt_cobranca_token_do_cartao_utilizado_para_pagar,"
                "txt_cobranca_hash_identificar_cliente,"
                "tb_situacao_da_cobranca_str_situacao_da_cobranca,"
                "str_chave_de_acesso,"
                "txt_hash_unico_gerado,"
                "str_ano,"
                "str_inscricao,"
                "tb_pendencia_id,"
                "calculo_pf_ou_cnpj,"
                "calculo_tipo,"
                "calculo_ano,"
                "calculo_categoria,"
                "calculo_data_de_inscricao"
                ") values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                [
                    datetime.strptime(objeto_pagamento['dat_cobranca_data_de_vencimento'], '%d/%m/%Y').date(),
                    0.0,
                    objeto_pagamento['str_cobranca_moeda'],
                    objeto_pagamento['txt_cobranca_descricao'],
                    objeto_pagamento['SEGURANCA_CHAVE_DE_ACESSO'],
                    objeto_pagamento['txt_cobranca_capturar'],
                    txt_cobranca_identificacao_de_referencia,
                    objeto_pagamento['int_cobranca_numero_de_parcelas'],
                    objeto_pagamento['bol_cobranca_regras_de_divisao_dividir'],
                    objeto_pagamento['txt_cobranca_regras_de_divisao_recebedor'],
                    objeto_pagamento['txt_cobranca_regras_de_divisao_codigo_extra'],
                    objeto_pagamento['lon_cobranca_regras_de_divisao_em_valor'],
                    objeto_pagamento['dou_cobranca_regras_de_divisao_em_porcentagem'],
                    objeto_pagamento['lon_cobranca_regras_de_divisao_taxa_de_processamento'],
                    objeto_pagamento['str_cobranca_tipo'],
                    tmp_hash_identifica_a_cobranca.hexdigest(),
                    txt_cobranca_token_do_cartao_utilizado_para_pagar,
                    objeto_pagamento['txt_cobranca_hash_identificar_cliente'],
                    objeto_pagamento['tb_situacao_da_cobranca_str_situacao_da_cobranca'],
                    objeto_pagamento['SEGURANCA_CHAVE_DE_ACESSO'],
                    tmp_hash_identifica_a_cobranca.hexdigest(),
                    objeto_pagamento['calculo_ano'],
                    objeto_pagamento['str_inscricao'],
                    objeto_pagamento['pendencia_id'],
                    objeto_pagamento['calculo_pf_ou_cnpj'],
                    objeto_pagamento['calculo_tipo'],
                    objeto_pagamento['calculo_ano'],
                    objeto_pagamento['calculo_categoria'],
                    objeto_pagamento['calculo_data_de_inscricao']
                ]
            )

            cb.db_conexao.commit()

            self.bol_cobranca_foi_gerada_no_banco = True

        except Exception as e:
            print("ERRO-SQL- Inserindo cobranca : ", str(e))
            self.bol_cobranca_foi_gerada_no_banco = False

        db_cursor_insere_cobranca.close()

        # SE FOI SALVA ENVIA PARA PAGAMENTO

        if (self.bol_cobranca_foi_gerada_no_banco == True):
            print("COBRANCA_SALVA_ENVIAR_PARA_PAGAMENTO")

            self.json_enviar_para_api_de_cobranca = {
                "ACAO": "ACAO_PAGAMENTO_EXECUTAR",
                "SEGURANCA_CHAVE_DE_ACESSO": objeto_pagamento['SEGURANCA_CHAVE_DE_ACESSO'],
                "txt_payment_type": objeto_pagamento["txt_payment_type"],
                "dat_cobranca_data_de_vencimento": objeto_pagamento["dat_cobranca_data_de_vencimento"],
                "dat_cobranca_data_retorno_financeira": "",
                "lon_cobranca_valor_total": objeto_pagamento["lon_cobranca_valor_total"],
                "str_cobranca_moeda": objeto_pagamento['str_cobranca_moeda'],
                "txt_cobranca_descricao": objeto_pagamento['calculo_tipo'] + " " + objeto_pagamento['calculo_ano'],
                "txt_cobranca_capturar": objeto_pagamento['txt_cobranca_capturar'],
                "txt_cobranca_identificacao_de_referencia": txt_cobranca_identificacao_de_referencia,
                "int_cobranca_numero_de_parcelas": objeto_pagamento['int_cobranca_numero_de_parcelas'],
                "bol_cobranca_regras_de_divisao_dividir": objeto_pagamento['bol_cobranca_regras_de_divisao_dividir'],
                "txt_cobranca_regras_de_divisao_recebedor": objeto_pagamento[
                    'txt_cobranca_regras_de_divisao_recebedor'],
                "txt_cobranca_regras_de_divisao_codigo_extra": objeto_pagamento[
                    'txt_cobranca_regras_de_divisao_codigo_extra'],
                "lon_cobranca_regras_de_divisao_taxa_de_processamento": objeto_pagamento[
                    'dou_cobranca_regras_de_divisao_em_porcentagem'],
                "dou_cobranca_regras_de_divisao_em_porcentagem": objeto_pagamento[
                    'dou_cobranca_regras_de_divisao_em_porcentagem'],
                "lon_cobranca_regras_de_divisao_em_valor": objeto_pagamento['lon_cobranca_regras_de_divisao_em_valor'],
                "str_cobranca_tipo": objeto_pagamento['str_cobranca_tipo'],
                "txt_cobranca_hash_identificar_cobranca": tmp_hash_identifica_a_cobranca.hexdigest(),
                "txt_cobranca_str_token_do_cartao_utilizado_para_pagar": txt_cobranca_token_do_cartao_utilizado_para_pagar,
                "txt_cobranca_hash_identificar_cliente": tmp_hash_identifica_a_cobranca.hexdigest(),
                # COLOCAR HASH DO CLIENTE (ASSOCIADO) AQUI
                "str_cobranca_cartao_bandeira": objeto_pagamento["str_cobranca_cartao_bandeira"],
                "str_cobranca_cartao_ano": objeto_pagamento["str_cobranca_cartao_ano"],
                "str_cobranca_cartao_mes": objeto_pagamento["str_cobranca_cartao_mes"],
                "str_cobranca_cartao_numero": objeto_pagamento["str_cobranca_cartao_numero"],
                "str_cobranca_cartao_codigo_de_seguranca": objeto_pagamento["str_cobranca_cartao_codigo_de_seguranca"],
                "str_cobranca_cartao_titular": objeto_pagamento["str_cobranca_cartao_titular"],
                "tb_situacao_da_cobranca_str_situacao_da_cobranca": "COBRANCA_ENVIADA_AGUARDANDO",
                "txt_usage": objeto_pagamento["txt_usage"],
                "txt_mode": objeto_pagamento["txt_mode"]
            }

            self.tmp_retorno = self.helpers_requisicoes.requisicao_post_json_api_cobranca_pagamento(
                config.HOST_POPPAY + ":" + config.PORTA_POPPAY + config.API_COBRANCA_ROTA_PAGAMENTO_EXECUTAR,
                self.json_enviar_para_api_de_cobranca,
                objeto_pagamento['SEGURANCA_CHAVE_DE_ACESSO']
            )

        else:
            self.tmp = "ERRO_A_COBRANCA_NAO_FOI_GERADA_NO_BANCO_DO_SISTEMA"

        cb.fecha_conexao()

        return self.tmp_retorno

    def associado_recuperar_senha_email(self, objeto_autenticacao):

        # SE O USUÁRIO TENTAR RECUPERAR A SENHA MAIS DE 03 VEZES O SISTEMA
        # DEVE BLOQUEAR POR 01 HORA

        # QUANDO FOR PARA PRODUÇÃO TROCAR A TABELA PARA tb_usuario

        self.retorno = None

        self.tmp_objeto_retorno = None

        self.tmp_objeto_retorno_erro = None

        cb = ConectarBanco()

        db_cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

        db_cursor.execute(
            "select tb_usuario.str_inscricao,tb_usuario.str_hash_identifica_o_cliente, "
            "tb_usuario.str_email_1, tb_usuario.str_nome "
            "from tb_usuario "
            " where tb_usuario.str_inscricao = %s  ",
            [
                objeto_autenticacao['STR_INSCRICAO']
            ]
        )
        self.rows_usuario = db_cursor.fetchone()
        self.usuario = []

        # VERIFICA SE A INSCRIÇÃO DO USUÁRIO ESTÁ NA TABELA tb_recuperar_senha
        db_cursor_2 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        db_cursor_2.execute(
            "select count(id) as total_tentativas from tb_recuperar_senha "
            "where str_inscricao_usuario = %s  ",
            [
                objeto_autenticacao['STR_INSCRICAO']
            ]
        )
        self.rows_recuperar_senha = db_cursor_2.fetchone()

        # HASH DO USUÁRIO
        self.chave_gerada_256 = hashlib.sha256(
            str(random.getrandbits(256)).encode('utf-8') + datetime.now().strftime("%Y-%m-%d %H:%M").encode(
                'utf-8'))

        self.hash_usuario = self.chave_gerada_256.hexdigest()

        # VERIFICA O NÚMERO DE TENTATIVAS DE RECUPERAÇÃO DE SENHA
        # SE FOREM TRÊS TENTATIVAS O SISTEMA BLOQUEIA O ENVIO DO LINK PARA RECUPERAÇÃO DE SENHA

        if self.rows_usuario['str_email_1'] == None:
            self.resposta = 'PORFAVOR ENTRE EM CONTATO COM O CRO-RS PARA PREENCHER OS DADOS QUE FALTAM!'
            self.tmp_objeto_retorno = self.resposta
            cb.fecha_conexao()
            return self.tmp_objeto_retorno

        if (self.rows_recuperar_senha['total_tentativas'] < 3):
            temporario_usuario = {
                "EMAIL": self.rows_usuario['str_email_1'],
            }
            self.usuario.append(temporario_usuario)
            self.usuario_email = self.rows_usuario['str_email_1']
            self.nome_usuario = self.rows_usuario['str_nome']

            # ENVIAR LINK DE RECUPERAÇÃO DE SENHA PARA O USUÁRIO
            smtp_sender = SmtpSender("email-smtp.us-east-1.amazonaws.com", 587, "AKIAIIZJVK5RAIIHJ7HQ",
                                     "AryzSu3UZa2ho9/d/8nAeGF++eBFjJ8BDSFIQtxfoYzf", True)

            smtp_sender.read_html_template("/templates/email-nova-senha.html", nome_usuario=self.nome_usuario,
                                           usuario_email=self.usuario_email,
                                           link=config.API_CONSELHO_URL_PRINCIPAL + '/' + url_for(
                                               'cliente_login_route.recuperar_senha_hash', hash=self.hash_usuario))

            self.email_enviar = smtp_sender.send_email("no-reply@poppay.co", self.usuario_email, self.nome_usuario)

            # INSERE NA TABELA tb_recuperar_senha
            self.ip = miniupnpc.UPnP()
            self.ip.discoverdelay = 200
            self.ip.discover()
            self.ip.selectigd()

            self.ip_usuario = format(self.ip.externalipaddress())

            self.dat_recuperar_senha = datetime.now().strftime("%Y-%m-%d %H:%M")

            db_cursor_3 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
            db_cursor_3.execute(
                'insert into tb_recuperar_senha("str_ip_usuario","str_inscricao_usuario", "dat_data_recuperar_senha","str_hash_usuario","bol_status") values(%s,%s,%s,%s,%s)',
                [
                    self.ip_usuario,
                    self.rows_usuario['str_inscricao'],
                    self.dat_recuperar_senha,
                    self.hash_usuario,
                    True
                ]
            )
            cb.db_conexao.commit()

            self.resposta = 'E-MAIL PARA RECUPERAR SENHA ENVIADO COM SUCESSO !'

            self.tmp_objeto_retorno = self.resposta

        else:
            self.resposta = "SOLICITAÇÃO DE RECUPERAÇÃO DE SENHA EXCEDEU 03 TENTATIVAS. AGUARDE 01 HORA PARA SOLICITAR NOVAMENTE."
            self.tmp_objeto_retorno = self.resposta
        # SE A DIFERENÇÁ ENTRE A HORA DO SISTEMA E A HORA GRAVADA NO BANCO DE DADOS FOR MAIOR QUE 01 HORA

        db_cursor_4 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        db_cursor_4.execute(
            "select((extract(epoch from (current_timestamp::timestamp - dat_data_recuperar_senha::timestamp))))::integer as segundos "
            "from tb_recuperar_senha "
            "where str_hash_usuario = %s and bol_status = 'true'",
            [
                self.hash_usuario
            ]
        )
        while True:
            self.rows_segundos = db_cursor_4.fetchone()
            if not self.rows_segundos:
                break
            if (self.rows_segundos['segundos'] == 3.600):
                db_cursor_5 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

                db_cursor_5.execute(
                    "delete from tb_recuperar_senha "
                    "where str_inscricao_usuario = %s ",
                    [
                        objeto_autenticacao['STR_INSCRICAO']

                    ]
                )
                cb.db_conexao.commit()

                db_cursor.close()

                self.resposta = "SOLICITAÇÃO DE RECUPERAÇÃO DE SENHA LIBERADA."

                self.tmp_objeto_retorno = self.resposta

        cb.fecha_conexao()
        return self.tmp_objeto_retorno

    def associado_atualizar_senha(self, objeto_autenticacao):

        self.retorno = None
        self.tmp_objeto_retorno = None
        self.tmp_objeto_retorno_erro = None
        self.nova_senha_usuario = objeto_autenticacao['STR_NOVA_SENHA_USUARIO']
        self.confirma_nova_senha_usuario = objeto_autenticacao['STR_CONFIRMA_NOVA_SENHA_USUARIO']

        cb = ConectarBanco()
        db_cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
        db_cursor.execute(
            "select str_inscricao_usuario,str_hash_usuario, bol_status, extract(hour from dat_data_recuperar_senha) as horas, extract(minute from dat_data_recuperar_senha) as minutos,extract(second "
            "from dat_data_recuperar_senha) as segundos from tb_recuperar_senha  where str_hash_usuario = %s and bol_status = 'true'",
            [
                objeto_autenticacao['STR_HASH_USUARIO']
            ]
        )
        self.rows_recuperar_senha = db_cursor.fetchone()

        print(self.rows_recuperar_senha)

        if (self.confirma_nova_senha_usuario == self.nova_senha_usuario):

            if (self.rows_recuperar_senha):
                db_cursor_1 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
                db_cursor_1.execute(
                    "update tb_usuario "
                    "set str_senha = %s "
                    "where str_inscricao = %s ",
                    [
                        self.confirma_nova_senha_usuario,
                        self.rows_recuperar_senha["str_inscricao_usuario"]

                    ]
                )
                cb.db_conexao.commit()
                db_cursor_2 = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
                db_cursor_2.execute(
                    "delete from tb_recuperar_senha "
                    "where str_inscricao_usuario = %s ",
                    [
                        self.rows_recuperar_senha["str_inscricao_usuario"]
                    ]
                )
            cb.db_conexao.commit()
            self.resposta = "Senha atualizada com sucesso!"
            self.tmp_objeto_retorno = self.resposta

        if (self.confirma_nova_senha_usuario != self.nova_senha_usuario):
            self.resposta = "FAVOR DIGITAR NOVAMENTE A NOVA SENHA E A CONFIRMAÇÃO DA NOVA SENHA"
            self.tmp_objeto_retorno = self.resposta

        return self.tmp_objeto_retorno