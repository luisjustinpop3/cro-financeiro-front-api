import psycopg2

class ConectarBanco:

    global db_conexao

    def __init__(self):
        try:
            self.db_conexao = psycopg2.connect(
                dbname='db_api_conselho',
                user='postgres',
                host='192.168.0.87',
                password='monolito'
            )

        except:
            print("ERROR_TRY_CONNECT_DATABASE")

    def fecha_conexao(self):
        self.db_conexao.close()