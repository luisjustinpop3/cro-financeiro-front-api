from flask import Blueprint, render_template, request, url_for, current_app, session
from decorators.AutenticadoDecorator import esta_autenticado_cliente
from helpers.Paginator import Paginator
import json
import requests

cliente_home_route = Blueprint('cliente_home_route', __name__, url_prefix='/cliente')

@cliente_home_route.route('/')
@esta_autenticado_cliente
def home():

    page = 1
    if (request.args.get('page') != None):
        page = request.args.get('page')

    limit = 10
    offset = 0

    if ( int(page) > 1 ):
        offset = int(page)

    dados = {
        "STR_SESSAO_TOKEN": session.get("token_acesso"),
        "INT_LIMIT": limit,
        "INT_OFFSET": offset
    }
    data = {
        "data": json.dumps(dados)
    }
    sess = {
        'SESSAO_TOKEN': session.get('token_acesso')
    }
    headers = {
        'SESSAO_TOKEN': json.dumps(sess)
    }

    response = requests.post(current_app.config["API_CONSELHO_URL_PRINCIPAL"] + current_app.config["API_CONSELHO_ROTA_ASSOCIADOS_LISTAR_PENDENCIAS"], data=data, headers=headers)
    pagamentos_em_aberto = json.loads(response.content)

    print(pagamentos_em_aberto)

    paginator = Paginator(int(page), pagamentos_em_aberto["retorno"]["TOTAL PENDENCIAS"], url_for('cliente_home_route.home'), limit)

    return render_template('cliente/home.html', pagamentos_em_aberto=pagamentos_em_aberto["retorno"]["PENDENCIAS"], paginacao=paginator.paginate())

@cliente_home_route.route('/geral')
@esta_autenticado_cliente
def geral():

    page = 1
    if (request.args.get('page') != None):
        page = request.args.get('page')

    limit = 10
    offset = 0

    if (int(page) > 1):
        offset = int(page)

    dados = {
        "STR_SESSAO_TOKEN": session.get("token_acesso"),
        "INT_LIMIT": limit,
        "INT_OFFSET": offset
    }
    data = {
        "data": json.dumps(dados)
    }
    sess = {
        'SESSAO_TOKEN': session.get('token_acesso')
    }
    headers = {
        'SESSAO_TOKEN': json.dumps(sess)
    }

    response = requests.post(current_app.config["API_CONSELHO_URL_PRINCIPAL"] + current_app.config["API_CONSELHO_ROTA_ASSOCIADOS_LISTAR_PAGAMENTOS"], data=data, headers=headers)

    print(response.content)

    pagamentos_em_aberto = json.loads(response.content)

    paginator = Paginator(int(page), pagamentos_em_aberto["retorno"]["TOTAL_PAGAMENTOS"], url_for('cliente_home_route.geral'), limit)

    return render_template('cliente/geral.html', pagamentos=pagamentos_em_aberto["retorno"]["PAGAMENTOS"], paginacao=paginator.paginate())