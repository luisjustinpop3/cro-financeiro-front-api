from flask import Blueprint, render_template, request, url_for, current_app, session
from decorators.AutenticadoDecorator import esta_autenticado_cliente
from helpers.Paginator import Paginator
import json, requests
from helpers.HeaderGenerator import generateHeader

cliente_editar_cadastro_route = Blueprint('cliente_editar_cadastro_route', __name__, url_prefix='/cliente/editar-cadastro')

@cliente_editar_cadastro_route.route('/', methods=["GET", "POST"])
@esta_autenticado_cliente
def home():

    if( request.method == "POST" ):
        email = request.form.get('txtEmail')
        telefone = request.form.get('txtTelefone')
        data_post = {
            'email': email,
            'telefone': telefone
        }
        d = requests.post(current_app.config["API_CONSELHO_URL_PRINCIPAL"] + current_app.config["API_CONSELHO_ROTA_ASSOCIADOS_RECUPERAR_DADOS"], headers=generateHeader(session.get('token_acesso')), data=data_post)

    d = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] + current_app.config["API_CONSELHO_ROTA_ASSOCIADOS_RECUPERAR_DADOS"], headers=generateHeader(session.get('token_acesso')))
    j = json.loads(d.content)["retorno"]

    return render_template('cliente/editar_cadastro.html', dados=j)