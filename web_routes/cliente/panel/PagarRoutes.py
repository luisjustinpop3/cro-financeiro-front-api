from flask import Blueprint, render_template, request, url_for, current_app, session, make_response

import config
from decorators.AutenticadoDecorator import esta_autenticado_cliente
import json, requests
from helpers.HeaderGenerator import generateHeader
import datetime
from modulos.modulo_associados.AssociadoOperacoes import AssociadoOperacoes
from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor

cliente_pagar_route = Blueprint('cliente_pagar_route', __name__, url_prefix='/cliente/pagar')
ap = AssociadoOperacoes()

@cliente_pagar_route.route('/<int:cobranca_id>', methods=["GET"])
@esta_autenticado_cliente
def home(cobranca_id):
    response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] + current_app.config[
        "API_CONSELHO_ROTA_ASSOCIADOS_DETALHES_COBRANCA"] + "/" + str(cobranca_id),
                            headers=generateHeader(session.get("token_acesso")))
    print(response.content)
    loaded_data = json.loads(response.content)
    loaded_data = loaded_data["retorno"]

    return render_template('cliente/pagar-cartao.html', cobranca_dados=loaded_data, id=cobranca_id)


@cliente_pagar_route.route('finalizar/<int:crobanca_id>', methods=["POST"])
@esta_autenticado_cliente
def finalizar(crobanca_id):

    response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] + current_app.config[
        "API_CONSELHO_ROTA_ASSOCIADOS_DETALHES_COBRANCA"] + "/" + str(crobanca_id),
                            headers=generateHeader(session.get("token_acesso")))
    loaded_data = json.loads(response.content)["retorno"]
    parcelas = 1
    if len(request.form) >= 1:
        parcelas = request.form.get("txttotalparceladoParcelas")
        valor_pagar = float(loaded_data["valor"]) / float(parcelas)
    else:
        valor_pagar = loaded_data["valor"]

    tmp_ano_atual = datetime.datetime.now().year
    tmp_anos_select = []
    for x in range(10):
        tmp_anos_select.append(tmp_ano_atual)
        tmp_ano_atual += 1

    return render_template('cliente/finalizar-pagamento.html', cobranca_dados=loaded_data, id=crobanca_id, valor_pagar=valor_pagar, anos_select=tmp_anos_select, parcelas=1)

@cliente_pagar_route.route('ajax/finalizar', methods=["POST"])
@esta_autenticado_cliente
def ajax_send_card_request():
    date_now = datetime.datetime.now()
    cb = ConectarBanco()
    cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
    sql = "SELECT *, p.id as id_pendencia FROM tb_pendencia as p INNER JOIN tb_usuario u ON u.str_inscricao = p.str_inscricao WHERE p.id=%s LIMIT 1"
    cursor.execute(sql, [
        request.form.get('txtPendenciaId')
    ])
    dados_pendencia = cursor.fetchone()
    valor = float(dados_pendencia["dou_valor"])
    parcelas = 1
    if len(request.form.get('txtParcelas')) > 0:
        parcelas = request.form.get('txtParcelas')
        valor = float(dados_pendencia["dou_valor"]) / float(request.form.get('txtParcelas'))

    valor_em_centavos = valor * 100

    data = {
        "ACAO": "ACAO_PAGAMENTO_EXECUTAR",
        "STR_SESSAO_TOKEN": str(session.get("token_acesso")),
        "calculo_pf_ou_cnpj":"PF",
        "SEGURANCA_CHAVE_DE_ACESSO": str(config.API_COBRANCA_CHAVE_DE_ACESSO),
        "txt_payment_type":"CREDITO",
        "dat_cobranca_data_de_vencimento": str(str(date_now.day) + "/" + str(date_now.month) + "/" + str(date_now.year)),
        "dat_cobranca_data_retorno_financeira": str(str(date_now.day) + "/" + str(date_now.month) + "/" + str(date_now.year)),
        "lon_cobranca_valor_total": str(int(valor_em_centavos)),
        "str_cobranca_moeda": "BRL",
        "calculo_ano": str(dados_pendencia["str_ano"]),
        "str_inscricao": str(dados_pendencia["str_inscricao"]),
        "pendencia_id": str(dados_pendencia["id_pendencia"]),
        "split":"",
        "calculo_tipo":"",
        "calculo_categoria":"",
        "calculo_data_de_inscricao":"",
        "txt_cobranca_descricao": str(dados_pendencia["str_tipo_de_cobranca"]),
        "txt_cobranca_capturar": "true",
        "int_cobranca_numero_de_parcelas": str(parcelas),
        "bol_cobranca_regras_de_divisao_dividir":"false",
        "txt_cobranca_regras_de_divisao_recebedor": "false",
        "txt_cobranca_regras_de_divisao_codigo_extra": "0",
        "lon_cobranca_regras_de_divisao_taxa_de_processamento": "0",
        "dou_cobranca_regras_de_divisao_em_porcentagem": "0",
        "lon_cobranca_regras_de_divisao_em_valor": "0",
        "str_cobranca_tipo": "card",
        "txt_cobranca_hash_identificar_cobranca": "hash identifica a propria cobranca aqui",
        "txt_cobranca_str_token_do_cartao_utilizado_para_pagar": "hash_do_cliente_gerado_identifica_o_dono_do_cartao21c36ff63663eb998d840f7268bc20b66904be8a1b912a3054545d328601576f7048414717119833543236040985797198353",
        "txt_cobranca_hash_identificar_cliente": str(dados_pendencia["str_hash_identifica_o_cliente"]),
        "txt_cobranca_token_do_cartao_utilizado_para_pagar":"",
        "str_cobranca_cartao_bandeira": str(request.form.get("txtBandeira")),
        "str_cobranca_cartao_ano": str(request.form.get("txtValidadeCartaoAno")),
        "str_cobranca_cartao_mes": str(request.form.get("txtValidadeCartaoMes")),
        "str_cobranca_cartao_numero": str(request.form.get("txtNumeroCartao")),
        "str_cobranca_cartao_codigo_de_seguranca": str(request.form.get("txtCodigoSeguranca")),
        "str_cobranca_cartao_titular": str(request.form.get("txtNomeImpressoCartao")),
        "tb_situacao_da_cobranca_str_situacao_da_cobranca":"COBRANCA_ENVIADA_AGUARDANDO",
        "txt_usage":"single_use",
        "txt_mode":"interest_free"
    }

    result = ap.associado_executa_pagamento(data)

    print(result)

    return make_response(result)
