
def clienteRoutes(app):
    from web_routes.cliente.login.LoginRoute import cliente_login_route
    app.register_blueprint(cliente_login_route)
    from web_routes.cliente.panel.HomeRoutes import cliente_home_route
    app.register_blueprint(cliente_home_route)
    from web_routes.cliente.panel.EditarCadastroRoutes import cliente_editar_cadastro_route
    app.register_blueprint(cliente_editar_cadastro_route)
    from web_routes.cliente.panel.PagarRoutes import cliente_pagar_route
    app.register_blueprint(cliente_pagar_route)