from flask import Blueprint, render_template, current_app, request, session, redirect, url_for, flash
import requests
import json
from modulos.modulo_associados.AssociadoOperacoes import AssociadoOperacoes

ap = AssociadoOperacoes()

cliente_login_route = Blueprint('cliente_login_route', __name__, url_prefix='/cliente/login')


@cliente_login_route.route('/', methods=['GET', 'POST'])
def login():
    if ( request.method == "POST" ):
        error = None
        login = {
            "ACAO": "ACAO_AUTENTICAR",
            "STR_TIPO": "AUTENTICACAO_SIMPLES",
            "STR_USUARIO": "RS-" + request.form["txtCategoria"] + "-" + request.form["txtLogin"],
            "STR_SENHA": request.form["txtSenha"]
        }
        data = {
            "data": json.dumps(login)
        }
        response = requests.post(current_app.config["API_CONSELHO_URL_PRINCIPAL"] + current_app.config["API_CONSELHO_ROTA_AUTENTICAR"], data=data)
        user_data = json.loads(response.content)
        if ( user_data["retorno"]["PERMISSAO"] ):
            if ( user_data["retorno"]["TIPO USUARIO"] == "PADRAO" ):
                session["token_acesso"] = user_data["retorno"]["SESSAO_TOKEN"]
                session["nivel_acesso"] = user_data["retorno"]["TIPO USUARIO"]
                session["usuario_nome"] = user_data["retorno"]["USUARIO_NOME"]
                return redirect(url_for('cliente_home_route.home'))
            else:
                session.clear()
                error = "Tipo de usuário não permitido!"
                flash(error)
        else:
            session.clear()
            error = "Usuário/Senha Inválidos!"
            flash(error)
    else:
        session.clear()

    return render_template('cliente/login.html')


@cliente_login_route.route('/logout')
def logout():
    if ( session.get('token_acesso') ):
        login = {
            "ACAO": "ACAO_DESLOGAR",
            "STR_TIPO": "AUTENTICACAO_SIMPLES",
            "STR_SESSAO_TOKEN": session.get('token_acesso')
        }
        data = {
            "data": json.dumps(login)
        }
        sess = {
            'SESSAO_TOKEN': session.get('token_acesso')
        }
        headers = {
            'SESSAO_TOKEN': json.dumps(sess)
        }
        response = requests.post(current_app.config["API_CONSELHO_URL_PRINCIPAL"] + current_app.config["API_CONSELHO_ROTA_LOGOUT"], data=data, headers=headers)
        return redirect(url_for('cliente_login_route.login'))

@cliente_login_route.route('/manutencao')
def manutencao():
    return render_template('cliente/manutencao.html')

@cliente_login_route.route('/recuperar-senha', methods=["GET", "POST"])
def recuperar_senha_cliente():
    if request.method == "POST":
        inscricao = request.form.get('txtLogin')
        data = {
            "STR_INSCRICAO": inscricao
        }
        result = ap.associado_recuperar_senha_email(data)
        flash(result)

    return render_template('recuperar-senha.html')

@cliente_login_route.route('/recuperar-senha/<string:hash>', methods=["GET", "POST"])
def recuperar_senha_hash(hash):
    if request.method == "POST":
        password = request.form.get('txtPass')
        password_confirm = request.form.get('txtPassConfirm')
        if password == password_confirm:
            data = {
                "STR_NOVA_SENHA_USUARIO": password,
                "STR_CONFIRMA_NOVA_SENHA_USUARIO": password_confirm,
                "STR_HASH_USUARIO": hash
            }
            result = ap.associado_atualizar_senha(data)
            flash(result)
        else:
            flash("Senhas não são iguais!")

    return render_template('nova_senha.html', hash=hash)