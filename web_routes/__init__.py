from web_routes.admin import adminRoutes
from web_routes.cliente import clienteRoutes

def registerRoutes(app):
    adminRoutes(app)
    clienteRoutes(app)