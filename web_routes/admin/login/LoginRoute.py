from flask import Blueprint, render_template, current_app, request, session, redirect, url_for, flash
import requests
import json

login_route = Blueprint('login_route', __name__, url_prefix='/admin/login')

@login_route.route('/', methods=['GET', 'POST'])
def login():
    if ( request.method == "POST" ):
        error = None
        login = {
            "ACAO": "ACAO_AUTENTICAR",
            "STR_TIPO": "AUTENTICACAO_SIMPLES",
            "STR_USUARIO": request.form["txtLogin"],
            "STR_SENHA": request.form["txtSenha"]
        }
        data = {
            "data": json.dumps(login)
        }
        response = requests.post(current_app.config["API_CONSELHO_URL_PRINCIPAL"] + current_app.config["API_CONSELHO_ROTA_AUTENTICAR"], data=data)
        user_data = json.loads(response.content)
        print(user_data["retorno"])
        if ( user_data["retorno"]["PERMISSAO"] ):
            if ( user_data["retorno"]["TIPO USUARIO"] == "SISTEMA" ):
                session["token_acesso"] = user_data["retorno"]["SESSAO_TOKEN"]
                session["nivel_acesso"] = user_data["retorno"]["TIPO USUARIO"]
                session["usuario_nome"] = user_data["retorno"]["USUARIO_NOME"]
                return redirect(url_for('home_route.home'))
            else:
                session.clear()
                error = "Tipo de usuário não permitido!"
                flash(error)
        else:
            session.clear()
            error = "Usuário/Senha Inválidos!"
            flash(error)
    else:
        session.clear()

    return render_template('admin/login.html')

@login_route.route('/logout')
def logout():
    if (session.get('token_acesso')):
        login = {
            "ACAO": "ACAO_DESLOGAR",
            "STR_TIPO": "AUTENTICACAO_SIMPLES",
            "STR_SESSAO_TOKEN": session.get('token_acesso')
        }
        data = {
            "data": json.dumps(login)
        }
        sess = {
            'SESSAO_TOKEN': session.get('token_acesso')
        }
        headers = {
            'SESSAO_TOKEN': json.dumps(sess)
        }
        response = requests.post(
            current_app.config["API_CONSELHO_URL_PRINCIPAL"] + current_app.config["API_CONSELHO_ROTA_LOGOUT"],
            data=data, headers=headers)
        return redirect(url_for('cliente_login_route.login'))