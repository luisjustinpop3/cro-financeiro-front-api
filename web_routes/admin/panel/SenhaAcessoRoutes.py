from flask import Blueprint, render_template, request, session, current_app, url_for
from decorators.AutenticadoDecorator import esta_autenticado_admin
import json
import requests
from helpers.HeaderGenerator import generateHeader
from helpers.Paginator import Paginator

senha_acesso_route = Blueprint('senha_acesso_route', __name__, url_prefix='/admin')


@senha_acesso_route.route('/senhas', methods=["GET"])
@esta_autenticado_admin
def senhas():
    associados = []

    page = 1
    if request.args.get('page') is not None:
        page = request.args.get('page')

    limit = 10
    offset = 0

    if int(page) > 1:
        offset = (int(page) - 1) * 10

    if request.args.get('search') is not None:
        search = request.args.get('search')
        # Se tiver busca
        if int(page) > 1:
            # se tiver busca e pagina
            response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                                    current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_LISTAR_ASSOCIADOS"] +
                                    "/" + str(offset) +
                                    "/buscar/" + str(search),
                                    headers=generateHeader(session.get('token_acesso')))
        else:
            # se tiver somente busca
            response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                                    current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_LISTAR_ASSOCIADOS"] +
                                    "/buscar/" + str(search),
                                    headers=generateHeader(session.get('token_acesso')))
    else:
        # Se nao tiver buscar
        if int(page) > 1:
            # Se nao tiver busca mas tem pagina
            response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                                    current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_LISTAR_ASSOCIADOS"] +
                                    "/" + str(offset),
                                    headers=generateHeader(session.get('token_acesso')))
        else:
            # Se nao tiver nem busca nem pagina
            response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                                    current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_LISTAR_ASSOCIADOS"],
                                    headers=generateHeader(session.get('token_acesso')))

    d = json.loads(response.content)

    associados = d["retorno"]["resultados"]

    paginator = Paginator(int(page), d["retorno"]["total"], url_for('senha_acesso_route.senhas'), limit)

    return render_template('admin/senha-acesso.html', associados=associados, paginacao=paginator.paginate(), sms_ativado=current_app.config["SMS_ATIVADO"])

@senha_acesso_route.route('/senhas/editar/<int:id>', methods=["GET", "POST"])
@esta_autenticado_admin
def editar_profissional(id):

    if request.method == "POST":
        dados = {
            'email': request.form.get('txtEmail'),
            'telefone': request.form.get('txtTelefone')
        }
        response = requests.post(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                            current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_EDITAR_DADOS_ASSOCIADO"] + "/" + str(id),
                            data=dados,
                            headers=generateHeader(session.get('token_acesso')))

    response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                            current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_RECUPERAR_DADOS_ASSOCIADO"] + "/" + str(id),
                            headers=generateHeader(session.get('token_acesso')))

    d = json.loads(response.content)

    return render_template('admin/editar-profissional.html', dados=d["retorno"], id=id)