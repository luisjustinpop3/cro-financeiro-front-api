from flask import Blueprint, render_template, request, flash, redirect, url_for
from decorators.AutenticadoDecorator import esta_autenticado_admin
from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor

atualizacao_route = Blueprint('atualizacao_route', __name__, url_prefix='/admin')

@atualizacao_route.route('/atualizacao')
@esta_autenticado_admin
def atualizacao():
    cb = ConectarBanco()
    cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
    cursor.execute("SELECT * FROM tb_notificacoes WHERE type='IMPORT_STARTED' AND status=0 AND token='0'")
    cursor.fetchone()

    import_started = False
    if cursor.rowcount > 0:
        flash("Há uma importação em Andamento não pode efetuar uma nova até finalizar a atual! Você será notificado ao termino da importação!")
        return redirect(url_for('home_route.home'))

    return render_template('admin/atualizacao.html')