from flask import Blueprint, render_template, request
from decorators.AutenticadoDecorator import esta_autenticado_admin
from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor

relatorios_route = Blueprint('relatorios_route', __name__, url_prefix='/admin')

@relatorios_route.route('/relatorios')
@esta_autenticado_admin
def reletorios():
    cb = ConectarBanco()
    cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)
    sql = "SELECT COUNT(str_ano) as total, str_ano as ano FROM tb_pendencia WHERE bol_paga = false GROUP BY str_ano order by str_ano DESC LIMIT 5"
    cursor.execute(sql)
    pendencias_por_ano = cursor.fetchall()

    sql = ""

    return render_template('admin/relatorios.html', pendencias_por_ano=pendencias_por_ano)