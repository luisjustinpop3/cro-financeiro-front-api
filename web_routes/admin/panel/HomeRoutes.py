from flask import Blueprint, render_template, request, session, current_app, url_for
from decorators.AutenticadoDecorator import esta_autenticado_admin
import json
import requests
from helpers.HeaderGenerator import generateHeader
from helpers.Paginator import Paginator
from modulos.conexao_pgsql.ConectarBanco import ConectarBanco
from psycopg2.extras import RealDictCursor
from modulos.modulo_administracao.AdministracaoOperacoesModule import AdministracaoOperacoesModule
import datetime
from calendar import monthrange

admin_module = AdministracaoOperacoesModule()

home_route = Blueprint('home_route', __name__, url_prefix='/admin')

@home_route.route('/', methods=["GET", "POST"])
@esta_autenticado_admin
def home():

    cb = ConectarBanco()
    cursor = cb.db_conexao.cursor(cursor_factory=RealDictCursor)

    #SQL VALOR TOTAL EM PENDENCIAS
    sql = "SELECT SUM(p.dou_valor) as sum FROM tb_pendencia as p WHERE bol_paga = false"
    cursor.execute(sql)
    valor_total_pendencias = cursor.fetchone()["sum"]
    valor_total_pendencias = "%.2f" % (valor_total_pendencias)

    #SQL TOTAL DE PAGAMENTOS
    sql = "SELECT SUM(c.lon_cobranca_valor_total) as sum FROM tb_cobranca as c WHERE bol_concluida = true"
    cursor.execute(sql)
    valor_total_pago = cursor.fetchone()["sum"]
    if valor_total_pago != None:
        valor_total_pago = "%.2f" % (valor_total_pago)
    else:
        valor_total_pago = 0.0

    if request.method == "POST":
        txtDataInicial = request.form.get("txtDataInicial")
        txtDataFinal = request.form.get("txtDataFinal")
    else:
        now = datetime.datetime.today()
        txtDataInicial = str(now.year) + "-" + '%02d' % (now.month) + "-01"
        dateRange = monthrange(now.year, now.month)
        txtDataFinal = str(now.year) + "-" + '%02d' % (now.month) + "-" + str(dateRange[1])

    rangeDate = {
        "DATA_INICIAL": txtDataInicial,
        "DATA_FINAL": txtDataFinal
    }

    #VALOR LIBERADO PARA SAQUE

    valor_liberado_para_saque = admin_module.administracao_valor_liberado_saque(rangeDate)

    #VALOR HA LIBERAR

    valor_ha_liberar = admin_module.administracao_valor_ha_liberar(rangeDate)

    if valor_ha_liberar == None:
        valor_ha_liberar = 0.00



    ultimas_transacoes = []

    response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] + current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_LISTA_ULTIMAS_TRANSACOES"], headers=generateHeader(session.get('token_acesso')))
    d = json.loads(response.content)

    ultimas_transacoes = d["retorno"]["resultados"]

    return render_template('admin/home.html', ultimas_transacoes=ultimas_transacoes, txtDataInicial=txtDataInicial, txtDataFinal=txtDataFinal, valor_total_pendencias=valor_total_pendencias, valor_total_pago=valor_total_pago, valor_liberado_para_saque=valor_liberado_para_saque, valor_ha_liberar=valor_ha_liberar)

@home_route.route('/geral')
@esta_autenticado_admin
def geral():
    page = 1
    if request.args.get('page') is not None:
        page = request.args.get('page')

    limit = 10
    offset = 0

    if int(page) > 1:
        offset = (int(page) - 1) * 10

    if request.args.get('search') is not None:
        search = request.args.get('search')
        # Se tiver busca
        if int(page) > 1:
            # se tiver busca e pagina
            response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                                    current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_LISTA_PAGAMENTOS"] +
                                    "/" + str(offset) +
                                    "/buscar/" + str(search),
                                    headers=generateHeader(session.get('token_acesso')))
        else:
            # se tiver somente busca
            response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                                    current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_LISTA_PAGAMENTOS"] +
                                    "/buscar/" + str(search),
                                    headers=generateHeader(session.get('token_acesso')))
    else:
        # Se nao tiver buscar
        if int(page) > 1:
            # Se nao tiver busca mas tem pagina
            response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                                    current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_LISTA_PAGAMENTOS"] +
                                    "/" + str(offset),
                                    headers=generateHeader(session.get('token_acesso')))
        else:
            # Se nao tiver nem busca nem pagina
            response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                                    current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_LISTA_PAGAMENTOS"],
                                    headers=generateHeader(session.get('token_acesso')))

    d = json.loads(response.content)

    pagamentos = d["retorno"]["resultados"]

    paginator = Paginator(int(page), d["retorno"]["total"], url_for('home_route.geral'), limit)

    return render_template('admin/geral.html', todos_pagamentos=pagamentos, paginacao=paginator.paginate())

@home_route.route('/sacar')
@esta_autenticado_admin
def sacar():
    return render_template('admin/retirar-valor.html')

@home_route.route('/haliberar')
@esta_autenticado_admin
def haliberar():
    return render_template('admin/valor-ha-liberar.html')

@home_route.route('/devedores')
@esta_autenticado_admin
def devedores():
    page = 1
    if request.args.get('page') is not None:
        page = request.args.get('page')

    limit = 10
    offset = 0

    if int(page) > 1:
        offset = (int(page) - 1) * 10

    if request.args.get('search') is not None:
        search = request.args.get('search')
        # Se tiver busca
        if int(page) > 1:
            # se tiver busca e pagina
            response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                                    current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_DEVEDORES"] +
                                    "/" + str(offset) +
                                    "/buscar/" + str(search),
                                    headers=generateHeader(session.get('token_acesso')))
        else:
            # se tiver somente busca
            response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                                    current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_DEVEDORES"] +
                                    "/buscar/" + str(search),
                                    headers=generateHeader(session.get('token_acesso')))
    else:
        # Se nao tiver buscar
        if int(page) > 1:
            # Se nao tiver busca mas tem pagina
            response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                                    current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_DEVEDORES"] +
                                    "/" + str(offset),
                                    headers=generateHeader(session.get('token_acesso')))
        else:
            # Se nao tiver nem busca nem pagina
            response = requests.get(current_app.config["API_CONSELHO_URL_PRINCIPAL"] +
                                    current_app.config["API_CONSELHO_ROTA_ADMINISTRACAO_DEVEDORES"],
                                    headers=generateHeader(session.get('token_acesso')))

    d = json.loads(response.content)

    pagamentos = d["retorno"]["data"]

    paginator = Paginator(int(page), d["retorno"]["total"], url_for('home_route.devedores'), limit)
    print(pagamentos)
    return render_template('admin/devedores.html', todos_pagamentos=pagamentos, paginacao=paginator.paginate())