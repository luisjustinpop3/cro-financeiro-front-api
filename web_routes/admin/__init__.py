
def adminRoutes(app):
    from web_routes.admin.login.LoginRoute import login_route
    app.register_blueprint(login_route)
    from web_routes.admin.panel.HomeRoutes import home_route
    app.register_blueprint(home_route)
    from web_routes.admin.panel.RelatoriosRoutes import relatorios_route
    app.register_blueprint(relatorios_route)
    from web_routes.admin.panel.AtualizacaoRoutes import atualizacao_route
    app.register_blueprint(atualizacao_route)
    from web_routes.admin.panel.SenhaAcessoRoutes import senha_acesso_route
    app.register_blueprint(senha_acesso_route)