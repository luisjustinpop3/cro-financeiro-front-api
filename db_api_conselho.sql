--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1 (Ubuntu 12.1-1.pgdg18.04+1)
-- Dumped by pg_dump version 12.1 (Ubuntu 12.1-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: function_deleta_a_sessao(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.function_deleta_a_sessao() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM tb_sessao WHERE timestamp < NOW() - INTERVAL '2 days';
  RETURN NULL;
END;
$$;


ALTER FUNCTION public.function_deleta_a_sessao() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: tb_cobranca; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_cobranca (
    id integer NOT NULL,
    dat_data_registro timestamp without time zone DEFAULT now() NOT NULL,
    dat_cobranca_data_de_vencimento timestamp without time zone,
    lon_cobranca_valor_total bigint,
    str_cobranca_moeda character varying(255),
    txt_cobranca_descricao text,
    txt_cobranca_identificacao_do_vendedor text,
    txt_cobranca_capturar text,
    txt_cobranca_identificacao_de_referencia text,
    int_cobranca_numero_de_parcelas integer,
    bol_cobranca_regras_de_divisao_dividir boolean DEFAULT false NOT NULL,
    txt_cobranca_regras_de_divisao_recebedor text,
    txt_cobranca_regras_de_divisao_codigo_extra text,
    lon_cobranca_regras_de_divisao_em_valor bigint,
    dou_cobranca_regras_de_divisao_em_porcentagem double precision,
    lon_cobranca_regras_de_divisao_taxa_de_processamento bigint,
    str_cobranca_tipo character varying(255),
    txt_cobranca_hash_identificar_cobranca text,
    txt_cobranca_str_token_do_cartao_utilizado_para_pagar text,
    txt_cobranca_hash_identificar_cliente text,
    tb_situacao_da_cobranca_str_situacao_da_cobranca text,
    txt_cobranca_token_do_cartao_utilizado_para_pagar text,
    str_chave_de_acesso text,
    bol_concluida boolean DEFAULT false NOT NULL,
    txt_hash_unico_gerado text,
    txt_retorno_financeira text,
    str_ano text,
    str_inscricao text,
    tb_pendencia_id bigint,
    calculo_pf_ou_cnpj text,
    calculo_tipo text,
    calculo_ano text,
    calculo_categoria text,
    calculo_data_de_inscricao text
);


ALTER TABLE public.tb_cobranca OWNER TO postgres;

--
-- Name: tb_cobranca_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_cobranca_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_cobranca_id_seq OWNER TO postgres;

--
-- Name: tb_cobranca_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_cobranca_id_seq OWNED BY public.tb_cobranca.id;


--
-- Name: tb_conecta_perfil_ao_grupo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_conecta_perfil_ao_grupo (
    id integer NOT NULL,
    tb_perfil_id bigint,
    tb_grupo_id bigint
);


ALTER TABLE public.tb_conecta_perfil_ao_grupo OWNER TO postgres;

--
-- Name: tb_conecta_perfil_ao_grupo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_conecta_perfil_ao_grupo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_conecta_perfil_ao_grupo_id_seq OWNER TO postgres;

--
-- Name: tb_conecta_perfil_ao_grupo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_conecta_perfil_ao_grupo_id_seq OWNED BY public.tb_conecta_perfil_ao_grupo.id;


--
-- Name: tb_conecta_permissao_ao_perfil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_conecta_permissao_ao_perfil (
    id integer NOT NULL,
    tb_permissao_id bigint,
    tb_perfil_id bigint
);


ALTER TABLE public.tb_conecta_permissao_ao_perfil OWNER TO postgres;

--
-- Name: tb_conecta_permissao_ao_perfil_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_conecta_permissao_ao_perfil_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_conecta_permissao_ao_perfil_id_seq OWNER TO postgres;

--
-- Name: tb_conecta_permissao_ao_perfil_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_conecta_permissao_ao_perfil_id_seq OWNED BY public.tb_conecta_permissao_ao_perfil.id;


--
-- Name: tb_conecta_usuario_ao_grupo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_conecta_usuario_ao_grupo (
    id integer NOT NULL,
    tb_usuario_id bigint,
    tb_grupo_id bigint
);


ALTER TABLE public.tb_conecta_usuario_ao_grupo OWNER TO postgres;

--
-- Name: tb_conecta_usuario_ao_grupo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_conecta_usuario_ao_grupo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_conecta_usuario_ao_grupo_id_seq OWNER TO postgres;

--
-- Name: tb_conecta_usuario_ao_grupo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_conecta_usuario_ao_grupo_id_seq OWNED BY public.tb_conecta_usuario_ao_grupo.id;


--
-- Name: tb_grupo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_grupo (
    id bigint NOT NULL,
    str_grupo_titulo text,
    str_grupo_descricao text
);


ALTER TABLE public.tb_grupo OWNER TO postgres;

--
-- Name: tb_grupo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_grupo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_grupo_id_seq OWNER TO postgres;

--
-- Name: tb_grupo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_grupo_id_seq OWNED BY public.tb_grupo.id;


--
-- Name: tb_notificacoes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_notificacoes (
    id integer NOT NULL,
    type text,
    message text,
    status integer,
    token text
);


ALTER TABLE public.tb_notificacoes OWNER TO postgres;

--
-- Name: tb_notificacoes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_notificacoes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_notificacoes_id_seq OWNER TO postgres;

--
-- Name: tb_notificacoes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_notificacoes_id_seq OWNED BY public.tb_notificacoes.id;


--
-- Name: tb_notifications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_notifications (
    nid bigint NOT NULL,
    type character varying NOT NULL,
    message text NOT NULL,
    uid bigint,
    status integer NOT NULL,
    created_at date
);


ALTER TABLE public.tb_notifications OWNER TO postgres;

--
-- Name: tb_notifications_nid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_notifications_nid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_notifications_nid_seq OWNER TO postgres;

--
-- Name: tb_notifications_nid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_notifications_nid_seq OWNED BY public.tb_notifications.nid;


--
-- Name: tb_pendencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_pendencia (
    id integer NOT NULL,
    dat_data_de_registro timestamp without time zone DEFAULT now() NOT NULL,
    str_inscricao text,
    str_ano text,
    str_tipo_de_cobranca text,
    bol_paga boolean DEFAULT false NOT NULL,
    dou_valor double precision,
    str_hash_identifica_a_pendencia text
);


ALTER TABLE public.tb_pendencia OWNER TO postgres;

--
-- Name: tb_pendencia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_pendencia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_pendencia_id_seq OWNER TO postgres;

--
-- Name: tb_pendencia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_pendencia_id_seq OWNED BY public.tb_pendencia.id;


--
-- Name: tb_perfil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_perfil (
    id integer NOT NULL,
    str_perfil_titulo text,
    str_perfil_descricao text
);


ALTER TABLE public.tb_perfil OWNER TO postgres;

--
-- Name: tb_perfil_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_perfil_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_perfil_id_seq OWNER TO postgres;

--
-- Name: tb_perfil_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_perfil_id_seq OWNED BY public.tb_perfil.id;


--
-- Name: tb_permissao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_permissao (
    id integer NOT NULL,
    str_permissao_titulo text,
    str_permissao_descricao text,
    str_permissao_alias text
);


ALTER TABLE public.tb_permissao OWNER TO postgres;

--
-- Name: tb_permissao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_permissao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_permissao_id_seq OWNER TO postgres;

--
-- Name: tb_permissao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_permissao_id_seq OWNED BY public.tb_permissao.id;


--
-- Name: tb_recuperar_senha; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_recuperar_senha (
    id integer NOT NULL,
    str_ip_usuario character varying,
    str_inscricao_usuario character varying,
    dat_data_recuperar_senha timestamp without time zone,
    str_hash_usuario text,
    bol_status boolean
);


ALTER TABLE public.tb_recuperar_senha OWNER TO postgres;

--
-- Name: tb_recuperar_senha_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_recuperar_senha_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_recuperar_senha_id_seq OWNER TO postgres;

--
-- Name: tb_recuperar_senha_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_recuperar_senha_id_seq OWNED BY public.tb_recuperar_senha.id;


--
-- Name: tb_sessao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_sessao (
    id integer NOT NULL,
    str_sessao_token text,
    tb_usuario_id bigint
);


ALTER TABLE public.tb_sessao OWNER TO postgres;

--
-- Name: tb_sessao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_sessao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_sessao_id_seq OWNER TO postgres;

--
-- Name: tb_sessao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_sessao_id_seq OWNED BY public.tb_sessao.id;


--
-- Name: tb_situacao_da_cobranca; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_situacao_da_cobranca (
    id integer NOT NULL,
    str_situacao_da_cobranca text
);


ALTER TABLE public.tb_situacao_da_cobranca OWNER TO postgres;

--
-- Name: tb_situacao_da_cobranca_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_situacao_da_cobranca_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_situacao_da_cobranca_id_seq OWNER TO postgres;

--
-- Name: tb_situacao_da_cobranca_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_situacao_da_cobranca_id_seq OWNED BY public.tb_situacao_da_cobranca.id;


--
-- Name: tb_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_usuario (
    id integer NOT NULL,
    bol_ativo boolean DEFAULT true NOT NULL,
    dat_data_criacao timestamp without time zone DEFAULT now() NOT NULL,
    str_usuario text,
    str_senha text,
    str_email_1 text,
    str_tipo text,
    str_inscricao text,
    str_email_2 text,
    str_nome text,
    str_endereco text,
    str_bairro text,
    str_uf text,
    str_municipio text,
    str_cep text,
    str_telefone_1 text,
    str_telefone_2 text,
    str_cpf_cnpj text,
    str_debito text,
    str_inscrespttec text,
    str_nomeresptec text,
    str_ident text,
    str_emissor text,
    str_estcivil text,
    str_sexo text,
    str_nacionalidade text,
    str_cancelado text,
    str_dat_cro text,
    str_capital_social text,
    str_data_requerimento text,
    str_tipo_pf_ou_pj text,
    str_hash_identifica_o_cliente text
);


ALTER TABLE public.tb_usuario OWNER TO postgres;

--
-- Name: tb_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_usuario_id_seq OWNER TO postgres;

--
-- Name: tb_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_usuario_id_seq OWNED BY public.tb_usuario.id;


--
-- Name: tb_usuario_teste; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_usuario_teste (
    id integer NOT NULL,
    str_nome character varying,
    str_senha character varying,
    str_email_1 character varying,
    str_inscricao character varying,
    str_hash_identifica_o_cliente character varying
);


ALTER TABLE public.tb_usuario_teste OWNER TO postgres;

--
-- Name: tb_usuario_teste_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_usuario_teste_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_usuario_teste_id_seq OWNER TO postgres;

--
-- Name: tb_usuario_teste_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_usuario_teste_id_seq OWNED BY public.tb_usuario_teste.id;


--
-- Name: tb_usuario_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_usuario_tipo (
    id integer NOT NULL,
    str_tipo text
);


ALTER TABLE public.tb_usuario_tipo OWNER TO postgres;

--
-- Name: tb_usuario_tipo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_usuario_tipo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_usuario_tipo_id_seq OWNER TO postgres;

--
-- Name: tb_usuario_tipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_usuario_tipo_id_seq OWNED BY public.tb_usuario_tipo.id;


--
-- Name: tb_cobranca id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_cobranca ALTER COLUMN id SET DEFAULT nextval('public.tb_cobranca_id_seq'::regclass);


--
-- Name: tb_conecta_perfil_ao_grupo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_conecta_perfil_ao_grupo ALTER COLUMN id SET DEFAULT nextval('public.tb_conecta_perfil_ao_grupo_id_seq'::regclass);


--
-- Name: tb_conecta_permissao_ao_perfil id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_conecta_permissao_ao_perfil ALTER COLUMN id SET DEFAULT nextval('public.tb_conecta_permissao_ao_perfil_id_seq'::regclass);


--
-- Name: tb_conecta_usuario_ao_grupo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_conecta_usuario_ao_grupo ALTER COLUMN id SET DEFAULT nextval('public.tb_conecta_usuario_ao_grupo_id_seq'::regclass);


--
-- Name: tb_grupo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_grupo ALTER COLUMN id SET DEFAULT nextval('public.tb_grupo_id_seq'::regclass);


--
-- Name: tb_notificacoes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_notificacoes ALTER COLUMN id SET DEFAULT nextval('public.tb_notificacoes_id_seq'::regclass);


--
-- Name: tb_notifications nid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_notifications ALTER COLUMN nid SET DEFAULT nextval('public.tb_notifications_nid_seq'::regclass);


--
-- Name: tb_pendencia id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pendencia ALTER COLUMN id SET DEFAULT nextval('public.tb_pendencia_id_seq'::regclass);


--
-- Name: tb_perfil id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_perfil ALTER COLUMN id SET DEFAULT nextval('public.tb_perfil_id_seq'::regclass);


--
-- Name: tb_permissao id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_permissao ALTER COLUMN id SET DEFAULT nextval('public.tb_permissao_id_seq'::regclass);


--
-- Name: tb_recuperar_senha id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_recuperar_senha ALTER COLUMN id SET DEFAULT nextval('public.tb_recuperar_senha_id_seq'::regclass);


--
-- Name: tb_sessao id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_sessao ALTER COLUMN id SET DEFAULT nextval('public.tb_sessao_id_seq'::regclass);


--
-- Name: tb_situacao_da_cobranca id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_situacao_da_cobranca ALTER COLUMN id SET DEFAULT nextval('public.tb_situacao_da_cobranca_id_seq'::regclass);


--
-- Name: tb_usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_usuario ALTER COLUMN id SET DEFAULT nextval('public.tb_usuario_id_seq'::regclass);


--
-- Name: tb_usuario_teste id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_usuario_teste ALTER COLUMN id SET DEFAULT nextval('public.tb_usuario_teste_id_seq'::regclass);


--
-- Name: tb_usuario_tipo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_usuario_tipo ALTER COLUMN id SET DEFAULT nextval('public.tb_usuario_tipo_id_seq'::regclass);


--
-- Data for Name: tb_cobranca; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_cobranca (id, dat_data_registro, dat_cobranca_data_de_vencimento, lon_cobranca_valor_total, str_cobranca_moeda, txt_cobranca_descricao, txt_cobranca_identificacao_do_vendedor, txt_cobranca_capturar, txt_cobranca_identificacao_de_referencia, int_cobranca_numero_de_parcelas, bol_cobranca_regras_de_divisao_dividir, txt_cobranca_regras_de_divisao_recebedor, txt_cobranca_regras_de_divisao_codigo_extra, lon_cobranca_regras_de_divisao_em_valor, dou_cobranca_regras_de_divisao_em_porcentagem, lon_cobranca_regras_de_divisao_taxa_de_processamento, str_cobranca_tipo, txt_cobranca_hash_identificar_cobranca, txt_cobranca_str_token_do_cartao_utilizado_para_pagar, txt_cobranca_hash_identificar_cliente, tb_situacao_da_cobranca_str_situacao_da_cobranca, txt_cobranca_token_do_cartao_utilizado_para_pagar, str_chave_de_acesso, bol_concluida, txt_hash_unico_gerado, txt_retorno_financeira, str_ano, str_inscricao, tb_pendencia_id, calculo_pf_ou_cnpj, calculo_tipo, calculo_ano, calculo_categoria, calculo_data_de_inscricao) FROM stdin;
178	2020-02-04 15:35:43.78326	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475[#]9163d590dbaaed0d6778dce44d7160fb4355c526f79fc4fff88be583779fb5c2	1	f	false	0	0	0	0	card	9163d590dbaaed0d6778dce44d7160fb4355c526f79fc4fff88be583779fb5c2	\N	9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	9163d590dbaaed0d6778dce44d7160fb4355c526f79fc4fff88be583779fb5c2	\N	2013	RS-CD-1	1087	PF		2013		
179	2020-02-04 15:36:48.836927	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475[#]c23e22cd07ba8974c929f6c06df0fb1ad5669fedcce518eccd9a6a96d50747e2	1	f	false	0	0	0	0	card	c23e22cd07ba8974c929f6c06df0fb1ad5669fedcce518eccd9a6a96d50747e2	\N	9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	c23e22cd07ba8974c929f6c06df0fb1ad5669fedcce518eccd9a6a96d50747e2	\N	2013	RS-CD-1	1087	PF		2013		
181	2020-02-04 15:40:23.856481	2020-05-10 00:00:00	0	BRL	Pagamento 2019	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]hash que identifica o pagante cliente da loja[#]ad085a97195f45c41294a1b716ddf0ef38a7e68b273f823799597ca354647b9b	1	f	false	0	0	0	0	card	ad085a97195f45c41294a1b716ddf0ef38a7e68b273f823799597ca354647b9b	\N	hash que identifica o pagante cliente da loja	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	ad085a97195f45c41294a1b716ddf0ef38a7e68b273f823799597ca354647b9b	\N	2020	RS-CD-1	1087	PF		2020		
182	2020-02-04 15:44:53.630987	2020-05-10 00:00:00	0	BRL	Pagamento 2019	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]hash que identifica o pagante cliente da loja[#]8e4b02500174359d1d7740284ccf1007b7b4ce91149b66f77d0e333f91a4c1c2	1	f	false	0	0	0	0	card	8e4b02500174359d1d7740284ccf1007b7b4ce91149b66f77d0e333f91a4c1c2	\N	hash que identifica o pagante cliente da loja	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	8e4b02500174359d1d7740284ccf1007b7b4ce91149b66f77d0e333f91a4c1c2	\N	2020	RS-CD-1	1087	PF		2020		
183	2020-02-04 15:45:54.088946	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475[#]b375295dd2b709df4ecace25a98845bb730f1388daa2028b72e3b1b9c113b51d	1	f	false	0	0	0	0	card	b375295dd2b709df4ecace25a98845bb730f1388daa2028b72e3b1b9c113b51d	\N	9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	b375295dd2b709df4ecace25a98845bb730f1388daa2028b72e3b1b9c113b51d	\N	2013	RS-CD-1	1087	PF		2013		
184	2020-02-04 15:53:07.291456	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475[#]ced56de6fe58056ec0e707739e6930a0695e6098a943ea5d820b51d4c566ded0	1	f	false	0	0	0	0	card	ced56de6fe58056ec0e707739e6930a0695e6098a943ea5d820b51d4c566ded0	\N	9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	ced56de6fe58056ec0e707739e6930a0695e6098a943ea5d820b51d4c566ded0	\N	2013	RS-CD-1	1087	PF		2013		
185	2020-02-04 15:56:40.78108	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475[#]ba7eb908c7e584f1dea3232bb1e1b46006a8bfd3ce67e5cff3d447b7050fd086	1	f	false	0	0	0	0	card	ba7eb908c7e584f1dea3232bb1e1b46006a8bfd3ce67e5cff3d447b7050fd086	\N	9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	ba7eb908c7e584f1dea3232bb1e1b46006a8bfd3ce67e5cff3d447b7050fd086	\N	2013	RS-CD-1	1087	PF		2013		
186	2020-02-04 15:59:09.290605	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475[#]b6a288c801a63a9ccde966ec1499f5a9304720db57ffac406020f637805596c6	1	f	false	0	0	0	0	card	b6a288c801a63a9ccde966ec1499f5a9304720db57ffac406020f637805596c6	\N	9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	b6a288c801a63a9ccde966ec1499f5a9304720db57ffac406020f637805596c6	\N	2013	RS-CD-1	1087	PF		2013		
187	2020-02-04 16:00:08.762498	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475[#]5083116f3dfca827f897d9975cd1ce9a397de02e902de20c6437a81aa6095377	1	f	false	0	0	0	0	card	5083116f3dfca827f897d9975cd1ce9a397de02e902de20c6437a81aa6095377	\N	9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	5083116f3dfca827f897d9975cd1ce9a397de02e902de20c6437a81aa6095377	\N	2013	RS-CD-1	1087	PF		2013		
188	2020-02-04 16:04:36.467489	2020-05-10 00:00:00	0	BRL	Pagamento 2019	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]hash que identifica o pagante cliente da loja[#]c1a72c72d24990214dc959627fc3600053f0edac388ae84807f7e1ac7b2745a5	1	f	false	0	0	0	0	card	c1a72c72d24990214dc959627fc3600053f0edac388ae84807f7e1ac7b2745a5	\N	hash que identifica o pagante cliente da loja	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	c1a72c72d24990214dc959627fc3600053f0edac388ae84807f7e1ac7b2745a5	\N	2020	RS-CD-1	1087	PF		2020		
189	2020-02-04 16:18:31.949667	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]f288fc8539a98d850defc27995e0be77b53dac0749f2d797dab8724bcd8b070d	1	f	false	0	0	0	0	card	f288fc8539a98d850defc27995e0be77b53dac0749f2d797dab8724bcd8b070d	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	f288fc8539a98d850defc27995e0be77b53dac0749f2d797dab8724bcd8b070d	\N	2013	RS-CD-1	1087	PF		2013		
190	2020-02-04 16:19:58.149721	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]b362073fadb0842111491011057a321d4a5eedd4794f59c7ed91ead2cc68d7a3	1	f	false	0	0	0	0	card	b362073fadb0842111491011057a321d4a5eedd4794f59c7ed91ead2cc68d7a3	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	b362073fadb0842111491011057a321d4a5eedd4794f59c7ed91ead2cc68d7a3	\N	2013	RS-CD-1	1087	PF		2013		
191	2020-02-04 16:19:59.760718	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]facac38c14c663ace20f1481f8a9ee89ae208f16c30d5ca45eaec492a62bb5e7	1	f	false	0	0	0	0	card	facac38c14c663ace20f1481f8a9ee89ae208f16c30d5ca45eaec492a62bb5e7	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	facac38c14c663ace20f1481f8a9ee89ae208f16c30d5ca45eaec492a62bb5e7	\N	2013	RS-CD-1	1087	PF		2013		
192	2020-02-04 16:22:36.482216	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]a42d79818db83aaa6b80fe13b683747219520e9dc876f4cafd48cac7163fbad0	1	f	false	0	0	0	0	card	a42d79818db83aaa6b80fe13b683747219520e9dc876f4cafd48cac7163fbad0	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	a42d79818db83aaa6b80fe13b683747219520e9dc876f4cafd48cac7163fbad0	\N	2013	RS-CD-1	1087	PF		2013		
193	2020-02-04 16:23:01.027768	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]095281a305ff2f82113dabba4a1be0a04274dbfa1332039c86e39aca010fc0c6	1	f	false	0	0	0	0	card	095281a305ff2f82113dabba4a1be0a04274dbfa1332039c86e39aca010fc0c6	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	095281a305ff2f82113dabba4a1be0a04274dbfa1332039c86e39aca010fc0c6	\N	2013	RS-CD-1	1087	PF		2013		
194	2020-02-04 16:24:02.379938	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]36fa2ccbe1ec6e8d14d7d139235edac5679cf672ce4d8e98b7dc0beeba6f342d	1	f	false	0	0	0	0	card	36fa2ccbe1ec6e8d14d7d139235edac5679cf672ce4d8e98b7dc0beeba6f342d	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	36fa2ccbe1ec6e8d14d7d139235edac5679cf672ce4d8e98b7dc0beeba6f342d	\N	2013	RS-CD-1	1087	PF		2013		
195	2020-02-04 16:24:07.71961	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]1f06ae883c2c10f144b944f7460f8c6cb0e8ecebead9429e6dbdc637e258c0ec	1	f	false	0	0	0	0	card	1f06ae883c2c10f144b944f7460f8c6cb0e8ecebead9429e6dbdc637e258c0ec	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	1f06ae883c2c10f144b944f7460f8c6cb0e8ecebead9429e6dbdc637e258c0ec	\N	2013	RS-CD-1	1087	PF		2013		
196	2020-02-04 16:28:21.424404	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]dc0712fd21686f896373c31088d54c9244174ec2d3bd29e2d3f601f9cd60d2e6	1	f	false	0	0	0	0	card	dc0712fd21686f896373c31088d54c9244174ec2d3bd29e2d3f601f9cd60d2e6	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	dc0712fd21686f896373c31088d54c9244174ec2d3bd29e2d3f601f9cd60d2e6	\N	2013	RS-CD-1	1087	PF		2013		
197	2020-02-04 16:31:50.006369	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]f6f8e25bda03f39263aa21c05be5f1a8459d2c07da7eb26370d6d1183431ae31	1	f	false	0	0	0	0	card	f6f8e25bda03f39263aa21c05be5f1a8459d2c07da7eb26370d6d1183431ae31	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	f6f8e25bda03f39263aa21c05be5f1a8459d2c07da7eb26370d6d1183431ae31	\N	2013	RS-CD-1	1087	PF		2013		
198	2020-02-04 16:33:09.691942	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]f742579937d904cfcc14d170ec72426b525497313e3526b9e44c360347b978fe	1	f	false	0	0	0	0	card	f742579937d904cfcc14d170ec72426b525497313e3526b9e44c360347b978fe	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	f742579937d904cfcc14d170ec72426b525497313e3526b9e44c360347b978fe	\N	2013	RS-CD-1	1087	PF		2013		
199	2020-02-04 16:35:46.181866	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]7a800b6d73e045113fe69586a1d6176350f80df8558383506f6ea06384fc5b4f	1	f	false	0	0	0	0	card	7a800b6d73e045113fe69586a1d6176350f80df8558383506f6ea06384fc5b4f	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	7a800b6d73e045113fe69586a1d6176350f80df8558383506f6ea06384fc5b4f	\N	2013	RS-CD-1	1087	PF		2013		
204	2020-02-04 17:07:38.479011	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]b56c5506fbbe5796f69bdeee3314148b3b23e1598e4b479eff847f0817eb2713	1	f	false	0	0	0	0	card	b56c5506fbbe5796f69bdeee3314148b3b23e1598e4b479eff847f0817eb2713	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	b56c5506fbbe5796f69bdeee3314148b3b23e1598e4b479eff847f0817eb2713	\N	2013	RS-CD-1	1087	PF		2013		
205	2020-02-04 17:07:39.624877	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]cd1ecb52b758ea3da81af892562b595397b34d38422b5e69ba2904ea76f62518	1	f	false	0	0	0	0	card	cd1ecb52b758ea3da81af892562b595397b34d38422b5e69ba2904ea76f62518	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	cd1ecb52b758ea3da81af892562b595397b34d38422b5e69ba2904ea76f62518	\N	2013	RS-CD-1	1087	PF		2013		
200	2020-02-04 17:01:19.107958	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]812c0499e1ba6506ab74167da911ca5056c06152819e5644dc34ff58bbb9e380	1	f	false	0	0	0	0	card	812c0499e1ba6506ab74167da911ca5056c06152819e5644dc34ff58bbb9e380	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_EXTORNADA	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	812c0499e1ba6506ab74167da911ca5056c06152819e5644dc34ff58bbb9e380	"{\\n    \\"bol_cobranca_regras_de_divisao_dividir\\": false,\\n    \\"dat_cobranca_data_de_vencimento\\": \\"2020-04-02 00:00:00\\",\\n    \\"dou_cobranca_regras_de_divisao_em_porcentagem\\": 0.0,\\n    \\"int_cobranca_numero_de_parcelas\\": 1,\\n    \\"lon_cobranca_regras_de_divisao_em_valor\\": 0,\\n    \\"lon_cobranca_regras_de_divisao_taxa_de_processamento\\": 0,\\n    \\"lon_cobranca_valor_total\\": 100047,\\n    \\"str_chave_de_acesso\\": \\"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5\\",\\n    \\"str_cobranca_moeda\\": \\"BRL\\",\\n    \\"str_cobranca_tipo\\": \\"card\\",\\n    \\"tb_situacao_da_cobranca_str_situacao_da_cobranca\\": \\"COBRANCA_EXTORNADA\\",\\n    \\"txt_cobranca_capturar\\": \\"true\\",\\n    \\"txt_cobranca_descricao\\": \\" 2013\\",\\n    \\"txt_cobranca_hash_identificar_cliente\\": \\"812c0499e1ba6506ab74167da911ca5056c06152819e5644dc34ff58bbb9e380\\",\\n    \\"txt_cobranca_hash_identificar_cobranca\\": \\"c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e\\",\\n    \\"txt_cobranca_identificacao_de_referencia\\": \\"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]812c0499e1ba6506ab74167da911ca5056c06152819e5644dc34ff58bbb9e380\\",\\n    \\"txt_cobranca_identificacao_do_vendedor\\": \\"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5\\",\\n    \\"txt_cobranca_regras_de_divisao_codigo_extra\\": \\"0\\",\\n    \\"txt_cobranca_regras_de_divisao_recebedor\\": \\"false\\",\\n    \\"txt_cobranca_str_token_do_cartao_utilizado_para_pagar\\": \\"NAO_UTILIZADO_CARTAO_TOKENIZADO\\",\\n    \\"txt_hash_unico_gerado\\": \\"812c0499e1ba6506ab74167da911ca5056c06152819e5644dc34ff58bbb9e380\\"\\n}"	2013	RS-CD-1	1087	PF		2013		
201	2020-02-04 17:03:47.894246	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]90f8101c3e78d2c6312c2958e6b80afcfc8808fcfc71fe0bd6140b1b9ffee374	1	f	false	0	0	0	0	card	90f8101c3e78d2c6312c2958e6b80afcfc8808fcfc71fe0bd6140b1b9ffee374	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_EXTORNADA	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	90f8101c3e78d2c6312c2958e6b80afcfc8808fcfc71fe0bd6140b1b9ffee374	"{\\n    \\"bol_cobranca_regras_de_divisao_dividir\\": false,\\n    \\"dat_cobranca_data_de_vencimento\\": \\"2020-04-02 00:00:00\\",\\n    \\"dou_cobranca_regras_de_divisao_em_porcentagem\\": 0.0,\\n    \\"int_cobranca_numero_de_parcelas\\": 1,\\n    \\"lon_cobranca_regras_de_divisao_em_valor\\": 0,\\n    \\"lon_cobranca_regras_de_divisao_taxa_de_processamento\\": 0,\\n    \\"lon_cobranca_valor_total\\": 100047,\\n    \\"str_chave_de_acesso\\": \\"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5\\",\\n    \\"str_cobranca_moeda\\": \\"BRL\\",\\n    \\"str_cobranca_tipo\\": \\"card\\",\\n    \\"tb_situacao_da_cobranca_str_situacao_da_cobranca\\": \\"COBRANCA_EXTORNADA\\",\\n    \\"txt_cobranca_capturar\\": \\"true\\",\\n    \\"txt_cobranca_descricao\\": \\" 2013\\",\\n    \\"txt_cobranca_hash_identificar_cliente\\": \\"90f8101c3e78d2c6312c2958e6b80afcfc8808fcfc71fe0bd6140b1b9ffee374\\",\\n    \\"txt_cobranca_hash_identificar_cobranca\\": \\"c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e\\",\\n    \\"txt_cobranca_identificacao_de_referencia\\": \\"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]90f8101c3e78d2c6312c2958e6b80afcfc8808fcfc71fe0bd6140b1b9ffee374\\",\\n    \\"txt_cobranca_identificacao_do_vendedor\\": \\"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5\\",\\n    \\"txt_cobranca_regras_de_divisao_codigo_extra\\": \\"0\\",\\n    \\"txt_cobranca_regras_de_divisao_recebedor\\": \\"false\\",\\n    \\"txt_cobranca_str_token_do_cartao_utilizado_para_pagar\\": \\"NAO_UTILIZADO_CARTAO_TOKENIZADO\\",\\n    \\"txt_hash_unico_gerado\\": \\"90f8101c3e78d2c6312c2958e6b80afcfc8808fcfc71fe0bd6140b1b9ffee374\\"\\n}"	2013	RS-CD-1	1087	PF		2013		
202	2020-02-04 17:04:37.087763	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]c3c4a193ba02fd4a33b967a302f0f348fc7bf86211a3415fadb461c3e59f4f9f	1	f	false	0	0	0	0	card	c3c4a193ba02fd4a33b967a302f0f348fc7bf86211a3415fadb461c3e59f4f9f	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_EXTORNADA	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	c3c4a193ba02fd4a33b967a302f0f348fc7bf86211a3415fadb461c3e59f4f9f	"{\\n    \\"bol_cobranca_regras_de_divisao_dividir\\": false,\\n    \\"dat_cobranca_data_de_vencimento\\": \\"2020-04-02 00:00:00\\",\\n    \\"dou_cobranca_regras_de_divisao_em_porcentagem\\": 0.0,\\n    \\"int_cobranca_numero_de_parcelas\\": 1,\\n    \\"lon_cobranca_regras_de_divisao_em_valor\\": 0,\\n    \\"lon_cobranca_regras_de_divisao_taxa_de_processamento\\": 0,\\n    \\"lon_cobranca_valor_total\\": 100047,\\n    \\"str_chave_de_acesso\\": \\"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5\\",\\n    \\"str_cobranca_moeda\\": \\"BRL\\",\\n    \\"str_cobranca_tipo\\": \\"card\\",\\n    \\"tb_situacao_da_cobranca_str_situacao_da_cobranca\\": \\"COBRANCA_EXTORNADA\\",\\n    \\"txt_cobranca_capturar\\": \\"true\\",\\n    \\"txt_cobranca_descricao\\": \\" 2013\\",\\n    \\"txt_cobranca_hash_identificar_cliente\\": \\"c3c4a193ba02fd4a33b967a302f0f348fc7bf86211a3415fadb461c3e59f4f9f\\",\\n    \\"txt_cobranca_hash_identificar_cobranca\\": \\"c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e\\",\\n    \\"txt_cobranca_identificacao_de_referencia\\": \\"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]c3c4a193ba02fd4a33b967a302f0f348fc7bf86211a3415fadb461c3e59f4f9f\\",\\n    \\"txt_cobranca_identificacao_do_vendedor\\": \\"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5\\",\\n    \\"txt_cobranca_regras_de_divisao_codigo_extra\\": \\"0\\",\\n    \\"txt_cobranca_regras_de_divisao_recebedor\\": \\"false\\",\\n    \\"txt_cobranca_str_token_do_cartao_utilizado_para_pagar\\": \\"NAO_UTILIZADO_CARTAO_TOKENIZADO\\",\\n    \\"txt_hash_unico_gerado\\": \\"c3c4a193ba02fd4a33b967a302f0f348fc7bf86211a3415fadb461c3e59f4f9f\\"\\n}"	2013	RS-CD-1	1087	PF		2013		
203	2020-02-04 17:07:28.698977	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]386d1d06f7506999b0f1c937ea33f45baf84e2fe272bf33af54138cf201c1739	1	f	false	0	0	0	0	card	386d1d06f7506999b0f1c937ea33f45baf84e2fe272bf33af54138cf201c1739	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	386d1d06f7506999b0f1c937ea33f45baf84e2fe272bf33af54138cf201c1739	\N	2013	RS-CD-1	1087	PF		2013		
206	2020-02-04 17:07:39.972484	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]cee2090e3d8e6d990ef3bf73083babd095ea3b5ec755155bd480d31629101846	1	f	false	0	0	0	0	card	cee2090e3d8e6d990ef3bf73083babd095ea3b5ec755155bd480d31629101846	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	cee2090e3d8e6d990ef3bf73083babd095ea3b5ec755155bd480d31629101846	\N	2013	RS-CD-1	1087	PF		2013		
207	2020-02-04 17:07:40.108083	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]b521ebe833e6ef90fa594b24dddc77faeb6866cbabdfd60481fd9a724fb673f5	1	f	false	0	0	0	0	card	b521ebe833e6ef90fa594b24dddc77faeb6866cbabdfd60481fd9a724fb673f5	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	b521ebe833e6ef90fa594b24dddc77faeb6866cbabdfd60481fd9a724fb673f5	\N	2013	RS-CD-1	1087	PF		2013		
208	2020-02-04 17:07:40.149354	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]1dd4e1a8ae732be87a63c38d2bea2524acac7ad93951fa6a35b94925a3a61b9c	1	f	false	0	0	0	0	card	1dd4e1a8ae732be87a63c38d2bea2524acac7ad93951fa6a35b94925a3a61b9c	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	1dd4e1a8ae732be87a63c38d2bea2524acac7ad93951fa6a35b94925a3a61b9c	\N	2013	RS-CD-1	1087	PF		2013		
209	2020-02-04 17:07:40.267662	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]8e7831e4aba8672608298cf1e5b6a491392f34969a294aa04ea14a874ae8c8e9	1	f	false	0	0	0	0	card	8e7831e4aba8672608298cf1e5b6a491392f34969a294aa04ea14a874ae8c8e9	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	8e7831e4aba8672608298cf1e5b6a491392f34969a294aa04ea14a874ae8c8e9	\N	2013	RS-CD-1	1087	PF		2013		
210	2020-02-04 17:07:40.414913	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]8aefcde2a8d48ea9c2a01dce6595d31a119f29521f88732393309998199ae0eb	1	f	false	0	0	0	0	card	8aefcde2a8d48ea9c2a01dce6595d31a119f29521f88732393309998199ae0eb	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	8aefcde2a8d48ea9c2a01dce6595d31a119f29521f88732393309998199ae0eb	\N	2013	RS-CD-1	1087	PF		2013		
211	2020-02-04 17:08:05.593756	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]3cf7098e1a985646c982906de9bd94c4b0c438da368d3ca2c427e98bdb6a45ca	1	f	false	0	0	0	0	card	3cf7098e1a985646c982906de9bd94c4b0c438da368d3ca2c427e98bdb6a45ca	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	3cf7098e1a985646c982906de9bd94c4b0c438da368d3ca2c427e98bdb6a45ca	\N	2013	RS-CD-1	1087	PF		2013		
212	2020-02-04 17:08:55.510646	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]f6530c86e383cbcfe46c5fbc80904372fca9274bd2bd5dacd399607bada7b9aa	1	f	false	0	0	0	0	card	f6530c86e383cbcfe46c5fbc80904372fca9274bd2bd5dacd399607bada7b9aa	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	f6530c86e383cbcfe46c5fbc80904372fca9274bd2bd5dacd399607bada7b9aa	\N	2013	RS-CD-1	1087	PF		2013		
213	2020-02-04 17:14:06.787432	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]5e719b0da945f1f7c6a8e60d924339d011ac808fd1ebd23d3835c6f6c7e5f492	1	f	false	0	0	0	0	card	5e719b0da945f1f7c6a8e60d924339d011ac808fd1ebd23d3835c6f6c7e5f492	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	5e719b0da945f1f7c6a8e60d924339d011ac808fd1ebd23d3835c6f6c7e5f492	\N	2013	RS-CD-1	1087	PF		2013		
214	2020-02-04 17:20:45.437955	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]c2a941c3ed6bfa46d4ed37b7061f3ad49899817ce8f4cbb8352b860b428687cc	1	f	false	0	0	0	0	card	c2a941c3ed6bfa46d4ed37b7061f3ad49899817ce8f4cbb8352b860b428687cc	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_EXTORNADA	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	c2a941c3ed6bfa46d4ed37b7061f3ad49899817ce8f4cbb8352b860b428687cc	"{\\n    \\"bol_cobranca_regras_de_divisao_dividir\\": false,\\n    \\"dat_cobranca_data_de_vencimento\\": \\"2020-04-02 00:00:00\\",\\n    \\"dou_cobranca_regras_de_divisao_em_porcentagem\\": 0.0,\\n    \\"int_cobranca_numero_de_parcelas\\": 1,\\n    \\"lon_cobranca_regras_de_divisao_em_valor\\": 0,\\n    \\"lon_cobranca_regras_de_divisao_taxa_de_processamento\\": 0,\\n    \\"lon_cobranca_valor_total\\": 100047,\\n    \\"str_chave_de_acesso\\": \\"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5\\",\\n    \\"str_cobranca_moeda\\": \\"BRL\\",\\n    \\"str_cobranca_tipo\\": \\"card\\",\\n    \\"tb_situacao_da_cobranca_str_situacao_da_cobranca\\": \\"COBRANCA_EXTORNADA\\",\\n    \\"txt_cobranca_capturar\\": \\"true\\",\\n    \\"txt_cobranca_descricao\\": \\" 2013\\",\\n    \\"txt_cobranca_hash_identificar_cliente\\": \\"c2a941c3ed6bfa46d4ed37b7061f3ad49899817ce8f4cbb8352b860b428687cc\\",\\n    \\"txt_cobranca_hash_identificar_cobranca\\": \\"c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e\\",\\n    \\"txt_cobranca_identificacao_de_referencia\\": \\"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]c2a941c3ed6bfa46d4ed37b7061f3ad49899817ce8f4cbb8352b860b428687cc\\",\\n    \\"txt_cobranca_identificacao_do_vendedor\\": \\"8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5\\",\\n    \\"txt_cobranca_regras_de_divisao_codigo_extra\\": \\"0\\",\\n    \\"txt_cobranca_regras_de_divisao_recebedor\\": \\"false\\",\\n    \\"txt_cobranca_str_token_do_cartao_utilizado_para_pagar\\": \\"NAO_UTILIZADO_CARTAO_TOKENIZADO\\",\\n    \\"txt_hash_unico_gerado\\": \\"c2a941c3ed6bfa46d4ed37b7061f3ad49899817ce8f4cbb8352b860b428687cc\\"\\n}"	2013	RS-CD-1	1087	PF		2013		
215	2020-02-04 17:22:56.676251	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]6187697ad538ef7cb62d2c74b4763ed1071d3094679fd8ff09bf4dc80daae629	1	f	false	0	0	0	0	card	6187697ad538ef7cb62d2c74b4763ed1071d3094679fd8ff09bf4dc80daae629	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	6187697ad538ef7cb62d2c74b4763ed1071d3094679fd8ff09bf4dc80daae629	\N	2013	RS-CD-1	1087	PF		2013		
216	2020-02-04 17:24:04.598039	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]a2096fbbc8bb2751e25293d3ececef614cb795b2369675ca0373cd515139fbbe	1	f	false	0	0	0	0	card	a2096fbbc8bb2751e25293d3ececef614cb795b2369675ca0373cd515139fbbe	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	a2096fbbc8bb2751e25293d3ececef614cb795b2369675ca0373cd515139fbbe	\N	2013	RS-CD-1	1087	PF		2013		
217	2020-02-04 17:24:27.429008	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]e24bc5bbd10291e1c4aa1e8501bc91060a50f869049d5b8f4fcf0dd29533965e	1	f	false	0	0	0	0	card	e24bc5bbd10291e1c4aa1e8501bc91060a50f869049d5b8f4fcf0dd29533965e	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	e24bc5bbd10291e1c4aa1e8501bc91060a50f869049d5b8f4fcf0dd29533965e	\N	2013	RS-CD-1	1087	PF		2013		
218	2020-02-04 17:24:41.071062	2020-02-04 00:00:00	0	BRL	Unica	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	true	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5[#]c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e[#]9cdd3da667beebe39a399fbb7b97b08e02b0737cff6b1e4ee5b70a544433911a	1	f	false	0	0	0	0	card	9cdd3da667beebe39a399fbb7b97b08e02b0737cff6b1e4ee5b70a544433911a	\N	c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e	COBRANCA_ENVIADA_AGUARDANDO	NAO_UTILIZADO_CARTAO_TOKENIZADO	8d68bcd4a664d1104371b5d414bd9286c7d9d5cdf48934c437778d14ca1516d5	f	9cdd3da667beebe39a399fbb7b97b08e02b0737cff6b1e4ee5b70a544433911a	\N	2013	RS-CD-1	1087	PF		2013		
\.


--
-- Data for Name: tb_conecta_perfil_ao_grupo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_conecta_perfil_ao_grupo (id, tb_perfil_id, tb_grupo_id) FROM stdin;
1	1	1
2	2	2
3	3	3
4	4	4
5	5	5
6	6	6
\.


--
-- Data for Name: tb_conecta_permissao_ao_perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_conecta_permissao_ao_perfil (id, tb_permissao_id, tb_perfil_id) FROM stdin;
1	1	1
2	2	2
3	19	5
4	20	5
5	21	5
6	22	5
7	23	5
8	24	5
9	25	5
10	26	5
11	27	5
12	28	5
13	29	5
14	3	3
15	4	3
16	5	3
17	6	3
18	7	3
19	8	3
20	9	3
21	10	3
22	11	3
23	12	3
24	19	3
25	20	3
26	21	3
27	22	3
28	23	3
29	24	3
30	25	3
31	26	3
32	27	3
33	28	3
34	29	3
35	13	4
36	14	4
37	15	4
38	16	4
39	17	4
40	18	4
41	19	4
42	20	4
43	21	4
44	22	4
45	23	4
46	24	4
47	25	4
48	31	6
\.


--
-- Data for Name: tb_conecta_usuario_ao_grupo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_conecta_usuario_ao_grupo (id, tb_usuario_id, tb_grupo_id) FROM stdin;
1	1	1
2	2	2
3	1	3
4	2	4
5	6	6
\.


--
-- Data for Name: tb_grupo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_grupo (id, str_grupo_titulo, str_grupo_descricao) FROM stdin;
1	Grupo de Administradores Total	Usuários deste grupo possuem acesso administrador
2	Grupo de Usurios Padrão  Total	Usuários deste grupo possuem acesso padrão
4	Grupo de usuários associados	UUsuários deste grupo recebem poderes de associados
5	Grupo de usuários operadores	Usuários deste grupo recebem poderes de opeardores do sistema padrão
3	Grupo de usuários administradores padrão	Usuários deste grupo recebem poderes de administradores, porém padrão
6	Grupo de testes	Serve apenas para testes sera deletado
\.


--
-- Data for Name: tb_notificacoes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_notificacoes (id, type, message, status, token) FROM stdin;
31	IMPORT_STARTED	Importação Iniciada	1	0
32	IMPORT_FINISHED	Importação Concluída	1	1
\.


--
-- Data for Name: tb_notifications; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_notifications (nid, type, message, uid, status, created_at) FROM stdin;
4	NOTIFICATION	TESTE DE NOTIFICACAO	1	0	2020-02-05
\.


--
-- Data for Name: tb_pendencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_pendencia (id, dat_data_de_registro, str_inscricao, str_ano, str_tipo_de_cobranca, bol_paga, dou_valor, str_hash_identifica_a_pendencia) FROM stdin;
1088	2020-02-03 11:58:57.751748	RS-CD-420	2014	Unica	f	733.04	a51b92eba4e134886adb0086421157d0d62207827004712ce8f6f7372ee675ef
1089	2020-02-03 11:58:57.768059	RS-CD-420	2019	Unica	f	6500.3	5865fa11065594ec05ddc3e0068c17b6402bf64681387828429a5a4d515b50ef
1090	2020-02-03 11:58:57.770257	RS-CD-695	2013	Unica	f	100.47	0b614b011064e78b70ca0277c8606810584f46c505dba6f5c50dcc36109be029
1091	2020-02-03 11:58:57.789002	RS-CD-1095	2014	Unica	f	345.04	2a6f739601750f02ce1578179cf24aeed9f684ca0d5a90eeaa990124df377753
1092	2020-02-03 11:58:57.793736	RS-CD-1095	2019	Unica	f	631.3	b984d38b4d0f347b1ca910802122cad5b202f4f381679ce9f658fab69bc94f61
1093	2020-02-03 11:58:57.796424	RS-CD-1153	2013	Unica	f	2000	39e6532a4ccb56b3fe642482706ab32e45d5ccae81ea9178128f0575b3d6e6e5
1094	2020-02-03 11:58:57.802298	RS-CD-1866	2014	Unica	f	150.04	7dd50e26566b3daf2db9d8b28f20ee2741b4e498f373388bb99322021373eaff
1095	2020-02-03 11:58:57.8075	RS-CD-1866	2019	Unica	f	1631.3	a30f1e2c4b5b474f139954d506a0f261bd6bbe548e7df533401373a476269a7b
1096	2020-02-03 11:58:57.809869	RS-CD-1915	2013	Unica	f	721.47	6265d4d68d90972d4305c84d9fafb10f5ce729e5c8c258ce2bd1d4e860a327b7
1097	2020-02-03 11:58:57.814665	RS-CD-2032	2014	Unica	f	1733.04	ff86b7da7d1c5bd90a9973cc6d245f1bd80c3b310d079b4710108970fa009f48
1098	2020-02-03 11:58:57.819581	RS-CD-2032	2019	Unica	f	6631.3	8691fcab27f95161b23986e28e2dbe815c6012b691da13facde57a2f7a3e6a41
1099	2020-02-03 11:58:57.822218	RS-CD-2175	2013	Unica	f	7215.47	64ba42080993d2711eb33a4101c8bc6a4fa2e9ce0077d319eb01802f62064cf4
1100	2020-02-03 11:58:57.82721	RS-CD-2287	2014	Unica	f	7433.04	78f6f43f003afc96b776eab32949d3c700533b5704cfffceb3b8cf9952190228
1101	2020-02-03 11:58:57.83196	RS-CD-2287	2019	Unica	f	6351.3	889ff467bd265635827572f2d4b2d38dc49a783982f110affaad4fca21978798
1102	2020-02-03 11:58:57.834871	RS-CD-2506	2013	Unica	f	7212.47	739f747083fc39c27a709a81bf34aad97cae5b7760f261db4979ec8debfd0ca6
1103	2020-02-03 11:58:57.840164	RS-CD-2619	2014	Unica	f	7333.04	dcbdbe636fa9b807e97fb0d4ae11d50558b102bd26cbca04b62ae1bfd2673c3c
1104	2020-02-03 11:58:57.844892	RS-CD-2619	2019	Unica	f	6331.3	d78696807df5c02d41fd9d4c2b8e85f9d1b2976b5a2ce4e54330a07244f7324f
1105	2020-02-03 11:58:57.847404	RS-CD-2663	2013	Unica	f	721.47	78c85e626ec2f8fca90cb5e055114ddf21afb545d1d9cb45da7be56f839d1b54
1106	2020-02-03 11:58:57.854339	RS-CD-2724	2014	Unica	f	7313.04	35517d753e8705f73e504c56d3c4e820f1f4b1bd0531cdc62daff14be588db67
1107	2020-02-03 11:58:57.859294	RS-CD-2724	2019	Unica	f	6311.3	6862cb704950b4a6cf316c891585169b31fc4ae27f96800c1fa670b0b392fdf4
1108	2020-02-03 11:58:57.864684	RS-CD-2851	2013	Unica	f	3721.47	9f11d03a4add795af74b5544465925fbd66c71a52e89d779ee19bac5860965d4
1109	2020-02-03 11:58:57.869471	RS-CD-2917	2014	Unica	f	5733.04	55d18b945deb54b98f5c7eb352dfdd5656ccb78e4d287503a9228538f358ab7c
1110	2020-02-03 11:58:57.873906	RS-CD-2917	2019	Unica	f	65531.3	c9e40511febabfc2721a5e07944c531cc66f943a5f073eab27fc907dee4f6cbc
1111	2020-02-03 11:58:57.876331	RS-CD-2931	2013	Unica	f	7251.47	4f71c83720cce9ca16047dae8d869f4f45c4a681e7113a313d38c21cab65816f
1112	2020-02-03 11:58:57.880963	RS-CD-2964	2014	Unica	f	7533.04	b00d97eafa2ba0848b487f48169206ea36104e11ebb8532fea4e6c354e0213ee
1113	2020-02-03 11:58:57.886055	RS-CD-2964	2019	Unica	f	6351.3	6a16e6e6f4bf5846679d45f2cbeea6c6c9a48ba87826fe3eee6e4629220eda43
1114	2020-02-03 11:58:57.888367	RS-CD-2988	2013	Unica	f	721.45	6f40cde81c18db8819f44161bb9e3740d0cff4f27e45dd066dd8fb6b388ab4d1
1087	2020-02-03 11:58:57.735516	RS-CD-1	2013	Unica	t	1000.47	9436dac72c74fabf692a52d89649287492b56afce83203f1f24173d27efa6475
\.


--
-- Data for Name: tb_perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_perfil (id, str_perfil_titulo, str_perfil_descricao) FROM stdin;
1	Administrador Total	Este perfil recebe todas permissões de administrador
2	Padrão Total	Este perfil recebe todas permissões padrão
3	Administrador	Este perfil é administrador com todas permissões, porém definidas
4	Associado	Este perfil é o associado do sistema que efeturá pagamentos
5	Operador	Este perfil é o operador de sistema básico
6	Teste	Perfil de teste sera deletado
\.


--
-- Data for Name: tb_permissao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_permissao (id, str_permissao_titulo, str_permissao_descricao, str_permissao_alias) FROM stdin;
31	Teste	Teste	ACAO_TESTE
3	Administrador	Gera nova cobrança para uma associado	ACAO_ADMINISTRACAO_GERAR_COBRANCA
4	Adminitrador	Cancela um pagamento que ainda não se concretizou (liquidou na financeira)	ACAO_ADMINISTRACAO_CANCELA_PAGAMENTO
5	Administrador	Cancela uma cobrança cadastrada para um associado que ainda não foi paga	ACAO_ADMINISTRACAO_CANCELA_COBRANCA
6	Administrador	Extorna um pagamento	ACAO_ADMINISTRACAO_EXTORNAR_PAGAMENTO
7	Administrador	Busca lista de todos pagamentos pendentes em um determinado período de tempo	ACAO_ADMINISTRACAO_BUSCA_TODOS_PAGAMENTOS_PENDENTES_EM_UM_PERIODO_DE_TEMPO
8	Administrador 	Busca lista todos pagamentos liberados (liquidados) que o valor já está disponível na financeira em um determinado período de tempo	ACAO_ADMINISTRACAO_BUSCA_TODOS_PAGAMENTOS_LIBERADOS_EM_PERIODO_DE_TEMPO
9	Administrador	Busca lista de todos pagamentos (total) em determinado perído de tempo	ACAO_ADMINISTRACAO_LISTA_TOTAIS_DE_PAGAMENTOS_EM_PERIODO_DE_TEMPO
10	Administrador	Busca lista de pagamentos com vencimentos próximos	ACAO_ADMINISTRACAO_BUSCA_LISTA_PAGAMENTOS_COM_VENCIMENTOS_PROXIMOS
11	Administrador	Busca lista de pagamentos futuros. (Gerar estatística)	ACAO_ADMINISTRACAO_LISTAR_LANCAMENTOS_FUTUROS
12	Administrador	Envia uma nova senha para um usuário ou associado	ACAO_ADMINISTRACAO_ENVIAR_NOVA_SENHA
13	Associado	Associado executa pagamento de cobrança	ACAO_ASSOCIADO_PAGAR
14	Associado	Lista todas pendências de um associado	ACAO_ASSOCIADO_LISTAR_PENDENCIAS
15	Associado	Lista todos pagamentos de um associado	ACAO_ASSOCIADO_LISTAR_PAGAMENTOS
16	Associado	Registra duvida (suporte) de um associado	ACAO_ASSOCIADO_AJUDA
17	Associado	Registra pedido de associado para cancelar um pagamento ou extorno	ACAO_ASSOCIADO_SOLICITAR_CANCELAMENTO
18	Associado	Busca lista recebidos de associado	ACAO_ASSOCIADO_BUSCAR_RECIBOS
19	Autenticação	Autentica um associado ou usuário no sistema	ACAO_AUTENTICACAO_AUTENTICAR
20	Autenticação	Desloga um associado ou usurio de sistema	ACAO_AUTENTICACAO_DESLOGAR
21	Autenticação	Verifica se o usuário informado existe	ACAO_AUTENTICACAO_VERIFICAR_USUARIO
22	Autenticação	Verifica se a senha é válida	ACAO_AUTENTICACAO_VERIFICAR_SENHA
23	Autenticação	Permite recuperar a senha por link enviado para e-mail	ACAO_AUTENTICACAO_RECUPERAR_SENHA
24	Autenticação	Solicita um tolke-link por e-mail com um código hash para alterar a senha	ACAO_AUTENTICACAO_SOLICITAR_ALTERAR_SENHA
25	Autenticação	Alterar a senha atuala para uma nova utlizando o token-link (código) solicitado anteriormente	ACAO_AUTENTICACAO_ALTERAR_SENHA
26	Operações	Tokenizar cartão na financeira quando ela oferece o recurso	ACAO_OPERACOES_TOKENIZAR_CARTAO
27	Operações	Mudar token do cartão na financeira quando ela oferece o recurso	ACAO_OPERACOES_MUDAR_TOKEN_DO_CARTAO
28	OPerações	Bloquear uso do token de cartão no sitema	ACAO_OPERACOES_BLOQUEAR_TOKEN_DE_CARTAO
29	Operações	Desbloquear uso do token de cartão no sistema	ACAO_OPERACOES_DESBLOQUEAR_TOKEN_DE_CARTAO
30	Webhook	Utlizar WEBHOOK para atualizar estatus de um pagamento (operação de sistema)	ACAO_WEBHOOK_ATUALIZA_ESTATUS_DO_PAGAMENTO
1	Total Administrador	Esta permissão define acesso a todas funções de administradores	PERMISSAO_ADMINISTRACAO_TOTAL
2	Total Associado	Esta permissão define acesso a todas funções de usuário padrão	PERMISSAO_PADRAO_TOTAL
\.


--
-- Data for Name: tb_recuperar_senha; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_recuperar_senha (id, str_ip_usuario, str_inscricao_usuario, dat_data_recuperar_senha, str_hash_usuario, bol_status) FROM stdin;
162	201.21.206.62	RS-CD-1	2020-02-05 17:12:00	397d25a52b95384b2f601ef422e6fa616046d1f5471fadd98ceb2c7abd5db9e9	t
163	201.21.206.62	RS-CD-1	2020-02-05 17:13:00	3f28f964549a948cc6c46ad9b1f698e6437aacc272faf974b9c6748fc2d2e004	t
\.


--
-- Data for Name: tb_sessao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_sessao (id, str_sessao_token, tb_usuario_id) FROM stdin;
369	980defa1140568e0870cf335bd603bf45464547bc0824159ec3904f8013e96f5	1
368	109dc32dc89c91cd1a37fff7effa945f4f29e7825d6567910defffc8644f8f45	128695
\.


--
-- Data for Name: tb_situacao_da_cobranca; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_situacao_da_cobranca (id, str_situacao_da_cobranca) FROM stdin;
2	COBRANCA_APROVADA_AGUARDANDO_RETORNO
3	COBRANCA_EXTORNADA
4	COBRANCA_CANCELADA
5	COBRANCA_UNIFICADA
6	COBRANCA_REJEITADA
7	COBRANCA_LIQUIDADA
8	COBRANCA_ENVIADA_NOVAMENTE
9	COBRANCA_INICIADA
10	COBRANCA_INICIADA_AINDA_NAO_ENVIADA
11	COBRANCA_ENVIADA_AGUARDANDO
12	COBRANCA_PREPARADA_PARA_SER_REENVIADA
13	COBRANCA_CARTAO_INVALIDO
14	COBRANCA_ERRO_DESCONHECIDO
15	COBRANCA_APROVADA_AGUARDANDO_RETORNO
\.


--
-- Data for Name: tb_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_usuario (id, bol_ativo, dat_data_criacao, str_usuario, str_senha, str_email_1, str_tipo, str_inscricao, str_email_2, str_nome, str_endereco, str_bairro, str_uf, str_municipio, str_cep, str_telefone_1, str_telefone_2, str_cpf_cnpj, str_debito, str_inscrespttec, str_nomeresptec, str_ident, str_emissor, str_estcivil, str_sexo, str_nacionalidade, str_cancelado, str_dat_cro, str_capital_social, str_data_requerimento, str_tipo_pf_ou_pj, str_hash_identifica_o_cliente) FROM stdin;
128709	t	2020-01-31 15:33:40.66007	RS-CD-2851	123		PADRAO	RS-CD-2851		MIRIAM EDI BERRUTTI DE MATOS	RS	PORTO ALEGRE	AV VENANCIO AIRES 821 APTO/04	RS	90040-193	(51) 3311-5255		131.698.580-68	2013#Unica#R$3721,47			410146	SSP	MIRIAM EDI BERRUTTI DE MATOS	F	BRASILEIRA	N	14/06/1972		03/03/1972	PF	a2bf762151d8621977a41af6e4b2411754620170f51712041ee6285a05aa99c4
128710	t	2020-01-31 15:33:40.667224	RS-CD-2917	123	jose.o.castilhos@gmail.com	PADRAO	RS-CD-2917		MARIA CRISTINA BOOS DE CASTILHOS	RS	PORTO ALEGRE	R PADRE CHAGAS 140 CONJ 301	RS	90570-080	(51) 3222-0279	(51) 99117-6689	136.406.740-49	2014#Unica#R$5733,04@2019#Unica#R$65531,30			4004870384	II	MARIA CRISTINA BOOS DE CASTILHOS	F	BRASILEIRA	N	06/09/1972		26/01/1972	PF	4fe779813c1b429e000f9889f49e1df44a0111fd39c0d42e503f6045dd3c2283
128711	t	2020-01-31 15:33:40.676968	RS-CD-2931	123		PADRAO	RS-CD-2931		TEODOR KURRLE	RS	ESTEIO	R FERNANDO FERRARI 965 SALA 102	RS	93260-030	(51) 3473-0561		117.051.400-68	2013#Unica#R$7251,47			397498	SSP	TEODOR KURRLE	M	BRASILEIRA	N	06/09/1972		24/02/1972	PF	88dc0c6f9c9ae47942020705aaff6856f029587fca2b3c4440aa1526b6feb27e
128713	t	2020-01-31 15:33:40.693407	RS-CD-2988	123	luis.justin@pop3planning.com	PADRAO	RS-CD-2988		SONIA DE LOURDES ALMEIDA ARAUJO	RS	CAPAO DA CANOA	TRAV. LARGO 2 CASA 809 CONJ. RES. VILAGE	RS	95555-000	(51) 3665-5055	(51) 9698-5185	956.145.670-20	2013#Unica#R$721,45			38779	II	SONIA DE LOURDES ALMEIDA ARAUJO	F	BRASILEIRA	N	22/11/1972		06/09/1972	PF	74bb8fd73d03c818553e85421812c5893650f260f0e5aa31646b683514bf8c61
3	t	2019-11-29 16:24:16.639722	Administrador	123	Email_1	SISTEMA	Inscricao	Email_2	Nome	Endereco	Bairro	UF	Municipio	CEP	Telefone_1	Telefone_2	CPF/CNPJ	Debito	InscRespTec	NomeRespTec	Ident	Emissor	EstCivil	Sexo	Nacionalidade	Cancelado	Data_CRO	Capital_Social	Data_Requerimento\n	\N	\N
128706	t	2020-01-31 15:33:40.631231	RS-CD-2619	123		PADRAO	RS-CD-2619		PEDRO ERNESTO ENDERLE	RS	RIO GRANDE	AV MAJOR CARLOS PINTO 503 	RS	96211-021	(53) 9145-5937		091.664.750-15	2014#Unica#R$7333,04@2019#Unica#R$6331,30			35508	SSP	PEDRO ERNESTO ENDERLE	M	BRASILEIRA	N	14/01/1972		15/12/1970	PF	c9c8c4490bd8f0d50e0364aca7a6a6e5a683a197ed61e1240c982e09c899aa49
128707	t	2020-01-31 15:33:40.641848	RS-CD-2663	123	carlospalmadias@hotmail.com	PADRAO	RS-CD-2663		CARLOS ALBERTO TORRES PALMA DIAS	RS	PORTO ALEGRE	R OIAMPI 281 	RS	91770-580	(51) 99912-7374		123.541.710-72	2013#Unica#R$721,47			8007423232	SSP	CARLOS ALBERTO TORRES PALMA DIAS	M	BRASILEIRA	N	14/01/1972		01/07/1971	PF	0ec3b79193d3a09ec110f497292471ec919955ad942417ffc74e11d9969e2d7c
128708	t	2020-01-31 15:33:40.649359	RS-CD-2724	123		PADRAO	RS-CD-2724		FLAVIO SALIM	RS	PORTO ALEGRE	R JOAQUIM SILVEIRA 510 	RS	91060-320	(51) 3340-2769		125.544.720-68	2014#Unica#R$7313,04@2019#Unica#R$6311,30			418432	SSP	FLAVIO SALIM	M	BRASILEIRA	N	11/02/1972		20/04/1971	PF	bfdb0be53e6b4b32e0f6acf0bd39d1032ad512f41eb386a98b01a8e6ee811f5e
128712	t	2020-01-31 15:33:40.683684	RS-CD-2964	123		PADRAO	RS-CD-2964	sergiozanca@hotmail.com	SERGIO ZANCANELLA	RS	SARANDI	RUA MIGUEL ORTOLAN 791 CASA	RS	99560-000		(54) 3361-1048	126.814.820-20	2014#Unica#R$7533,04@2019#Unica#R$6351,30			448057	SSP	SERGIO ZANCANELLA	M	BRASILEIRA	N	04/10/1972		16/02/1972	PF	27eb73853f01e3247599997ae9cec8ad3c20b0098f90cc4dc62d881adeac9418
128698	t	2020-01-31 15:33:40.560894	RS-CD-1095	123		PADRAO	RS-CD-1095		JOYCE THEODOLINA LIMA ECHART	RS	SANTA MARIA	R SILVA JARDIM 1324 AP 30	RS	97010-490				2014#Unica#R$345,04@2019#Unica#R$631,30			11263	SSP	JOYCE THEODOLINA LIMA ECHART	F	BRASILEIRA	S	13/12/1967				7dab32c8615d1ca12a9df961430507036a7345061b99691669a7f96134adfa9f
128699	t	2020-01-31 15:33:40.572056	RS-CD-1153	123		PADRAO	RS-CD-1153		ELDER CUNHA PAIVA	RS	PORTO ALEGRE	R MARECHAL FLORIANO PEIXOTO 270 SALA 602	RS	90020-060	(51) 3211-4955		013.878.980-00	2013#Unica#R$2000,00			8000333263	SJS	ELDER CUNHA PAIVA	M	BRASILEIRA	N	13/12/1967			PF	3323aa94fe1a9b0d15a89c1b6dba0f55df5ba5643009200e05938dcaaa6bfa91
128700	t	2020-01-31 15:33:40.579914	RS-CD-1866	123		PADRAO	RS-CD-1866		INACIO PASA	RS	JULIO DE CASTILHOS	RUA BARAO DO RIO BRANCO 470 LOJA 13-A	RS	98130-000	(55) 212-4784		037.040.170-00	2014#Unica#R$150,04@2019#Unica#R$1631,30			27214	SSP	INACIO PASA	M	BRASILEIRA	S	27/09/1968		18/01/1968	PF	942c2f5a7448bf364319c6d553978d8e522e8ceb63774b23c17dd81209d92095
128701	t	2020-01-31 15:33:40.589658	RS-CD-1915	123		PADRAO	RS-CD-1915		SALVADOR MOURA E SILVA	RS	PORTO ALEGRE	R SAO CARLOS 177 CASA	RS	90220-121	(51) 9167-9499		087.758.000-63	2013#Unica#R$721,47			196703	SSP	SALVADOR MOURA E SILVA	M	BRASILEIRA	N	05/12/1968			PF	88515729bd7262ef7727d36b5be59add79b2d5dfce43b18e5fbe57f59cf7528b
128702	t	2020-01-31 15:33:40.596515	RS-CD-2032	123		PADRAO	RS-CD-2032		LUIZ CARLOS ESCOBAR MACHADO	RS	CANOAS	R IRMAO ADAO RUI 496 	RS	92020-070	(51) 3478-2568		055.083.040-53	2014#Unica#R$1733,04@2019#Unica#R$6631,30			335539	SSP	LUIZ CARLOS ESCOBAR MACHADO	M	BRASILEIRA	S	17/09/1969		27/03/1969	PF	833f95176be28137a305e97660bffc7fc1f364a1be483ea98a2c65b10b03a0d0
128703	t	2020-01-31 15:33:40.605963	RS-CD-2175	123		PADRAO	RS-CD-2175		LUIZ OTAVIO LOPES CABRAL	SC	IMBITUBA	RUA A-5 S/N CASA	RS	88780-000	(48) 3356-0821		004.551.050-49	2013#Unica#R$7215,47			301315	SSP	LUIZ OTAVIO LOPES CABRAL	M	BRASILEIRA	S	30/03/1970		18/02/1970	PF	b554069b165d98986ade1eefb11a8168b38fad805c73c698f9e7e5e13739f2de
128704	t	2020-01-31 15:33:40.61296	RS-CD-2287	123		PADRAO	RS-CD-2287		LUIZ ANTONIO LARANJEIRA	RS	MONTENEGRO	RUA RAMIRO BARCELOS 919 CASA	RS	95780-000	(51) 9331-5963	(51) 3632-3102	112.374.220-00	2014#Unica#R$7433,04@2019#Unica#R$6351,30			365898	SSP	LUIZ ANTONIO LARANJEIRA	M	BRASILEIRA	N	02/05/1970		02/05/1970	PF	5f90d0f81aa48fec4d7c2d4a8c34f9a701781d8d71b8bf64162c25f4e51dcd9b
128705	t	2020-01-31 15:33:40.623755	RS-CD-2506	123	EBBODONTO@GMAIL.COM	PADRAO	RS-CD-2506		EVILASIO BORGES BOEIRA	RS	SAO MARCOS	RAIMUNDO PESSINI 1148 TERREO	RS	95190-000	(54) 3291-1206	(54) 9609-3983	003.488.800-49	2013#Unica#R$7212,47			7006455039	SSP	EVILASIO BORGES BOEIRA	M	BRASILEIRA	N	24/05/1971		01/04/1971	PF	4a43057e0f89055ec8e4af3db2fd4d2c18fb09ff539949287cb3b30cdc8c9fd1
1	t	2019-11-29 16:23:23.806561	Master	123	luis.justin@pop3planning.com	SISTEMA	Inscricao	Email_2	Nome	Endereco	Bairro	UF	Municipio	CEP	1259	Telefone_2	CPF/CNPJ	Debito	InscRespTec	NomeRespTec	Ident	Emissor	EstCivil	Sexo	Nacionalidade	Cancelado	Data_CRO	Capital_Social	Data_Requerimento\n	\N	\N
128695	t	2020-01-31 15:33:40.532167	RS-CD-1	123	luis.justin@pop3planning.com	PADRAO	RS-CD-1		PAULO DE OLIVEIRA CHAVES	RS		  						2013#Unica#R$1000,47					PAULO DE OLIVEIRA CHAVES	M	BRASILEIRA	S	31/05/1967				c53d8ccc0f02f4a59ed58c8e91d1c60b50307dd1d6deba465bd0124c3641236e
128696	t	2020-01-31 15:33:40.542865	RS-CD-420	123		PADRAO	RS-CD-420		LAURI BERNARDI	RS	PORTO ALEGRE	EST DO RINCAO 2001 CHACARA	RS	91787-380	(51) 9982-5629	(51) 3263-1334	008.261.540-34	2014#Unica#R$733,04@2019#Unica#R$6500,30			20437	SSP	LAURI BERNARDI	M	BRASILEIRA	S	11/12/1967		27/09/1967	PF	8debcccd5c14994c895e77444e8291b152f0248c458f6d5ed2e3d3caa53e9921
128697	t	2020-01-31 15:33:40.553808	RS-CD-695	123		PADRAO	RS-CD-695		WALDIR PEDRO SEVERGNINI	RS		 122 CASA	RS	98805-250	(55) 3381-1276	(55) 3312-2176	006.510.280-00	2013#Unica#R$100,47			1022729329	SSP	WALDIR PEDRO SEVERGNINI	M	BRASILEIRA	S	12/12/1967			PF	e8098ffc0e7522b7a2a39a1013629a9c48add7786678179359d8bb36fbffda79
\.


--
-- Data for Name: tb_usuario_teste; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_usuario_teste (id, str_nome, str_senha, str_email_1, str_inscricao, str_hash_identifica_o_cliente) FROM stdin;
10	USUARIO TESTE 2	1111111111	marcelo.rodrigues@pop3planning.com	RS-CD-2	\N
\.


--
-- Data for Name: tb_usuario_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_usuario_tipo (id, str_tipo) FROM stdin;
1	SISTEMA
2	PADRAO
\.


--
-- Name: tb_cobranca_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_cobranca_id_seq', 218, true);


--
-- Name: tb_conecta_perfil_ao_grupo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_conecta_perfil_ao_grupo_id_seq', 6, true);


--
-- Name: tb_conecta_permissao_ao_perfil_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_conecta_permissao_ao_perfil_id_seq', 48, true);


--
-- Name: tb_conecta_usuario_ao_grupo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_conecta_usuario_ao_grupo_id_seq', 5, true);


--
-- Name: tb_grupo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_grupo_id_seq', 6, true);


--
-- Name: tb_notificacoes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_notificacoes_id_seq', 32, true);


--
-- Name: tb_notifications_nid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_notifications_nid_seq', 4, true);


--
-- Name: tb_pendencia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_pendencia_id_seq', 1114, true);


--
-- Name: tb_perfil_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_perfil_id_seq', 6, true);


--
-- Name: tb_permissao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_permissao_id_seq', 31, true);


--
-- Name: tb_recuperar_senha_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_recuperar_senha_id_seq', 179, true);


--
-- Name: tb_sessao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_sessao_id_seq', 371, true);


--
-- Name: tb_situacao_da_cobranca_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_situacao_da_cobranca_id_seq', 15, true);


--
-- Name: tb_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_usuario_id_seq', 128713, true);


--
-- Name: tb_usuario_teste_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_usuario_teste_id_seq', 10, true);


--
-- Name: tb_usuario_tipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_usuario_tipo_id_seq', 2, true);


--
-- Name: tb_cobranca tb_cobranca_pk_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_cobranca
    ADD CONSTRAINT tb_cobranca_pk_id PRIMARY KEY (id);


--
-- Name: tb_conecta_perfil_ao_grupo tb_conecta_perfil_ao_grupo_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_conecta_perfil_ao_grupo
    ADD CONSTRAINT tb_conecta_perfil_ao_grupo_id PRIMARY KEY (id);


--
-- Name: tb_conecta_permissao_ao_perfil tb_conecta_permisao_ao_perfil_pk_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_conecta_permissao_ao_perfil
    ADD CONSTRAINT tb_conecta_permisao_ao_perfil_pk_id PRIMARY KEY (id);


--
-- Name: tb_conecta_usuario_ao_grupo tb_conecta_usuario_ao_grupo_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_conecta_usuario_ao_grupo
    ADD CONSTRAINT tb_conecta_usuario_ao_grupo_id PRIMARY KEY (id);


--
-- Name: tb_grupo tb_grupo_pk_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_grupo
    ADD CONSTRAINT tb_grupo_pk_id PRIMARY KEY (id);


--
-- Name: tb_notificacoes tb_notificacoes_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_notificacoes
    ADD CONSTRAINT tb_notificacoes_pk PRIMARY KEY (id);


--
-- Name: tb_pendencia tb_pendencia_pk_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pendencia
    ADD CONSTRAINT tb_pendencia_pk_id PRIMARY KEY (id);


--
-- Name: tb_perfil tb_perfil_pk_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_perfil
    ADD CONSTRAINT tb_perfil_pk_id PRIMARY KEY (id);


--
-- Name: tb_permissao tb_permissao_pk_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_permissao
    ADD CONSTRAINT tb_permissao_pk_id PRIMARY KEY (id);


--
-- Name: tb_sessao tb_sessao_pk_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_sessao
    ADD CONSTRAINT tb_sessao_pk_id PRIMARY KEY (id);


--
-- Name: tb_situacao_da_cobranca tb_situacao_da_cobranca_pk_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_situacao_da_cobranca
    ADD CONSTRAINT tb_situacao_da_cobranca_pk_id PRIMARY KEY (id);


--
-- Name: tb_usuario tb_usario_pk_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_usuario
    ADD CONSTRAINT tb_usario_pk_id PRIMARY KEY (id);


--
-- Name: tb_sessao tb_usuario_id_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_sessao
    ADD CONSTRAINT tb_usuario_id_unique UNIQUE (tb_usuario_id);


--
-- Name: tb_usuario_tipo tb_usuario_str_tipo_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_usuario_tipo
    ADD CONSTRAINT tb_usuario_str_tipo_unique UNIQUE (str_tipo);


--
-- Name: tb_usuario_tipo tb_usuario_tipo_pk_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_usuario_tipo
    ADD CONSTRAINT tb_usuario_tipo_pk_id PRIMARY KEY (id);


--
-- Name: fki_tb_pendencia_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_tb_pendencia_id ON public.tb_cobranca USING btree (tb_pendencia_id);


--
-- Name: fki_tb_usuario_str_tipo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_tb_usuario_str_tipo_fk ON public.tb_usuario USING btree (str_tipo);


--
-- Name: tb_cobranca tb_pendencia_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_cobranca
    ADD CONSTRAINT tb_pendencia_id FOREIGN KEY (tb_pendencia_id) REFERENCES public.tb_pendencia(id);


--
-- Name: tb_usuario tb_usuario_str_tipo_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_usuario
    ADD CONSTRAINT tb_usuario_str_tipo_fk FOREIGN KEY (str_tipo) REFERENCES public.tb_usuario_tipo(str_tipo);


--
-- PostgreSQL database dump complete
--

