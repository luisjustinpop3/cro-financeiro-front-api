import time
from rx import create, of
from rx import operators as ops

class RxPy:

    def __init__(self):
        self.f1 = 0
        self.f2 = 0
        self.f3 = 0
        self.r = 0

    def funcao1(self):
        self.f1 = 10
        time.sleep(3)
        print("func-1")
        return "executada-funcao-1"

    def funcao2(self):
        self.f2 = 20
        time.sleep(6)
        print("func-2")
        return "executada-funcao-2"

    def funcao3(self):
        self.f3 = 30
        time.sleep(9)
        print("func-3")
        return "executada-funcao-3"

    def soma(self):
        self.r = self.f1 + self.f2 + self.f3
        print(self.r)

    def push_five_strings(self, observer, scheduler):
        observer.on_next(self.funcao1())
        observer.on_next(self.funcao2())
        observer.on_next(self.funcao3())
        observer.on_completed()


    def ativa(self):
        source1 = create(self.push_five_strings)
        source1.subscribe(
            on_next = lambda i: print("teste", i),
            on_error = lambda e: print("Error Occurred: ", e),
            on_completed = lambda: print("Done!"),
        )

r = RxPy()
r.ativa()
r.soma()